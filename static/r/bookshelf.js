function setup_bookshelf() {
    const filtersElt = document.getElementById("filters-container");
    filtersElt.style.visibility = null;
    const booklistElt = document.getElementById("booklist");
    const allLi = new Map();

    function newLi(text, selector) {
        let li = document.createElement("li");
        if (!allLi.has(selector)) {
            allLi.set(selector, new Array());
        }
        allLi.get(selector).push(li);
        li.classList.add("inline-block", "text-lg", "text-gray-500", "cursor-pointer", "hover:text-sky-800");
        li.textContent = `#${text}`;
        li.addEventListener("click", (_) => {
            if (li.dataset["selected"] == "yes") {
                delete li.dataset["selected"];
                for (const el of booklistElt.children) {
                    delete el.dataset["filtered_" + selector];
                    if (Object.keys(el.dataset).every((str) => str.search(/^filtered_/) == -1)) {
                        el.classList.remove("bookHidden");
                    }
                }
            } else {
                allLi.get(selector).forEach((otherLi) => {
                    delete otherLi.dataset["selected"];
                });
                li.dataset["selected"] = "yes";
                for (const el of booklistElt.children) {
                    if (el.dataset[selector].search(new RegExp("\\b" + text + "\\b")) == -1) {
                        el.dataset["filtered_" + selector] = true;
                        el.classList.add("bookHidden");
                    } else {
                        delete el.dataset["filtered_" + selector];
                        if (Object.keys(el.dataset).every((str) => str.search(/^filtered_/) == -1)) {
                            el.classList.remove("bookHidden");
                        }
                    }
                }
            }
        });
        return li;
    }

    const categoryFiltersElt = document.getElementById("category-filters");
    let categories = Array.from(new Set(Array.from(booklistElt.children).flatMap((el) => el.dataset.categories.split(" ")))).sort();
    for (const cat of categories) {
        categoryFiltersElt.appendChild(newLi(cat, "categories"));
    }

    const opinionFiltersElt = document.getElementById("opinion-filters");
    let opinions = Array.from(new Set(Array.from(booklistElt.children).map((el) => el.dataset.opinion))).sort();
    for (const op of opinions) {
        opinionFiltersElt.appendChild(newLi(op, "opinion"));
    }

    const yearFiltersElt = document.getElementById("year-filters");
    let readYears = Array.from(new Set(Array.from(booklistElt.children).map((el) => el.dataset.readDate.slice(0, 4)))).sort();
    readYears.reverse();
    for (const readYear of readYears) {
        yearFiltersElt.appendChild(newLi(readYear, "readDate"));
    }

    console.log("Bookself setup done", categories, opinions, readYears);
}

window.addEventListener("load", (_) => {
    setup_bookshelf();
});

document.addEventListener("DOMContentLoaded", function() {
    renderMathInElement(document.body, {
        delimiters: [
            {left: '$$', right: '$$', display: false},
            {left: '\\[', right: '\\]', display: true},
        ],
        throwOnError : false
    });
});
