/* Untangle JavaScript's objects */

if (typeof Object.create !== 'function') {
    Object.create = function (o) {
        function f() { }
        f.prototype = o;
        return new f();
    };
}

var Foo = {
    x : 23,
    y : 42,
    print : function () {
        log(self);
    }
}

var Bar = Object.append(Object.clone(Foo), {
    z : 0
});
