<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>Fixed Frequency Loops</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="/r/screen.css" media="screen">
    <link rel="stylesheet" href="/r/print.css" media="print">
    <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
    <link rel="stylesheet" href="/r/syntax.css">

    <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net" />
    <link rel="canonical" href="https://scvalex.net/posts/20/" />

    <meta name="description" content="Suppose we want to stress-test a server by hitting it with a fixed number of requests per second. Or maybe we want to write a game loop that runs at…">

    <link rel="icon" type="image/png" href="/r/icon.png">

    <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
  </head>
  <body>
    <header id="postHeader">
  <a href="/">
    <img src="/r/title_post.png" width="111" height="120" alt="scvalex.net">
  </a>
</header>



<div id="content">
  <article id="standalonePost">
    <div class="postTitle">
      <h2 title="Stats
Words: 275
SLOC: 38
Effort: 208">20. Fixed Frequency Loops</h2>
      <time datetime="2013-03-06">2013-03-06</time>
    </div>

    <div class="post">
      <p>Suppose we want to stress-test a server by hitting it with a fixed number of requests per second. Or maybe we want to write a game loop that runs at a fixed number of frames per second. In both cases, we want to run some code at a fixed frequency <span class="math">\(\nu\)</span>. More precisely, we want a loop that calls some function, sleeps for a bit, then restarts, and overall, the function is called <span class="math">\(\nu\)</span> times per second.</p>
<p>First, we need a way to time code. The function <a href="http://linux.die.net/man/2/gettimeofday"><code>gettimeofday(2)</code></a> gives us a <code>timeval</code>, which represents the current time in seconds and microseconds. By bracketing our code in a couple of these, we can find exactly how many microseconds it took to run.</p>
<p>Given that we know the frequency <span class="math">\(\nu\)</span>, we also know that our code <em>should</em> take <span class="math">\(1/\nu\)</span> seconds to run. We time our code, and if it runs too quickly, we insert an artificial pause. In other words, if our code runs in <span class="math">\(t\)</span> seconds, and <span class="math">\(t &lt; 1/\nu\)</span>, we should pause for <span class="math">\(1/\nu - t\)</span> seconds.</p>
<p>We can use <a href="http://linux.die.net/man/3/usleep"><code>usleep(3)</code></a> to pause for a given number of microseconds. In our case, we want to <code>usleep</code> for <span class="math">\((1/\nu - t) * 1\_000\_000\)</span> microseconds. Note that <code>usleep</code> is unavailable on some older Unix systems; <a href="http://linux.die.net/man/2/pselect"><code>pselect(2)</code></a> can be used instead.</p>
<p>Putting it all together, we get the following <a href="https://gist.github.com/scvalex/5096359">code</a>:</p>
<pre class="sourceCode c"><code class="sourceCode c"><span class="ot">#include &lt;stdio.h&gt;</span>
<span class="ot">#include &lt;sys/time.h&gt;</span>
<span class="ot">#include &lt;time.h&gt;</span>
<span class="ot">#include &lt;unistd.h&gt;</span>

<span class="co">/* How many times per second do we want the code to run? */</span>
<span class="dt">const</span> <span class="dt">int</span> frequency = <span class="dv">2</span>;

<span class="co">/* Pretend to do something useful. */</span>
<span class="dt">void</span> do_work() {
    <span class="dt">volatile</span> <span class="dt">int</span> i;
    <span class="kw">for</span> (i = <span class="dv">0</span>; i &lt; <span class="dv">10000000</span>; ++i)
        ;
}

<span class="dt">int</span> main(<span class="dt">int</span> argc, <span class="dt">char</span> *argv[]) {
    <span class="co">/* How long should each work unit take? */</span>
    <span class="dt">long</span> slice = (<span class="dt">long</span>)(<span class="fl">1.0</span> / frequency * <span class="dv">1000000</span>);

    <span class="kw">struct</span> timeval beginning;
    gettimeofday(&amp;beginning, NULL);
    <span class="kw">struct</span> timeval last_tick = beginning;

    <span class="dt">long</span> total = <span class="dv">0</span>;
    <span class="dt">int</span> tick;
    <span class="kw">for</span> (tick = <span class="dv">1</span>; <span class="dv">1</span>; ++tick) {
        do_work();

        <span class="kw">struct</span> timeval now;
        gettimeofday(&amp;now, NULL);

        <span class="co">/* How much time has passed since the last tick? */</span>
        <span class="dt">long</span> usec_elapsed = (now.tv_sec - last_tick.tv_sec) * <span class="dv">1000000</span>
                          + (now.tv_usec - last_tick.tv_usec);
        last_tick = now;

        <span class="co">/* How time did we spend working this tick? */</span>
        <span class="dt">long</span> usec_work = (now.tv_sec - beginning.tv_sec) * <span class="dv">1000000</span>
                          + (now.tv_usec - beginning.tv_usec);

        total += usec_elapsed;
        printf(<span class="st">&quot;Worked for %ldus. Average time per tick: %ldus (%ldus since last).</span><span class="ch">\n</span><span class="st">&quot;</span>,
               usec_work, total / tick, usec_elapsed);

        <span class="co">/* Pause if appropriate. */</span>
        <span class="dt">long</span> usec_tosleep = slice - usec_work;
        <span class="kw">if</span> (usec_tosleep &gt; <span class="dv">0</span>) {
            printf(<span class="st">&quot;Sleeping for %ldus.</span><span class="ch">\n</span><span class="st">&quot;</span>, usec_tosleep);
            usleep(usec_tosleep);
        }

        <span class="co">/* Prepare for the next tick. */</span>
        gettimeofday(&amp;beginning, NULL);
    }

    <span class="kw">return</span> <span class="dv">0</span>;
}</code></pre>
<p>The code itself is unsurprising, but it’s easy to get wrong: if, when calculating the length of the pause, you use the time for the entire loop, <code>usec_elapsed</code>, instead of the time for the work part of it, <code>usec_work</code>, you end up waiting alternatively for <span class="math">\(1/\nu\)</span> and <span class="math">\(0\)</span>.</p>

    </div>
  </article>

  <div id="postNavigation">
    <ul>

      <li><a id="prevLink" rel="prev" href="/posts/19/" title="Bank Cards in the US">⋘ Prev</a></li>

      <li><a href="/posts/">All Posts</a></li>

      <li><a id="nextLink" rel="next" href="/posts/21/" title="Complexity Ordering">Next ⋙</a></li>

    </ul>
  </div>
</div>

<footer id="postFooter">
  <ul>
    <li class="copyright">
      © 2013 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code>
    </li>
  </ul>
  <ul>
    <li>
      <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
        CC BY-SA</a>
    </li>
    <li>
      <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
    </li>
  </ul>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/atom.xml">RSS Feed</a></li>
  </ul>
</footer>

  </body>
</html>
