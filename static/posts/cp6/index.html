<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">

  <title>Checker Challenge « Computer programming</title>
  <meta name="description" content="Checker Challenge is a very famous programming problem. It's one of the first examples of backtracking anyone learns and variations of it frequently appare in contests. In this article, I'll present the classic algorithm for solving it and then a few optimisations to make it run less like a geriatric turtle.">

  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp6/">

  <link rel="stylesheet" href="/r/wp-style-11.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">

  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li><a id="prevLink" rel="prev" href="/posts/cp5/">⋘ Prev</a></li>
      <li><a href="/posts/">All Posts</a></li>
      <li><a id="nextLink" rel="next" href="/posts/cp7/">Next ⋙</a></li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-10-24</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: Checker&nbsp;Challenge">Checker&nbsp;Challenge</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p><a href="https://web.archive.org/web/20080401105005/http://www.inf.bme.hu/contests/tasks/usa93q.html">Checker Challenge</a> is a very famous programming problem. It’s one of the first examples of backtracking anyone learns and variations of it frequently appare in contests.</p>
              <p>In this article, I’ll present the classic algorithm for solving it and then a few optimisations to make it run less like a geriatric turtle.</p>
              <p>First the problem:</p>
              <blockquote>
                <p>Given a checker table of <em>n</em> lines and <em>n</em> columns, find all the ways of placing <em>n</em> checker pieces so that no two are on the same line, column or diagonal.</p>
              </blockquote>
              <p>If <em>n</em> is greater or equal to <em>6</em> there is always at least one way to arrange the pieces. Usually there are <strong>a lot</strong> of ways (there are <em>73712</em> for <em>n = 13</em>).</p>
              <p>For <em>n = 6</em>, this is one of the possible four arrangements:<br>
              <code><br>
              | | O | | | | |<br>
              | | | | O | | |<br>
              | | | | | | O |<br>
              | O | | | | | |<br>
              | | | O | | | |<br>
              | | | | | O | |<br></code></p>
              <p>First of all, you need a way to memorise the board. A matrix of <em>n x n</em> is the obvious solution, but it’s not really necessary. Notice that there can be <strong>at most</strong> one piece on any column, so it’s enough to use an array of <em>n</em>, each element containing the number of the row on which the corresponding element in the matrix is located. <strong>e.g.</strong> For the board above, the array would be <em>{4, 1, 5, 2, 6, 3}</em>.</p>
              <p>Now, how do you approach such a problem? The simplest way (and, as it happens, the fastest) is to solve it recursively: first take the first column and attempt to place a piece on each of the <em>n</em> rows; for each of these, attempt to place a piece on each row of the second column; for every <strong>valid</strong> such arrangement, try to place a piece on each row of the third column; …; and so on until you place a piece on the <em>n<sup>th</sup></em> column, at which point, you have a valid arrangement.</p>
              <p>The key point in this algorithm is to know that <strong>when you’re working on the <em>k<sup>th</sup></em> column, all the pieces on the first <em>k - 1</em> columns form a valid arrangement</strong>.</p>
              <p>As it happens, this is <em>way</em> to slow. For <em>n = 10</em>, I got tired of waiting and stopped the programme. If you do the math, you’ll see that for <em>n</em>, this algorithm does about <em>n<sup>n</sup></em> recursive calls. This is bad, but if you write the checking routines inefficiently, you can easily make it slower by a factor of <em>n</em>.</p>
              <p>There are three ways I know of to check if a position is valid (i.e. if a piece can be placed there). The first checks for validity <strong>after</strong> the piece has been placed, while the other two work by marking positions as invalid:</p>
              <ol>
                <li>The first is simple: let’s say you’ve reached column <em>c</em> and are wondering whether or not you can place a piece of row <em>r</em>. First take all the columns before <em>c</em> and check whether a piece is already on row <em>r</em>; afterwards, for every column <em>c’</em> before <em>c</em>, and for every row <em>r’</em> on that column check if <em>abs(r - r’) = c - c’</em>. If any of there conditions are true, than you can’t place a piece on row <em>r</em>.</li>
                <li>The second is just a way of trading memory for speed. Instead of doing as above, keep three vectors of boolean values (<em>1</em> for the lines (<em>usedRows</em>) and <em>2</em> for the two types of diagonals (<em>d1</em> and <em>d2</em>)), and for every piece you place on the board mark the corresponding elements in the three vectors as used. That is, mark <em>r<sup>th</sup></em> element in <em>rows</em>, the <em>n - c - 1 + r <sup>th</sup></em> element in <em>d1</em> and the <em>r + c <sup>th</sup></em> element in <em>d2</em>. If any of the corresponding elements for any position are marked, than that position is invalid.</li>
                <li>The third way is just combining the first two. For every row column <em>c’</em> before <em>c</em>, supposing the piece is on row <em>r’</em>, mark the rows <em>r’</em>, r’ - (c - c’) and <em>r’ + (c - c’)</em> as invalid.</li>
              </ol>
              <p>That’s it. For another speed boost, I use bitsets instead of vectors. Anyway, here’s the code in C (<a href="/r/checker.c" title="checker.c">checker.c</a>):</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;
#include &lt;string.h&gt;

int q[14];

int sol[3][14];

int r = 0,
        d1 = 0,
        d2 = 0;

int solP = 0;

int n;

void gen_sol(int col) {
        if (col == n) {
                if (solP &lt; 3)
                        memcpy(sol[solP], q, sizeof(int) * n);
                ++solP;
                return;
        }

//NOTE Just calculating the non-admisible rows for this col every time is slightly faster than the "keep track of used rows and diagonals" solution
//      int oc = 0;
//      for (int i(0); i &lt; col; ++i) {
//              oc |= 1&lt;&lt;q[i];
//              oc |= 1&lt;&lt;(q[i] - (col - i));
//              oc |= 1&lt;&lt;(q[i] + (col - i));
//      }

        int i;
        for (i = 0; i &lt; n; ++i) {
                if (!(r & 1&lt;&lt;i) && !(d1 & 1&lt;&lt;(n - col - 1 + i)) && !(d2 & 1&lt;&lt;(i + col))) {
                        r |= 1&lt;&lt;i;
                        d1 |= 1&lt;&lt;(n - col - 1 + i);
                        d2 |= 1&lt;&lt;(i + col);

                        q[col] = i;
                        gen_sol(col + 1);

                        r ^= 1&lt;&lt;i;
                        d1 ^= 1&lt;&lt;(n - col - 1 + i);
                        d2 ^= 1&lt;&lt;(i + col);
                }
        }
}

int main(int argc, char *argv[]) {
        n = 6;

        // Every solution's reflection is also a valid solution, so for the first
        // calculate only the first n/2 arrangements.
        int max = n;
        if (6 != n) {
                max = (n + 1) / 2;
        }

        // This complication appears because the odd values of n. Think about
        // it for a while and it will become obvious.
        int temp;
        int i;
        for (i = 0; i &lt; max; ++i) {
                temp = solP;

                r |= 1&lt;&lt;i;
                d1 |= 1&lt;&lt;(n - 1 + i);
                d2 |= 1&lt;&lt;(i);

                q[0] = i;
                gen_sol(1);

                r ^= 1&lt;&lt;i;
                d1 ^= 1&lt;&lt;(n - 1 + i);
                d2 ^= 1&lt;&lt;(i);
        }

        int j;
        for (i = 0; i &lt; 3; ++i) {
                for (j = 0; j &lt; n; ++j)
                        printf("%d ", sol[i][j] + 1);
                printf("\n");
        }

        if ((n % 2 == 0) && (6 &lt; n))
                solP *= 2;
        else if ((n % 2 == 1) && (n &gt; 6))
                solP += temp;
        printf("%d\n", solP);

        return 0;
}
</pre>
              <p>There are a few other optimisations littered throughout the code, but they’re not hard to understand. Good luck. Always open to comments.</p>
              <p>P.S. This also is also called “The Queens Problem” or the like.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-10-24 at 16:55</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter.css">
      <script type="text/javascript" src="/r/shCore.js"></script>
      <script type="text/javascript" src="/r/shBrushCpp.js"></script>
      <script type="text/javascript">
        dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
