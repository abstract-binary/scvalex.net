<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>Faking Exceptions in C</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="/r/screen.css" media="screen">
    <link rel="stylesheet" href="/r/print.css" media="print">
    <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
    <link rel="stylesheet" href="/r/syntax.css">

    <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net" />
    <link rel="canonical" href="https://scvalex.net/posts/32/" />

    <meta name="description" content="I love the breaking the structure of structured programs. We have previously implemented coroutines in C using setjmp(3) and longjmp(3). We are now going to fake exceptions in C using…">

    <link rel="icon" type="image/png" href="/r/icon.png">

    <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
  </head>
  <body>
    <header id="postHeader">
  <a href="/">
    <img src="/r/title_post.png" width="111" height="120" alt="scvalex.net">
  </a>
</header>



<div id="content">
  <article id="standalonePost">
    <div class="postTitle">
      <h2 title="Stats
Words: 449
SLOC: 37
Effort: 158">32. Faking Exceptions in C</h2>
      <time datetime="2013-10-02">2013-10-02</time>
    </div>

    <div class="post">
      <p>I love the breaking the structure of structured programs. We have previously implemented <a href="/posts/2/">coroutines in C</a> using <a href="http://linux.die.net/man/3/setjmp"><code>setjmp(3)</code></a> and <a href="http://linux.die.net/man/3/longjmp"><code>longjmp(3)</code></a>. We are now going to fake exceptions in C using the same functions.</p>
<p>Consider a program <code>foo</code>: first thing on startup, it calls <code>init()</code> which reads a config file and opens a log file for writing.</p>
<pre class="sourceCode c"><code class="sourceCode c"><span class="dt">int</span> read_config() {
  <span class="kw">if</span> (!(config = fopen(<span class="st">&quot;foo.config&quot;</span>, <span class="st">&quot;r&quot;</span>))) {
    <span class="kw">return</span> <span class="dv">1</span>;
  }
  <span class="kw">return</span> <span class="dv">0</span>;
}

<span class="dt">int</span> init_log() {
  <span class="kw">if</span> (!(log = fopen(<span class="st">&quot;foo.log&quot;</span>, <span class="st">&quot;a&quot;</span>))) {
    <span class="kw">return</span> <span class="dv">1</span>;
  }
  <span class="kw">return</span> <span class="dv">0</span>;
}

<span class="dt">int</span> init() {
  <span class="kw">if</span> (<span class="dv">0</span> != read_config()) {
    fprintf(stderr, <span class="st">&quot;error in read_config; rethrowing&quot;</span>);
    <span class="kw">return</span> <span class="dv">1</span>;
  }
  <span class="kw">if</span> (<span class="dv">0</span> != init_log()) {
    fprintf(stderr, <span class="st">&quot;error in init_log; rethrowing&quot;</span>);
    <span class="kw">return</span> <span class="dv">1</span>;
  }
  <span class="kw">return</span> <span class="dv">0</span>;
}

<span class="dt">int</span> foo() {
  <span class="kw">if</span> (<span class="dv">0</span> != init()) {
    fprintf(stderr, <span class="st">&quot;error in init&quot;</span>);
    <span class="kw">return</span> <span class="dv">1</span>;
  }
  <span class="co">// awesome foo stuff</span>
  <span class="kw">return</span> <span class="dv">0</span>;
}</code></pre>
<p>Both <code>read_config()</code> and <code>init_log()</code> may fail due to file errors, and <code>init()</code> may fail because it calls functions that may fail. We signal failure by returning non-zero and we check for failure after each function call. This is all fairly standard stuff for C.</p>
<p>This style of error handling has the advantage of being very clear, but it is also quite noisy. Point in case, <code>init()</code> is almost three times as long as it needs to be. The overhead would be justified if we were also recovering from errors, but here, we just want to propagate them. What’s worse is that we have to do error handling after every function call, and since the code is repetitive and boring, we’re likely to forget.</p>
<p>We want exceptions and we get them in C with <code>setjmp()</code> and <code>longjmp()</code> which are effectively intra-function gotos.</p>
<pre class="sourceCode c"><code class="sourceCode c">jmp_buf fail;

<span class="dt">void</span> read_config() {
  <span class="kw">if</span> (!(config = fopen(<span class="st">&quot;foo.config&quot;</span>, <span class="st">&quot;r&quot;</span>))) {
    longjmp(fail, <span class="dv">1</span>);
  }
}

<span class="dt">void</span> init_log() {
  <span class="kw">if</span> (!(log = fopen(<span class="st">&quot;foo.log&quot;</span>, <span class="st">&quot;a&quot;</span>))) {
    longjmp(fail, <span class="dv">1</span>);
  }
}

<span class="dt">void</span> init() {
  read_config();
  init_log();
}

<span class="dt">int</span> foo() {
  <span class="kw">if</span> (<span class="dv">0</span> == setjmp(fail)) {
    init();
    <span class="co">// awesome foo stuff</span>
    <span class="kw">return</span> <span class="dv">0</span>;
  } <span class="kw">else</span> {
    fprintf(stderr, <span class="st">&quot;error in init&quot;</span>);
    <span class="kw">return</span> <span class="dv">1</span>;
  }
}</code></pre>
<p>Like before, we use non-zero values to signal failure, but now we send them to <code>foo()</code> via <code>longjmp()</code> bypassing <code>init()</code> completely. On the one hand, this code is much less noisy and easier to write, but on the other, we can no longer understand the functions independently; we need to read the entire program to see its behaivour.</p>
<p>This scheme isn’t quite at feature parity with most other exception systems, but we can easily fix that. If we replace the single top-level <code>jmp_buf</code> with a linked list of <code>jmp_buf</code>s, we get nested exception handlers and rethrowable exceptions. If instead of sending <code>int</code>s via <code>longjmp()</code>, we send pointers to some exception <code>struct</code>, we get rich exceptions. If we register signal handlers, we can convert system errors like “write to broken pipe” and “division by zero” to exceptions.</p>
<p>As we can see, exceptions in C can be straightforward but there are some features which would be harder to implement from scratch. For instance, getting proper stack traces without having to modify each function call would be difficult. Similarly, it’s unclear how these exceptions should work with regard to threads and forking.</p>
<hr />
<p>One amazing thing about all this is that it’s actually used in practice. <a href="http://www.libpng.org/pub/png/libpng-manual.txt">Libpng</a>, which is included directly or indirectly in most graphical applications, does error handling this way: “When libpng encounters an error, it expects to longjmp back to your routine”.</p>
<hr />
<p>This style of error handling is also used in more modern languages. Go’s <a href="http://golang.org/doc/articles/defer_panic_recover.html">defer/panic/recover</a> mechanism is reminiscent of <code>setjump()</code> and <code>longjmp()</code>.</p>

    </div>
  </article>

  <div id="postNavigation">
    <ul>

      <li><a id="prevLink" rel="prev" href="/posts/31/" title="IsString Abuse">⋘ Prev</a></li>

      <li><a href="/posts/">All Posts</a></li>

      <li><a id="nextLink" rel="next" href="/posts/33/" title="Cycling Arrays">Next ⋙</a></li>

    </ul>
  </div>
</div>

<footer id="postFooter">
  <ul>
    <li class="copyright">
      © 2013 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code>
    </li>
  </ul>
  <ul>
    <li>
      <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
        CC BY-SA</a>
    </li>
    <li>
      <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
    </li>
  </ul>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/atom.xml">RSS Feed</a></li>
  </ul>
</footer>

  </body>
</html>
