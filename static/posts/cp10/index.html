<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">

  <title>Minimum Spanning Trees: Prim’s Algorithm « Computer programming</title>
  <meta name="description" content="In this article I give an informal definition of a graph and of the minimum spanning tree. Afterwards I describe Prim’s algorithm and then follow its execution on an example. Finally, the code in C is provided.">

  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp10/">

  <link rel="stylesheet" href="/r/wp-style-83.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">

  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li><a id="prevLink" rel="prev" href="/posts/cp9/">⋘ Prev</a></li>
      <li><a href="/posts/">All Posts</a></li>
      <li><a id="nextLink" rel="next" href="/posts/cp11/">Next ⋙</a></li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-11-09</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: Minimum Spanning Trees: Prim�s�Algorithm">Minimum Spanning Trees: Prim’s&nbsp;Algorithm</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p>In this article I give an informal definition of a <strong>graph</strong> and of the <strong>minimum spanning tree</strong>. Afterwards I describe <strong>Prim’s algorithm</strong> and then follow its execution on an example. Finally, the code in C is provided.</p>
              <h4>Graphs</h4>
              <p><a href="https://en.wikipedia.org/wiki/Graph_(abstract_data_type)">Wikipedia</a> gives one of the common definitions of a <strong>graph</strong>:</p>
              <blockquote>
                <p>In computer science, a graph is a kind of data structure, specifically an abstract data type (ADT), that consists of a set of nodes and a set of edges that establish relationships (connections) between the nodes. The graph ADT follows directly from the graph concept from mathematics.<br>
                Informally, G=(V,E) consists of vertices, the elements of V, which are connected by edges, the elements of E. Formally, a graph, G, is defined as an ordered pair, G=(V,E), where V is a finite set and E is a set consisting of two element subsets of V.</p>
              </blockquote>
              <p>This is a <strong>graph</strong>:<br>
              <img src="/r/p1.png" alt="p1.png"></p>
              <p>It’s a set of nodes (A, B, C, D and E) and the edges (lines) that interconnect them.</p>
              <p>An important thing to note about this graph is that the edges are bidirectional, i.e. if A is connected to B, then B is connected to A. This makes it an undirected graph.</p>
              <p>A common extension is to attribute <strong>weights</strong> to the edges. This is what I’ve done with the previous graph:<br>
              <img src="/r/p2.png" alt="p2"></p>
              <h4>Minimum spanning trees</h4>
              <p>Basically <a href="https://en.wikipedia.org/wiki/Minimum_spanning_tree">a minimum spanning tree</a> is a <strong>subset of the edges of the graph</strong>, so that <strong>there’s a path form any node to any other node</strong> and that <strong>the sum of the weights of the edges is minimum</strong>.</p>
              <p>Here’s the minimum spanning tree of the example:<br>
              <img src="/r/g3.png" alt="g3.png"></p>
              <p>Look at the above image closely. It <strong>contains all of the initial nodes</strong> and some of the initial edges. Actually it contains <strong>exactly <em>n - 1</em> edges</strong>, where <em>n</em> is the number of nodes. It’s called a <a href="https://en.wikipedia.org/wiki/Tree_(graph_theory)">tree</a> because there are no cycles.</p>
              <p>You can think of the graph as a map, with the nodes being cities, the edges passable terrain, and the weights the distance between the cities.</p>
              <p>It’s worth mentioning that a graph can have several minimum spanning trees. Think of the above example, but replace all the weight with <em>1</em>. The resulting graph will have <em>6</em> minimum spanning trees.</p>
              <p>Given a graph, find one of its minimum spanning trees.</p>
              <h4>Prim’s Algorithm</h4>
              <p>One of the classic algorithms for this problem is that found by Robert C. Prim. It’s a <strong>greedy</strong> style algorithm and it’s guaranteed to produce a correct result.</p>
              <p>In the following discussion, let the distance from each node not in the tree to the tree be the edge of minimal weight between that node and some node in the tree. If there is no such edge, assume the distance is infinity (this shouldn’t happen).</p>
              <p>The algorithm (greedily) builds the minimal spanning tree by iteratively adding nodes into a working tree:</p>
              <ol>
                <li>Start with a tree which contains only one node.</li>
                <li>Identify a node (outside the tree) which is closest to the tree and add the minimum weight edge from that node to some node in the tree and incorporate the additional node as a part of the tree.</li>
                <li>If there are less then <em>n - 1</em> edges in the tree, go to <strong>2</strong></li>
              </ol>
              <p>For the example graph, here’s how it would run:</p>
              <p>Start with only node A in the tree.<br>
              <img src="/r/g4.png" alt="g4.png"></p>
              <p>Find the closest node to the tree, and add it.<br>
              <img src="/r/g5.png" alt="g5.png"></p>
              <p>Repeat until there are <em>n - 1</em> edges in the tree.<br>
              <img src="/r/g6.png" alt="g6.png"></p>
              <p><img src="/r/g7.png" alt="g7.png"></p>
              <p><img src="/r/g82.png" alt="g8.png"></p>
              <h4>The Programme</h4>
              <p>The following programme just follows the algorithm. It runs in <strong>O(n<sup>2</sup>)</strong> time.</p>
              <p>Here’s the code in C (<a href="/r/prim.c" title="prim.c">prim.c</a>):</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;

/*
        The input file (weight.txt) look something like this
                4
                0 0 0 21
                0 0 8 17
                0 8 0 16
                21 17 16 0

        The first line contains n, the number of nodes.
        Next is an nxn matrix containg the distances between the nodes
        NOTE: The distance between a node and itself should be 0
*/

int n; /* The number of nodes in the graph */

int weight[100][100]; /* weight[i][j] is the distance between node i and node j;
                        if there is no path between i and j, weight[i][j] should
                        be 0 */

char inTree[100]; /* inTree[i] is 1 if the node i is already in the minimum
                        spanning tree; 0 otherwise*/

int d[100]; /* d[i] is the distance between node i and the minimum spanning
                tree; this is initially infinity (100000); if i is already in
                the tree, then d[i] is undefined;
                this is just a temporary variable. It's not necessary but speeds
                up execution considerably (by a factor of n) */

int whoTo[100]; /* whoTo[i] holds the index of the node i would have to be
                        linked to in order to get a distance of d[i] */

/* updateDistances(int target)
        should be called immediately after target is added to the tree;
        updates d so that the values are correct (goes through target's
        neighbours making sure that the distances between them and the tree
        are indeed minimum)
*/
void updateDistances(int target) {
        int i;
        for (i = 0; i &lt; n; ++i)
                if ((weight[target][i] != 0) && (d[i] &gt; weight[target][i])) {
                        d[i] = weight[target][i];
                        whoTo[i] = target;
                }
}

int main(int argc, char *argv[]) {
        FILE *f = fopen("dist.txt", "r");
        fscanf(f, "%d", &amp;n);
        int i, j;
        for (i = 0; i &lt; n; ++i)
                for (j = 0; j &lt; n; ++j)
                        fscanf(f, "%d", &amp;weight[i][j]);
        fclose(f);

        /* Initialise d with infinity */
        for (i = 0; i &lt; n; ++i)
                d[i] = 100000;

        /* Mark all nodes as NOT beeing in the minimum spanning tree */
        for (i = 0; i &lt; n; ++i)
                inTree[i] = 0;

        /* Add the first node to the tree */
        printf("Adding node %c\n", 0 + 'A');
        inTree[0] = 1;
        updateDistances(0);

        int total = 0;
        int treeSize;
        for (treeSize = 1; treeSize &lt; n; ++treeSize) {
                /* Find the node with the smallest distance to the tree */
                int min = -1;
                for (i = 0; i &lt; n; ++i)
                        if (!inTree[i])
                                if ((min == -1) || (d[min] &gt; d[i]))
                                        min = i;

                /* And add it */
                printf("Adding edge %c-%c\n", whoTo[min] + 'A', min + 'A');
                inTree[min] = 1;
                total += d[min];

                updateDistances(min);
        }

        printf("Total distance: %d\n", total);

        return 0;
}
</pre>
              <p>And here’s a sample input file (<a href="/r/dist.txt" title="dist.txt">dist.txt</a>). It’s the example graph:<br>
              <code>5<br>
              0 10 0 5 0<br>
              10 0 5 5 10<br>
              0 5 0 0 0<br>
              5 5 0 0 20<br>
              0 10 0 20 0<br></code></p>
              <p>The code’s commented and there shouldn’t be any problems.</p>
              <p>Good luck. Always open to comments.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-11-09 at 15:26</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter-a.css">
      <script type="text/javascript" src="/r/shCore.js"></script>
      <script type="text/javascript" src="/r/shBrushCpp.js"></script>
      <script type="text/javascript">
        dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
