<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>Malloc Never Fails</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="/r/screen.css" media="screen">
    <link rel="stylesheet" href="/r/print.css" media="print">
    <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
    <link rel="stylesheet" href="/r/syntax.css">

    <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net" />
    <link rel="canonical" href="https://scvalex.net/posts/6/" />

    <meta name="description" content="Here’s a fun bit of trivia: malloc on Linux never fails [due to the obvious cause]. In this post, we’ll show that this indeed is the case, and explore why…">

    <link rel="icon" type="image/png" href="/r/icon.png">

    <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
  </head>
  <body>
    <header id="postHeader">
  <a href="/">
    <img src="/r/title_post.png" width="111" height="120" alt="scvalex.net">
  </a>
</header>



<div id="content">
  <article id="standalonePost">
    <div class="postTitle">
      <h2 title="Stats
Words: 473
SLOC: 9
Effort: 0">6. Malloc Never Fails</h2>
      <time datetime="2012-11-02">2012-11-02</time>
    </div>

    <div class="post">
      <p>Here’s a fun bit of trivia: <code>malloc</code> on Linux never fails [due to the obvious cause]. In this post, we’ll show that this indeed is the case, and explore why this happens.</p>
<p>First off, what <em>should</em> happen? The <a href="http://linux.die.net/man/3/malloc"><code>malloc(3)</code></a> manpage only says that, “On error, these functions return NULL.”, and it’s careful not to specify what the error conditions are.</p>
<p>Let’s try it out; here’s a program that repeatedly allocates <span class="math">\(1\)</span> GB memory chunks until it fails.</p>
<pre class="sourceCode c"><code class="sourceCode c"><span class="ot">#include &lt;stdio.h&gt;</span>
<span class="ot">#include &lt;stdlib.h&gt;</span>

<span class="dt">int</span> main(<span class="dt">int</span> argc, <span class="dt">char</span> *argv[]) {
    printf(<span class="st">&quot;Before</span><span class="ch">\n</span><span class="st">&quot;</span>);
    <span class="dt">int</span> i = <span class="dv">0</span>;
    <span class="kw">while</span> (malloc(<span class="dv">1</span> &lt;&lt; <span class="dv">30</span>)) {
        printf(<span class="st">&quot;Allocated %d GB</span><span class="ch">\n</span><span class="st">&quot;</span>, ++i);
    }
    printf(<span class="st">&quot;After</span><span class="ch">\n</span><span class="st">&quot;</span>);

    <span class="kw">return</span> <span class="dv">0</span>;
}</code></pre>
<p>Running it, I see it failing after allocating about <span class="math">\(130315\)</span> GB, which, needless to say, is a lot more than the memory available on my laptop.</p>
<p>If we <a href="http://linux.die.net/man/1/strace"><code>strace(1)</code></a> the above program, we see that it’s <a href="http://linux.die.net/man/2/mmap"><code>mmap(2)</code></a>ing 1 GB chunks of private, anonymous memory for file descriptor <span class="math">\(-1\)</span>. Checking the manpage, we see that this is indeed the portable way to allocate memory on Unix.</p>
<pre class="sourceCode"><code>...
mmap(NULL, 1073745920, PROT_READ|PROT_WRITE,
     MAP_PRIVATE|MAP_ANONYMOUS, -1, 0)
= 0x7d6706f76000
write(1, &quot;Allocated 2627 GB\n&quot;, 18) = 18
...</code></pre>
<p>With this in mind, we can now see individual and total allocations for any program with commands like the following:</p>
<pre class="sourceCode bash"><code class="sourceCode bash"><span class="kw">strace</span> ls <span class="kw">2&gt;&amp;1</span> <span class="kw">&gt;</span>/dev/null <span class="kw">|</span> <span class="kw">grep</span> mmap

<span class="kw">strace</span> ls <span class="kw">2&gt;&amp;1</span> <span class="kw">&gt;</span>/dev/null <span class="kw">|</span> <span class="kw">grep</span> mmap <span class="kw">|</span> <span class="kw">\</span>
  <span class="kw">awk</span> <span class="st">&#39;{ total += $2 } END { print total }&#39;</span>

<span class="co"># ls allocates around 14561777 over a run</span></code></pre>
<p>Clearly, <code>ls</code> isn’t using 14 MB to run, so what’s going on? The answer is that Linux allocates memory pages for processes lazily. So, regardless of how much memory you ask for, it will just give it to you, but it will only <em>really</em> reserve it when you try to write to it. This is basically a combination of <a href="http://en.wikipedia.org/wiki/Virtual_memory">virtual memory</a>, and <a href="http://en.wikipedia.org/wiki/Demand_paging">demand paging</a>.</p>
<p>But wait, you ask, if that’s the case, why does it fail eventually? Both the kernel and glibc have an overhead when allocating memory. In my case, I saw the test program chew through about <span class="math">\(3\)</span> GB of memory after allocating <span class="math">\(130315\)</span> GB. A quick calculation gives us the overhead per 1 MB chunk:</p>
<pre class="sourceCode"><code>3 GB / 130315 / (1 GB / 1 MB) = 24 B</code></pre>
<p>That’s about 24 bytes overhead for each 1 MB of memory, which seems reasonable, and it explains why we usually don’t even notice the overhead. Section III of this <a href="http://www.phrack.org/issues.html?issue=66&amp;id=6#article">Phrack article</a> is a down to earth description of how the glibc malloc implementation works, if you’re curious.</p>
<p>So, <code>malloc</code> on Linux only fails if there isn’t enough memory for its control structures. It does <em>not</em> fail if there isn’t enough memory to fulfill the request.</p>
<hr>
<p>To clarify, the surprising behaviour <code>malloc</code> has does <em>not</em> mean we should ignore its return value. We just need to be careful because <code>malloc</code> returning successfully does not <em>always</em> mean that we can use the requested memory.</p>
<hr>
<p><span class="person"><span class="citation" data-cites="mete">@mete</span></span> notes in the comments that the situation is somewhat more complicated: the Linux kernel uses a <a href="http://www.kernel.org/doc/Documentation/vm/overcommit-accounting">heuristic</a> to determine if giving a process more memory than it has will lead to something bad happening.</p>
<hr>
<p>I was wrong about why <code>malloc</code> finally failed! <code>@GodmarBack</code> observes, in the comments, that x64 systems only have an address space of <span class="math">\(48\)</span> bits, which comes out to about <span class="math">\(131000\)</span> GB. So, on my machine at least, the <code>malloc</code> finally failed because of address space exhaustion.</p>

    </div>
  </article>

  <div id="postNavigation">
    <ul>

      <li><a id="prevLink" rel="prev" href="/posts/4/" title="permamake.sh">⋘ Prev</a></li>

      <li><a href="/posts/">All Posts</a></li>

      <li><a id="nextLink" rel="next" href="/posts/7/" title="Java Variance Pitfalls">Next ⋙</a></li>

    </ul>
  </div>
</div>

<footer id="postFooter">
  <ul>
    <li class="copyright">
      © 2012 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code>
    </li>
  </ul>
  <ul>
    <li>
      <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
        CC BY-SA</a>
    </li>
    <li>
      <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
    </li>
  </ul>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/atom.xml">RSS Feed</a></li>
  </ul>
</footer>

  </body>
</html>
