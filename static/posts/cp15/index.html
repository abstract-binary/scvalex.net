<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">

  <title>One Source Shortest Path: Dijkstra’s Algorithm « Computer programming</title>
  <meta name="description" content="In this article I describe Dijkstra's algorithm for finding the shortest path from one source to all the other vertexes in a graph. Afterwards, I provide the source code in C of a simple implementation.">

  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp15/">

  <link rel="stylesheet" href="/r/wp-style-83.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">

  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li><a id="prevLink" rel="prev" href="/posts/cp14/">⋘ Prev</a></li>
      <li><a href="/posts/">All Posts</a></li>
      <li><a id="nextLink" rel="next" href="/posts/cp16/">Next ⋙</a></li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-12-01</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: One Source Shortest Path: Dijkstra’s&nbsp;Algorithm">One Source Shortest Path: Dijkstra’s&nbsp;Algorithm</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p>In this article I describe Dijkstra’s algorithm for finding the shortest path from one source to all the other vertexes in a graph. Afterwards, I provide the source code in C of a simple implementation.</p>
              <p>To understand this you should know what a graph is, and how to store one in memory. If in doubt check <a href="/posts/cp10/">this</a> and <a href="https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)">this</a>.</p>
              <p>Another solution for this problem is the <a href="/posts/cp14/">Bellman-Ford algorithm</a>.</p>
              <h4>The Problem</h4>
              <p>Given the following graph calculate the length of the shortest path from <strong>node 1</strong> to every other node.<br>
              <img src="/r/dj1.png" alt="dj1.png"></p>
              <p>Lets take the nodes <strong>1</strong> and <strong>3</strong>. There are several paths (<strong>1 -&gt; 4 -&gt; 3</strong>, <strong>1 -&gt; 2 -&gt; 3</strong>, etc.), but the shortest of them is <strong>1 -&gt; 4 -&gt; 2 -&gt; 3</strong> of length <strong>9</strong>. Our job is to find it.</p>
              <h4>The Algorithm</h4>
              <p><a href="https://en.wikipedia.org/wiki/Dijkstra's_algorithm">Dijkstra’s algorithm</a> is one of the most common solutions to this problem. Even so, it only works on graphs which have <strong>no edges of negative weight</strong>, and the actual speed of the algorithm can vary from <strong>O(n*lg(lg(n)))</strong> to <strong>O(n<sup>2</sup>)</strong>.</p>
              <p>The idea is somewhat simple:</p>
              <p>Take the length of the shortest path to all nodes to be infinity. Mark the length of the shortest path to the source as <em>0</em>.</p>
              <p><img src="/r/dj2.png" alt="dj2.png"></p>
              <p>Now, we already know that the graph has no edges of negative weight so the a path of length <em>0</em> is the best we can come up with. The path to the source is <em>0</em>, so it’s optimal.</p>
              <p>This algorithm works by making the paths to one more node optimal at each step. So, at the <em>k</em>th step, you know for sure that there are at least <em>k</em> nodes to which you know the shortest path.</p>
              <p>At each step, choose the node, which is not yet optimal, but which is closest to the source; i.e. the node to which the current calculated shortest path is smallest. Then, from it, try to optimise the path to every node connected to it. Finally, mark the said node as <em>optimal</em> (visited, if you prefer). In the previous example, the node which is closest to the source and is not yet optimal is the source. From it, you can optimise the path to nodes <em>2</em> and <em>4</em>.<br>
              <img src="/r/dj3.png" alt="dj3_1.png"></p>
              <p>At this point, the only visited/optimal node is <em>0</em>. Now we have to redo this step <em>4</em> more times (to ensure that all nodes are optimal).</p>
              <p>The next node to consider is <em>4</em>:<br>
              <img src="/r/dj4.png" alt="dj4.png"></p>
              <p>It’s worthwhile to note that at this step, we’ve also found a better path to node <em>2</em>.<br>
              Next is node <em>2</em>:<br>
              <img src="/r/dj5.png" alt="dj5.png"></p>
              <p>Finally, we look at nodes <em>5</em> and <em>3</em> (none of which offer any optimisations):<br>
              <img src="/r/dj5.png" alt="dj5.png"></p>
              <p><img src="/r/dj5.png" alt="dj5.png"></p>
              <p>The actual code in C looks something like this:</p>
              <pre data-highlight="code" class="cpp">

void dijkstra(int s) {
        int i, k, mini;
        int visited[GRAPHSIZE];

        for (i = 1; i &lt;= n; ++i) {
                d[i] = INFINITY;
                visited[i] = 0; /* the i-th element has not yet been visited */
        }

        d[s] = 0;

        for (k = 1; k &lt;= n; ++k) {
                mini = -1;
                for (i = 1; i &lt;= n; ++i)
                        if (!visited[i] && ((mini == -1) || (d[i] &lt; d[mini])))
                                mini = i;

                visited[mini] = 1;

                for (i = 1; i &lt;= n; ++i)
                        if (dist[mini][i])
                                if (d[mini] + dist[mini][i] &lt; d[i])
                                        d[i] = d[mini] + dist[mini][i];
        }
}
</pre>
              <h4>The Programme</h4>
              <p>Putting the above into context, we get the <strong>O(n<sup>2</sup>)</strong> implementation. This works well for most graphs (it will <strong>not</strong> work for graphs with negative weight edges), and it’s quite fast.</p>
              <p>Here’s the source code in C (<a href="/r/dijkstra.c" title="dijkstra.c">dijkstra.c</a>):</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;

#define GRAPHSIZE 2048
#define INFINITY GRAPHSIZE*GRAPHSIZE
#define MAX(a, b) ((a &gt; b) ? (a) : (b))

int e; /* The number of nonzero edges in the graph */
int n; /* The number of nodes in the graph */
long dist[GRAPHSIZE][GRAPHSIZE]; /* dist[i][j] is the distance between node i and j; or 0 if there is no direct connection */
long d[GRAPHSIZE]; /* d[i] is the length of the shortest path between the source (s) and node i */

void printD() {
        int i;
        for (i = 1; i &lt;= n; ++i)
                printf("%10d", i);
        printf("\n");
        for (i = 1; i &lt;= n; ++i) {
                printf("%10ld", d[i]);
        }
        printf("\n");
}

void dijkstra(int s) {
        int i, k, mini;
        int visited[GRAPHSIZE];

        for (i = 1; i &lt;= n; ++i) {
                d[i] = INFINITY;
                visited[i] = 0; /* the i-th element has not yet been visited */
        }

        d[s] = 0;

        for (k = 1; k &lt;= n; ++k) {
                mini = -1;
                for (i = 1; i &lt;= n; ++i)
                        if (!visited[i] && ((mini == -1) || (d[i] &lt; d[mini])))
                                mini = i;

                visited[mini] = 1;

                for (i = 1; i &lt;= n; ++i)
                        if (dist[mini][i])
                                if (d[mini] + dist[mini][i] &lt; d[i])
                                        d[i] = d[mini] + dist[mini][i];
        }
}

int main(int argc, char *argv[]) {
        int i, j;
        int u, v, w;

        FILE *fin = fopen("dist.txt", "r");
        fscanf(fin, "%d", &amp;e);
        for (i = 0; i &lt; e; ++i)
                for (j = 0; j &lt; e; ++j)
                        dist[i][j] = 0;
        n = -1;
        for (i = 0; i &lt; e; ++i) {
                fscanf(fin, "%d%d%d", &amp;u, &amp;v, &amp;w);
                dist[u][v] = w;
                n = MAX(u, MAX(v, n));
        }
        fclose(fin);

        dijkstra(1);

        printD();

        return 0;
}
</pre>
              <p>And here’s a sample input file(<a href="/r/dist2.txt" title="dist.txt">dist.txt</a>):</p>
              <p><code>10<br>
              1 2 10<br>
              1 4 5<br>
              2 3 1<br>
              2 4 3<br>
              3 5 6<br>
              4 2 2<br>
              4 3 9<br>
              4 5 2<br>
              5 1 7<br>
              5 3 4<br></code></p>
              <p>The graph is given as an edge list:</p>
              <ul>
                <li>the first line contains <em>e</em>, the number of edges</li>
                <li>the following <em>e</em> lines contain <em>3</em> numbers: <em>u</em>, <em>v</em> and <em>w</em> signifying that there’s an edge from <em>u</em> to <em>v</em> of weight <em>w</em></li>
              </ul>
              <p>That’s it. Good luck and have fun. Always open to comments.</p>
              <h4>Finding the shortest path</h4>
              <p><strong>UPDATE</strong> In response to <strong>campOs</strong>‘ comment.</p>
              <p>Now we know the distance between the source node and any other node (the distance to the ith node is remembered in <strong>d[i]</strong>). But suppose we also need the path (which nodes make up the path).</p>
              <p>Look at the above code. Where is <strong>d</strong> modified? Where is the recorded distance between the source and a node modified? In two places:</p>
              <p>Firstly, <strong>d[s]</strong> is initialised to be <em>0</em>.</p>
              <pre data-highlight="code" class="cpp">

d[s] = 0;
</pre>
              <p>And then, when a new shortest path is found, <strong>d[i]</strong> is updated accordingly:</p>
              <pre data-highlight="code" class="cpp">

for (i = 1; i &lt;= n; ++i)
        if (dist[mini][i])
                if (d[mini] + dist[mini][i] &lt; d[i])
                        d[i] = d[mini] + dist[mini][i];
</pre>
              <p>The important thing to notice here is that <strong>when you update the shortest distance to node i, you know the previous node in the path to i</strong>. This is, of course, <strong>mini</strong>. This suggests the solution to our problem.</p>
              <p>For every node <strong>i</strong> other than the source, remember not only the distance to it, but also the previous node in the path to it. Thus we have a new array, <strong>prev</strong>.</p>
              <p>Now, we need to make to modifications.<br>
              First, we initialise the value of <strong>prev[i]</strong> to something impossible (say <em>-1</em>) at the start of <strong>dijkstra()</strong>.</p>
              <pre data-highlight="code" class="cpp">

for (i = 1; i &lt;= n; ++i) {
        d[i] = INFINITY;
        prev[i] = -1; /* no path has yet been found to i */
        visited[i] = 0; /* the i-th element has not yet been visited */
}
</pre>
              <p>Secondly, we update the value of <strong>prev[i]</strong> every time a new shortest path is found to i.</p>
              <pre data-highlight="code" class="cpp">

for (i = 1; i &lt;= n; ++i)
        if (dist[mini][i])
                if (d[mini] + dist[mini][i] &lt; d[i]) {
                        d[i] = d[mini] + dist[mini][i];
                        prev[i] = mini;
                }
</pre>
              <p>Good. For every node reachable from the source we know which node is just before it in the shortest path. For the above example, we would have the following array:<br>
              <code>i - prev[i]<br>
              1 - -1<br>
              2 - 4<br>
              3 - 2<br>
              4 - 1<br>
              5 - 4<br></code></p>
              <p>Using this, how do you get the path? Let’s say you want to get to <em>3</em>. Which node comes right before <em>3</em>? Node <em>2</em>. Which node comes right before node <em>2</em>? Node <em>4</em>. Which node comes before <em>4</em>? Node <em>1</em>. We’ve reached the source, so we’re done. Go through this list backwards and you get the path: <em>1 -&gt; 4 -&gt; 2 -&gt; 3</em>. This is easily implemented with <a href="https://en.wikipedia.org/wiki/Recursion">recursion</a>.</p>
              <pre data-highlight="code" class="cpp">

void printPath(int dest) {
        if (prev[dest] != -1)
                printPath(prev[dest]);
        printf("%d ", dest);
}
</pre>
              <p>Here is the updated source: <a href="/r/dijkstra.c" title="dijkstraWithPath.c">dijkstraWithPath.c</a>.</p>
              <p>Good luck.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-12-01 at 22:34</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter.css">
      <script type="text/javascript" src="/r/shCore.js"></script>
      <script type="text/javascript" src="/r/shBrushCpp.js"></script>
      <script type="text/javascript">
        dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
