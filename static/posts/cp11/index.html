<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">

  <title>All Sources Shortest Path: The Floyd-Warshall Algorithm « Computer programming</title>
  <meta name="description" content="In this article I describe the Floyd-Warshall algorithm for finding the shortest path between all nodes in a graph. I give an informal proof and provide an implementation in C.">

  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp11/">

  <link rel="stylesheet" href="/r/wp-style-83.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">

  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li><a id="prevLink" rel="prev" href="/posts/cp10/">⋘ Prev</a></li>
      <li><a href="/posts/">All Posts</a></li>
      <li><a id="nextLink" rel="next" href="/posts/cp12/">Next ⋙</a></li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-11-15</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: All Sources Shortest Path: The Floyd-Warshall Algorithm">All Sources Shortest Path: The Floyd-Warshall&nbsp;Algorithm</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p>In this article I describe the Floyd-Warshall algorithm for finding the shortest path between all nodes in a graph. I give an informal proof and provide an implementation in C.</p>
              <h4>Shortest paths</h4>
              <p>The <strong>shortest path</strong> between two nodes of a graph is a sequence of connected nodes so that the sum of the edges that inter-connect them is minimal.</p>
              <p>Take this graph,<br>
              <img src="/r/p2.png" alt="p2.png"></p>
              <p>There are several paths between <em>A</em> and <em>E</em>:<br>
              <code><strong>Path 1</strong>: A -&gt; B -&gt; E 20<br>
              <strong>Path 2</strong>: A -&gt; D -&gt; E 25<br>
              <strong>Path 3</strong>: A -&gt; B -&gt; D -&gt; E 35<br>
              <strong>Path 4</strong>: A -&gt; D -&gt; B -&gt; E 20<br></code></p>
              <p>There are several things to notice here:</p>
              <ol>
                <li>There can be more then one route between two nodes</li>
                <li>The number of nodes in the route isn’t important (<strong>Path 4</strong> has <em>4</em> nodes but is shorter than <strong>Path 2</strong>, which has <em>3</em> nodes)</li>
                <li>There can be more than one path of minimal length</li>
              </ol>
              <p>Something else that should be obvious from the graph is that any path worth considering is <a href="https://en.wikipedia.org/wiki/Path_(graph_theory)">simple</a>. That is, you only go through each node <strong>once</strong>.</p>
              <p>Unfortunately, this is not always the case. The problem appears when you allow negative weight edges. This isn’t by itself bad. But if <strong>a loop of negative weight appears</strong>, then <strong>there is no shortest path</strong>. Look at this example:<br>
              <img src="/r/fw1.png" alt="A graph containing a negative weight&nbsp;loop"></p>
              <p>Look at the path <em>B -&gt; E -&gt; D -&gt; B</em>. This is a loop, because the starting node is the also the end. What’s the cost? It’s <em>10 - 20 + 5 = -5</em>. This means that adding this loop to a path once lowers the cost of the path by <em>5</em>. Adding it twice would lower the cost by <em>2 * 5 = 10</em>. So, whatever shortest path you may have come up with, you can make it smaller by going through the loop one more time. BTW there’s no problem with a negative cost path.</p>
              <h4>The Floyd-Warshall Algorithm</h4>
              <p>This algorithm calculates the length of the shortest path between all nodes of a graph in O(V<sup>3</sup>) time. Note that it doesn’t actually find the paths, only their lengths.</p>
              <p>Let’s say you have the <a href="https://en.wikipedia.org/wiki/Adjacency_matrix">adjacency matrix</a> of a graph. Assuming no loop of negative values, at this point you have the minimum distance between any two nodes which are connected by an edge.<br>
              <code>A B C D E<br>
              A 0 10 0 5 0<br>
              B 10 0 5 5 10<br>
              C 0 5 0 0 0<br>
              D 5 5 0 0 20<br>
              E 0 10 0 20 0</code></p>
              <p>The graph is the one shown above (the first one).</p>
              <p>The idea is to try to interspace <em>A</em> between any two nodes in hopes of finding a shorter path.<br>
              <code>A B C D E<br>
              A 0 10 0 5 0<br>
              B 10 0 5 5 10<br>
              C 0 5 0 0 0<br>
              D 5 5 0 0 20<br>
              E 0 10 0 20 0</code></p>
              <p>Then try to interspace <em>B</em> between any two nodes:<br>
              <code>A B C D E<br>
              A 0 10 <strong>15</strong> 5 <strong>20</strong><br>
              B 10 0 5 5 10<br>
              C <strong>15</strong> 5 0 <strong>10</strong> <strong>15</strong><br>
              D 5 5 <strong>10</strong> 0 <strong>15</strong><br>
              E <strong>20</strong> 10 <strong>15</strong> <strong>15</strong> 0</code></p>
              <p>Do the same for <em>C</em>:<br>
              <code>A B C D E<br>
              A 0 10 15 5 20<br>
              B 10 0 5 5 10<br>
              C 15 5 0 10 15<br>
              D 5 5 10 0 15<br>
              E 20 10 15 15 0</code></p>
              <p>Do the same for <em>D</em>:<br>
              <code>A B C D E<br>
              A 0 10 15 5 20<br>
              B 10 0 5 5 10<br>
              C 15 5 0 10 15<br>
              D 5 5 10 0 15<br>
              E 20 10 15 15 0</code></p>
              <p>And for <em>E</em>:<br>
              <code>A B C D E<br>
              A 0 10 15 5 20<br>
              B 10 0 5 5 10<br>
              C 15 5 0 10 15<br>
              D 5 5 10 0 15<br>
              E 20 10 15 15 0</code></p>
              <p>This is the actual algorithm:<br></p>
              <pre><code>
# dist(i,j) is "best" distance so far from vertex i to vertex j 

# Start with all single edge paths.
 For i = 1 to n do
     For j = 1 to n do
         dist(i,j) = weight(i,j) 

 For k = 1 to n do # k is the `intermediate' vertex
     For i = 1 to n do
         For j = 1 to n do
             if (dist(i,k) + dist(k,j) &lt; dist(i,j)) then # shorter path?
                 dist(i,j) = dist(i,k) + dist(k,j)
</code></pre>
              <h4>The Programme</h4>
              <p>Here’s the code in C(<a href="/r/floyd_warshall.c" title="floyd_warshall.c">floyd_warshall.c</a>):</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;

int n; /* Then number of nodes */
int dist[16][16]; /* dist[i][j] is the length of the edge between i and j if
                        it exists, or 0 if it does not */

void printDist() {
        int i, j;
        printf("    ");
        for (i = 0; i &lt; n; ++i)
                printf("%4c", 'A' + i);
        printf("\n");
        for (i = 0; i &lt; n; ++i) {
                printf("%4c", 'A' + i);
                for (j = 0; j &lt; n; ++j)
                        printf("%4d", dist[i][j]);
                printf("\n");
        }
        printf("\n");
}

/*
        floyd_warshall()

        after calling this function dist[i][j] will the the minimum distance
        between i and j if it exists (i.e. if there's a path between i and j)
        or 0, otherwise
*/
void floyd_warshall() {
        int i, j, k;
        for (k = 0; k &lt; n; ++k) {
                printDist();
                for (i = 0; i &lt; n; ++i)
                        for (j = 0; j &lt; n; ++j)
                                /* If i and j are different nodes and if
                                        the paths between i and k and between
                                        k and j exist, do */
                                if ((dist[i][k] * dist[k][j] != 0) && (i != j))
                                        /* See if you can't get a shorter path
                                                between i and j by interspacing
                                                k somewhere along the current
                                                path */
                                        if ((dist[i][k] + dist[k][j] &lt; dist[i][j]) ||
                                                (dist[i][j] == 0))
                                                dist[i][j] = dist[i][k] + dist[k][j];
        }
        printDist();
}

int main(int argc, char *argv[]) {
        FILE *fin = fopen("dist.txt", "r");
        fscanf(fin, "%d", &amp;n);
        int i, j;
        for (i = 0; i &lt; n; ++i)
                for (j = 0; j &lt; n; ++j)
                        fscanf(fin, "%d", &amp;dist[i][j]);
        fclose(fin);

        floyd_warshall();

        return 0;
}
</pre>
              <p>Note that of the above programme, all the work is done by only five lines (30-48).</p>
              <p>That’s it. Good luck. Always open to comments.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-11-15 at 19:14</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter.css">
      <script type="text/javascript" src="/r/shCore.js"></script>
      <script type="text/javascript" src="/r/shBrushCpp.js"></script>
      <script type="text/javascript">
        dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
