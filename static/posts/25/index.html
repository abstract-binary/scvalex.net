<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>Where's my Pi?</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="/r/screen.css" media="screen">
    <link rel="stylesheet" href="/r/print.css" media="print">
    <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
    <link rel="stylesheet" href="/r/syntax.css">

    <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net" />
    <link rel="canonical" href="https://scvalex.net/posts/25/" />

    <meta name="description" content="When I powered on my new Raspberry Pi the other day, I realized I had a problem: even though we were both connected to the same WiFi network, I had…">

    <link rel="icon" type="image/png" href="/r/icon.png">

    <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
  </head>
  <body>
    <header id="postHeader">
  <a href="/">
    <img src="/r/title_post.png" width="111" height="120" alt="scvalex.net">
  </a>
</header>



<div id="content">
  <article id="standalonePost">
    <div class="postTitle">
      <h2 title="Stats
Words: 396
SLOC: 14
Effort: 115">25. Where's my Pi?</h2>
      <time datetime="2013-04-09">2013-04-09</time>
    </div>

    <div class="post">
      <p>When I powered on my new Raspberry Pi the other day, I realized I had a problem: even though we were both connected to the same WiFi network, I had no idea what its address was.</p>
<p>My anticlimatic solution was to connect it to a screen, but clearly this wouldn’t always be an option. No, we want to be able to locate a machine just by virtue of it being on the same network as us. The obvious idea is to have it broadcast some identifiable network packet periodically; then, we could wait for the packet, and, once we receive it, work our way back to the machine.</p>
<p>Let’s see if it works with UDP. On the local machine, we start <a href="http://linux.die.net/man/1/nc"><code>netcat(1)</code></a> listening on UDP port <span class="math">\(23456\)</span>:</p>
<pre class="sourceCode"><code>% nc -ulvn -p 23456
listening on [any] 23456 ...</code></pre>
<p>On the Raspberry Pi, we have <code>netcat</code> broadcast its hostname, <code>lily</code>, to everybody on the above UDP port:</p>
<pre class="sourceCode"><code>% echo &quot;$(hostname)&quot; | nc.traditional -ub -w 1 255.255.255.255 23456</code></pre>
<p>Checking back on the local machine, we find <code>lily</code>’s address, <code>192.168.0.6</code>:</p>
<pre class="sourceCode"><code>listening on [any] 23456 ...
connect to [192.168.0.2] from (UNKNOWN) [192.168.0.6] 50028
lily</code></pre>
<p>So, the idea works. But we’d like to get just the source address out of all that, and <code>netcat</code>’s output is a bit hard to work with. Instead of trying to process it, let’s write <a href="https://github.com/scvalex/script-fu/blob/master/whereis.py"><code>whereis.py</code></a>, a Python script just for this task:</p>
<pre class="sourceCode python"><code class="sourceCode python"><span class="co">#!/usr/bin/python</span>

<span class="ch">from</span> __future__ <span class="ch">import</span> print_function
<span class="ch">import</span> socket, sys

<span class="kw">if</span> <span class="ot">__name__</span> == <span class="st">&quot;__main__&quot;</span>:
    <span class="kw">if</span> <span class="dt">len</span>(sys.argv) != <span class="dv">2</span>:
        <span class="dt">print</span>(<span class="st">&quot;USAGE: </span><span class="ot">%s</span><span class="st"> &lt;id&gt;&quot;</span> % (sys.argv[<span class="dv">0</span>],))
        exit(<span class="dv">1</span>)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((<span class="st">&quot;0.0.0.0&quot;</span>, <span class="dv">23456</span>))
    <span class="kw">while</span> <span class="ot">True</span>:
        (data, (addr, port)) = sock.recvfrom(<span class="dv">1024</span>)
        <span class="kw">if</span> data.decode().strip() == sys.argv[<span class="dv">1</span>]:
            <span class="kw">break</span>
    <span class="dt">print</span>(addr)</code></pre>
<p>There’s nothing surprising about the above: we open a socket, bind it to a port, wait for packets, and exit only when the expected packet arrives. If we redo the experiment, we see:</p>
<pre class="sourceCode"><code>% whereis.py lily
192.168.0.6</code></pre>
<p>We still need to run the <code>netcat</code> broadcast on <code>lily</code> somehow. The simple solution is to add the command to its <a href="http://linux.die.net/man/1/crontab"><code>crontab(1)</code></a> so that it runs every minute:</p>
<pre class="sourceCode"><code>% crontab -l
* * * * * echo &quot;$(hostname)&quot; | nc.traditional -ub -w 1 255.255.255.255 23456</code></pre>
<p>As an example, we can now <code>ssh</code> into <code>lily</code> with:</p>
<pre class="sourceCode"><code>scvalex@alita ~ % ssh $(whereis.py lily)
scvalex@lily ~ %</code></pre>
<p>To recap, we wanted a way to find the address of a machine on the same network as us. We wrote a Python script that waits for a UDP packet with a certain content, and prints the source address of the packet. We then configured the remote machine to broadcast that packet using <code>netcat</code> and <code>cron</code>.</p>
<hr />
<p>We’ve already mentioned <code>netcat</code>’s unwieldy output, but a bigger problem is that there’s no standard <code>netcat</code>. The above commands work with the <em>traditional</em> <code>netcat</code> (which is <code>nc</code> on Gentoo, and <code>nc.traditional</code> on Debian); the flags for other <code>netcat</code> variants are probably different.</p>
<hr />
<p>Finally, note that our solution is basically a subset of <a href="https://en.wikipedia.org/wiki/Zeroconf">Zeroconf</a>. If you don’t want to get your hands dirty, or need more functionality, consider setting up <a href="https://en.wikipedia.org/wiki/Avahi_%28software%29">Avahi</a>.</p>

    </div>
  </article>

  <div id="postNavigation">
    <ul>

      <li><a id="prevLink" rel="prev" href="/posts/24/" title="Time Dilation">⋘ Prev</a></li>

      <li><a href="/posts/">All Posts</a></li>

      <li><a id="nextLink" rel="next" href="/posts/26/" title="Quines">Next ⋙</a></li>

    </ul>
  </div>
</div>

<footer id="postFooter">
  <ul>
    <li class="copyright">
      © 2013 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code>
    </li>
  </ul>
  <ul>
    <li>
      <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
        CC BY-SA</a>
    </li>
    <li>
      <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
    </li>
  </ul>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/atom.xml">RSS Feed</a></li>
  </ul>
</footer>

  </body>
</html>
