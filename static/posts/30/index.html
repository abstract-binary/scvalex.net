<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>Fun with MI in C++</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="/r/screen.css" media="screen">
    <link rel="stylesheet" href="/r/print.css" media="print">
    <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
    <link rel="stylesheet" href="/r/syntax.css">

    <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net" />
    <link rel="canonical" href="https://scvalex.net/posts/30/" />

    <meta name="description" content="The only features originally allowed into C++ were those which had efficient implementations. This sounds great, but the results were sometimes dubious. Consider the following program which uses multiple inheritance.…">

    <link rel="icon" type="image/png" href="/r/icon.png">

    <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
  </head>
  <body>
    <header id="postHeader">
  <a href="/">
    <img src="/r/title_post.png" width="111" height="120" alt="scvalex.net">
  </a>
</header>



<div id="content">
  <article id="standalonePost">
    <div class="postTitle">
      <h2 title="Stats
Words: 359
SLOC: 20
Effort: 91">30. Fun with MI in C++</h2>
      <time datetime="2013-05-04">2013-05-04</time>
    </div>

    <div class="post">
      <p>The only features originally allowed into C++ were those which had efficient implementations. This sounds great, but the results were sometimes dubious. Consider the following <a href="https://gist.github.com/scvalex/5504130">program</a> which uses multiple inheritance.</p>
<pre class="sourceCode cpp"><code class="sourceCode cpp"><span class="ot">#include &lt;cstdio&gt;</span>
<span class="ot">#include &lt;inttypes.h&gt;</span>

<span class="kw">using</span> <span class="kw">namespace</span> std;

<span class="kw">class</span> Left {
    <span class="dt">int32_t</span> a, b;
};

<span class="kw">class</span> Right {
    <span class="dt">int32_t</span> c, d;
};

<span class="kw">class</span> Bottom : <span class="kw">public</span> Left, <span class="kw">public</span> Right {
};

<span class="dt">int</span> main(<span class="dt">int</span> argc, <span class="dt">char</span> *argv[]) {
    Bottom *bottom = <span class="kw">new</span> Bottom();
    Left *left = bottom;
    Right *right = bottom;

    printf(<span class="st">&quot;left   -&gt; %p</span><span class="ch">\n</span><span class="st">&quot;</span>, left);
    printf(<span class="st">&quot;bottom -&gt; %p</span><span class="ch">\n</span><span class="st">&quot;</span>, bottom);
    printf(<span class="st">&quot;right  -&gt; %p</span><span class="ch">\n</span><span class="st">&quot;</span>, right);

    <span class="kw">if</span> (left == bottom &amp;&amp; bottom == right) {
        printf(<span class="st">&quot;left == bottom == right</span><span class="ch">\n</span><span class="st">&quot;</span>);
    } <span class="kw">else</span> {
        printf(<span class="st">&quot;!(left == bottom == right)</span><span class="ch">\n</span><span class="st">&quot;</span>);
    }

    <span class="kw">return</span> <span class="dv">0</span>;
}</code></pre>
<p>The first three lines of output are:</p>
<pre class="sourceCode"><code>left   -&gt; 0x24dd010
bottom -&gt; 0x24dd010
right  -&gt; 0x24dd018</code></pre>
<p>Right off the bat, we see that something happened to <code>right</code>. The statement <code>right = bottom</code> did something special: instead of setting <code>right</code> to <code>bottom</code>, it actually set it to <code>bottom + 8</code>. To understand why, we need to look at the memory representation for <code>*bottom</code>.</p>
<pre class="sourceCode"><code>0x24dd006 -&gt;  |    ...    |
0x24dd010 -&gt;  | int32_t a |   &lt;- bottom, left
0x24dd014 -&gt;  | int32_t b |
0x24dd018 -&gt;  | int32_t c |   &lt;- right
0x24dd022 -&gt;  | int32_t d |
0x24dd026 -&gt;  |    ...    |</code></pre>
<p>A <code>Bottom</code> object consists of a <code>Left</code> object, followed by a <code>Right</code> object. In turn, <code>Left</code> and <code>Right</code> objects consist of their own fields. So, a <code>Bottom</code> consists of four <code>int32_t</code>s.</p>
<p>One of the principles behind C++ was that access to object fields should be as fast as possible. In our case, it’s possible to avoid any indirection and make it as fast as normal variable access: since the compiler knows the memory layout, it replaces accesses to <code>bottom-&gt;a</code> by <code>*bottom</code>, accesses to <code>bottom-&gt;b</code> by <code>*(bottom + 4)</code>, and so on. This works fine for <code>bottom</code>, which actually points to a <code>Bottom</code> object, and for <code>left</code> because the first part of a <code>Bottom</code> object is a <code>Left</code> object.</p>
<p>What about <code>right</code>? We want <code>right-&gt;d</code> to be replaceable by <code>*(right+4)</code>, but it’s the <em>second</em> part of a <code>Bottom</code> object that is a <code>Right</code> object. For this to work, the statement <code>right = bottom</code> cannot simply assign <code>bottom</code> to <code>right</code>; instead, it casts <code>Bottom*</code> to <code>Right*</code> by assigning <code>bottom + &lt;offset of Right in Bottom&gt;</code> to <code>right</code>.</p>
<p>Now that we understand what’s happening, we can ask the next question: what is the last line of output? Is it <code>left == bottom == right</code>? Or <code>!(left == bottom == right)</code>? <span class="spoiler">It’s the former: the compiler somehow keeps track of what the pointers are supposed to be, and tries to hide the offsetting from the programmer.</span></p>
<p>I generally like C++, but this gives me mixed feelings. A part of me thinks that this is awesome, and another part screams in horror.</p>
<hr />
<p>This <a href="http://www.phpcompiler.org/articles/virtualinheritance.html">article</a> is a very good description of C++’s multiple and virtual inheritance with a focus on memory layouts.</p>

    </div>
  </article>

  <div id="postNavigation">
    <ul>

      <li><a id="prevLink" rel="prev" href="/posts/29/" title="Escape Analysis in Go">⋘ Prev</a></li>

      <li><a href="/posts/">All Posts</a></li>

      <li><a id="nextLink" rel="next" href="/posts/31/" title="IsString Abuse">Next ⋙</a></li>

    </ul>
  </div>
</div>

<footer id="postFooter">
  <ul>
    <li class="copyright">
      © 2013 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code>
    </li>
  </ul>
  <ul>
    <li>
      <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
        CC BY-SA</a>
    </li>
    <li>
      <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
    </li>
  </ul>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/atom.xml">RSS Feed</a></li>
  </ul>
</footer>

  </body>
</html>
