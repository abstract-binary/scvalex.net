<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>Escape Analysis in Go</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="/r/screen.css" media="screen">
    <link rel="stylesheet" href="/r/print.css" media="print">
    <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
    <link rel="stylesheet" href="/r/syntax.css">

    <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net" />
    <link rel="canonical" href="https://scvalex.net/posts/29/" />

    <meta name="description" content="Returning a pointer to a local variable is legal in Go. As a C programmer, the following looks like an error to me, but it’s perfectly alright in Go. func…">

    <link rel="icon" type="image/png" href="/r/icon.png">

    <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
  </head>
  <body>
    <header id="postHeader">
  <a href="/">
    <img src="/r/title_post.png" width="111" height="120" alt="scvalex.net">
  </a>
</header>



<div id="content">
  <article id="standalonePost">
    <div class="postTitle">
      <h2 title="Stats
Words: 400
SLOC: 11
Effort: 74">29. Escape Analysis in Go</h2>
      <time datetime="2013-05-02">2013-05-02</time>
    </div>

    <div class="post">
      <p>Returning a pointer to a local variable is legal in Go. As a C programmer, the following looks like an error to me, but it’s perfectly alright in Go.</p>
<pre class="sourceCode go"><code class="sourceCode go"><span class="kw">func</span> NewFile(fd <span class="dt">int</span>, name <span class="dt">string</span>) *File {
    f := File{fd, name, <span class="ot">nil</span>, <span class="dv">0</span>}
    <span class="kw">return</span> &amp;f
}</code></pre>
<p>Above we have a function that defines the local variable <code>f</code> as a <code>File</code> struct, and then returns <em>the address</em> of that local variable. If this were C, the memory layout would look like this:</p>
<pre class="sourceCode"><code>    The Stack

|      ...      |
|               |
+---------------+
|               |  \
|               |   } Stack frame of calling function
|               |  /
+---------------+
|               |  \
|  f : File     |   } Stack frame of NewFile
|               |  /
+---------------+</code></pre>
<p>If this were C, when we return from <code>NewFile</code>, its stack frame disappears along with the <code>f</code> defined in it. But this isn’t C. To quote <a href="http://golang.org/doc/effective_go.html#composite_literals">Effective Go</a>,</p>
<blockquote>
<p>Note that, unlike in C, it’s perfectly OK to return the address of a local variable; the storage associated with the variable survives after the function returns.</p>
</blockquote>
<p>This begs the question, how? How could the storage of local variables survive function returns? The <a href="http://golang.org/doc/faq#stack_or_heap">FAQ</a> explains:</p>
<blockquote>
<p>When possible, the Go compilers will allocate variables that are local to a function in that function’s stack frame. However, if the compiler cannot prove that the variable is not referenced after the function returns, then the compiler must allocate the variable on the garbage-collected heap to avoid dangling pointer errors. In the current compilers, if a variable has its address taken, that variable is a candidate for allocation on the heap. However, a basic escape analysis recognizes some cases when such variables will not live past the return from the function and can reside on the stack.</p>
</blockquote>
<p>In other words, the Go compiler may allocate all local variables, whose address is taken, on the garbage-collected heap (it actually did this until <a href="https://groups.google.com/d/msg/golang-nuts/TN8mhQJBlZ8/5GCScT8jUigJ">late 2011</a>); however, it’s clever enough to recognize some cases when it’s safe to allocate them on the stack instead.</p>
<p>The code that does the “escape analysis” lives in <a href="https://code.google.com/p/go/source/browse/src/cmd/gc/esc.c?name=release-branch.go1"><code>src/cmd/gc/esc.c</code></a>. Conceptually, it tries to determine if a local variable escapes the current scope; the only two cases where this happens are when a variable’s address is returned, and when its address is assigned to a variable in an outer scope. If a variable escapes, it has to be allocated on the heap; otherwise, it’s safe to put it on the stack.</p>
<p>Interestingly, this applies to <code>new(T)</code> allocations as well. If they don’t escape, they’ll end up being allocated on the stack. Here’s an example to clarify matters:</p>
<pre class="sourceCode go"><code class="sourceCode go"><span class="kw">var</span> intPointerGlobal *<span class="dt">int</span> = <span class="ot">nil</span>

<span class="kw">func</span> Foo() *<span class="dt">int</span> {
    anInt0 := <span class="dv">0</span>
    anInt1 := <span class="fu">new</span>(<span class="dt">int</span>)

    anInt2 := <span class="dv">42</span>
    intPointerGlobal = &amp;anInt2

    anInt3 := <span class="dv">5</span>

    <span class="kw">return</span> &amp;anInt3
}</code></pre>
<p>Above, <code>anInt0</code> and <code>anInt1</code> do not escape, so they are allocated on the stack; <code>anInt2</code> and <code>anInt3</code> escape, and are allocated on the heap.</p>

    </div>
  </article>

  <div id="postNavigation">
    <ul>

      <li><a id="prevLink" rel="prev" href="/posts/28/" title="A Stopping Problem">⋘ Prev</a></li>

      <li><a href="/posts/">All Posts</a></li>

      <li><a id="nextLink" rel="next" href="/posts/30/" title="Fun with MI in C++">Next ⋙</a></li>

    </ul>
  </div>
</div>

<footer id="postFooter">
  <ul>
    <li class="copyright">
      © 2013 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code>
    </li>
  </ul>
  <ul>
    <li>
      <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
        CC BY-SA</a>
    </li>
    <li>
      <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
    </li>
  </ul>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/atom.xml">RSS Feed</a></li>
  </ul>
</footer>

  </body>
</html>
