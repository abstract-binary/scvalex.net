{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = "github:ipetkov/crane";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      rust-overlay,
      crane,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };

        craneLib = (crane.mkLib pkgs).overrideToolchain (
          p:
          p.rust-bin.stable.latest.default.override {
            extensions = [
              "rust-src"
              "rust-analyzer"
              "clippy"
            ];
            # targets = [ "wasm32-unknown-unknown" ];
          }
        );

        commonArgs = {
          src = pkgs.lib.cleanSourceWith {
            src = ./.;
            name = "source";
          };
          strictDeps = true;
          buildInputs = [ ];
          version = "0.1.0";
          pname = "site";
        };
      in
      rec {
        # `nix build`
        packages.site = craneLib.buildPackage (commonArgs);
        defaultPackage = packages.site;

        packages.container = pkgs.dockerTools.buildLayeredImage {
          name = "scvalex.net";
          tag = "flake";
          created = "now";
          config = {
            ExposedPorts = {
              "80/tcp" = { };
            };
            Entrypoint = [ "${packages.site}/bin/site" ];
            Cmd = [
              "serve"
              "--dist-dir"
              "${packages.siteDist}"
              "--listen-on"
              "0.0.0.0:80"
            ];
          };
        };

        packages.node_modules = pkgs.stdenv.mkDerivation {
          name = "scvalex.net-node_modules";
          src = ./js;
          nativeBuildInputs = with pkgs; [
            cacert
          ];
          buildPhase = ''
            HOME=. ${pkgs.yarn}/bin/yarn install --frozen-lockfile
            # sed -i -e "s|#!/usr/bin/env node|#! ${pkgs.nodejs}/bin/node|" ./node_modules/.bin/perfectionist ./node_modules/.bin/tailwindcss
          '';
          installPhase = ''
            mv ./node_modules/ $out
          '';
          outputHashAlgo = "sha256";
          outputHashMode = "recursive";
          outputHash = "sha256-kt52gCDMsMnIvbtaB61NxTAeQzlWppi1Wzb//XI8PNE=";
        };

        packages.siteDist = pkgs.stdenv.mkDerivation {
          name = "scvalex.net-dist";
          nativeBuildInputs = [
            pkgs.nodejs
            pkgs.html-tidy
            pkgs.nix
          ];
          src = ./.;
          buildPhase = ''
            DIST_HASH=$(nix --extra-experimental-features nix-command hash path ./dist/)
            export NODE_PATH=${packages.node_modules}
            ln -s ${packages.node_modules} ./node_modules
            cp ./node_modules/katex/dist/katex.min.css ./node_modules/katex/dist/katex.min.js static/r/
            cp ./node_modules/katex/dist/contrib/auto-render.min.js static/r/katex-auto-render.min.js
            cp ./node_modules/katex/dist/fonts/* static/r/fonts
            mkdir -p styles_out
            ${packages.site}/bin/site gen
            if [[ "$DIST_HASH" != "$(nix --extra-experimental-features nix-command hash path ./dist/)" ]]; then
              echo "Dist dir changed during build. Aborting"
              exit 1
            fi
            # ${pkgs.python310}/bin/python generate-sitemap.py
            # ${pkgs.html-tidy}/bin/tidy -modify -quiet -wrap 0 -xml -indent --tidy-mark false dist/sitemap.xml
          '';
          installPhase = ''
            mv ./dist $out
          '';
        };

        # `nix run`
        apps.site = flake-utils.lib.mkApp {
          drv = packages.site;
        };
        defaultApp = apps.site;

        # `nix develop`
        devShell = craneLib.devShell {
          packages = with pkgs; [
            difftastic
            pre-commit
            tokei
            cargo-nextest
            cargo-outdated
            just
            git-crypt

            html-tidy
            inkscape
            inotify-tools
            nodejs_18
            optipng
            plantuml
            scour
            yarn
            zip
            dive
            skopeo
          ];

          GIT_EXTERNAL_DIFF = "${pkgs.difftastic}/bin/difft";
        };
      }
    );
}
