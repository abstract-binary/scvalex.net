site
====

The `site` app is a fairly simple website generator.  The primary goal
is simplicity and customizability through changing the code.

Because `site` always generates the entire result (i.e. there are no
partial builds), the architecture is change-trigger based, rather than
update-if-deps-changed based.  As in, a change to a source file
triggers changes to output files.  Output files are served/generated
ahead of time, and no dependencies are checked when a request is made.

For inputs with both metadata and content, these are tracked
separately for triggering rebuilds.
