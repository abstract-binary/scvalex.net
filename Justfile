BRANCH := `git branch --show-current`

export NODE_PATH := "./js/node_modules/"

default:
	just --choose

# Build the website part
build-site:
	yarn install --frozen-lockfile --cwd ./js/
	cp {{NODE_PATH}}/katex/dist/katex.min.css ./js/node_modules/katex/dist/katex.min.js static/r/
	cp {{NODE_PATH}}/katex/dist/contrib/auto-render.min.js static/r/katex-auto-render.min.js
	cp {{NODE_PATH}}/katex/dist/fonts/* static/r/fonts
	mkdir -p styles_out
	./target/release/site gen
	./generate-sitemap.py
	tidy -modify -quiet -wrap 0 -xml -indent --tidy-mark false dist/sitemap.xml

# Build the rust site generator
build-rust:
	cargo fmt --all -- --check
	cargo build --all --release

# Build and run the site gen in watch mode
dev: build-rust
	./target/release/site gen --watch

# Build (debug) and run the site gen in watch mode
dev-debug:
	cargo build --all
	./target/debug/site --debug gen --watch

# Serve the static site
serve: build-rust
	./target/release/site serve

# Type-check and lint the code
clippy:
	cargo clippy --workspace

# Run Rust tests
test:
	cargo nextest run

# Clean the generated HTML (use after removing pages)
clean-html:
	rm -rf dist/

# Clean everything
clean:
	rm -rf node_modules/ dist/
	cargo clean

# Build a container
container:
	nix build .#container
	podman load < ./result

# Pull a container from the remote registry
pull-container:
	podman pull registry.gitlab.com/abstract-binary/scvalex.net:{{BRANCH}}

# Run the container
up:
	podman container rm scvalex.net || true
	podman run --rm --name scvalex.net -p 8080:80 "scvalex.net:flake"
