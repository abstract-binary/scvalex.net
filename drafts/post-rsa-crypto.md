---
title: "The basic math behind public key crypto"
date: "2021-08-01"
layout: "../../layouts/BlogPost.astro"
drafts: 1
sloc: 0
math: true
---

Cryptography is hard and the details are notoriously tricky to get
right.  At the same time, the subject is broad, and it seems like
algorithms are introduced or deprecated every year.  That said, the
basic ideas are fairly easy to grok.  In this post, we look at the
basic math behind public key crypto (specifically the [][2] [RSA
cryptosystem][1]).

[1]: https://en.wikipedia.org/wiki/RSA_(cryptosystem)
[2]: https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange

<!-- more -->

### Some terminology

We're going to use the following consistently through this post:

- There are three people: Alice, Bob, and Carmen.  Alice wants to send
  a message to Bob and either sign it or encrypt it.  Carmen does the
  actual message delivery, and Bob wants to be sure Carmen didn't
  alter or read Alice's message (in the encryption case).
- Alice has private key $k_A$ and public key $P_A$.  Only Alice knows
  $k_A$, and everybody knows $P_A$.  Similarly, Bob has private key
  $k_B$ and public key $P_B$.  So, Carmen knows $P_A$ and $P_B$, but
  not $k_A$ and $k_B$.  These keys are numbers with specific
  properties which we'll talk about later.

### Basic operations modulo something

Every computation we're going to do will be in modular arithmetic
(usually in base $n$).  Conceptually, this means that for any result
we get, we'll then compute $mod n$.  To avoid having to write the $mod
n$ frequently, we'll just put it at the end of each line.  We'll also
use $\equiv$ instead of $=$ to further indicate that this is not
normal arithmetic.  For instance

$$
{`
\begin{align*}
16 mod 10 &= 6 mod 10 \\
16 &\equiv 6 (mod 10) \\
5+5+3 &\equiv 6 (mod 10) \\
n-1 &\equiv n-1 (mod n) \\
n &\equiv 0 (mod n) \\
n+1 &\equiv 1 (mod n)
\end{align*}
`}
$$

Addition and multiplication work as normal.  We can do the modulo
operation at the end or at the intermediate steps; it's all the same.

Subtraction works as normal as well, except that if we get a negative
number, we have to add the modulo base to it repeatedly until it
becomes a positive number.  In other words, the only valid results
$mod n$ are in the inclusive range $[0, n-1]$.  For any result outside
the range, we have to either add or subtract $n$ repeatedly until the
result falls in the range.

$$
{`
\begin{align*}
10-3 &\equiv 7 (mod 10) \\
10-13 &\equiv 7 (mod 10)
\end{align*}
`}
$$

Division is where things get tricky.  In theory, it works as normal.
That is, if $x / y \equiv z (mod n)$, then $z$ is the number which
satisfies $z * y \equiv x (mod n)$.  Additionally, $z$ must be in the
inclusive range $[0, n-1]$.  In practice, finding $z$ is hard.

# TODO Why is division hard?

### Raising to the power modulo something

The basic computational operation we'll be using is raising to the
power modulo something.  That is, given $m$, $d$, and $n$, we need to
efficiently compute $x$:

$$
{`
\begin{align*}
x &\equiv m^d &(\text{mod } n) \\
  &\equiv m * m * ... * m &(\text{mod } n) \\
  &= ((((m * m) \text{ mod } n) * m) \text{ mod } n) ...
\end{align*}
`}
$$

We can compute $m^d$ in logarithmic time by observing that ${`m^{2x} =
(m^x)^2`}$ and expressing $d$ as something like $2*(2*(2*1)+1)...$.
Computing everything modulo $n$ doesn't change anything, other than us
having to modulo the intermediate results.

```python
def fast_pow(m, d, n):
    """Computes: m^d (mod n)"""
    if d < 0:
        raise "Can't fast_pow to a negative power"
    elif d == 0:
        return 1
    elif d == 1:
        return m % n
    elif d % 2 == 1:
        # d = 2*x+1
        # => m^d = m^(2*x+1) = m * m^x * m^x
        z = fast_pow(m, d//2, n)
        return (m * z * z) % n
    else:
        # d = 2*x
        # => m^d = m^(2*x) = m^x * m^x
        z = fast_pow(m, d//2, n)
        return (z * z) % n
```

### Discrete logarithm modulo something

So, given $m$, $d$, and $n$, we can compute ${`b = m^d\text{ mod n}`}$
in logarithmic time.  Can we reverse this and, given $b$, $d$, and
$n$, compute ${`m = \text{log}_d(b) \text{ mod n}`}$?  No, we can't.
This is the [discrete logarithm problem][2], and there's no known
algorithm to solve it in the general case.

[2]: https://en.wikipedia.org/wiki/Discrete_logarithm#Algorithms

### Public and private keys

Let's say Alice has private key $k_A$ and public key $K_A$.  Only
Alice knows $k_A$, and everybody knows $K_A$. She wants to sign text
$T$ in a way that proves to Bob that the signature was hers and that
nobody else (including Bob) could have faked it.

She computes S=T^k_A?

### Diffie-Hellman exchange

From (Zero to Monero):

A basic Diffie-Hellman [52] exchange of a shared secret between Alice and Bob could take place
in the following manner:
1. Alice and Bob generate their own private/public keys (k A , K A ) and (k B , K B ). Both publish
or exchange their public keys, and keep the private keys for themselves.
2. Clearly, it holds that
S = k A K B = k A k B G = k B k A G = k B K A
Alice could privately calculate S = k A K B , and Bob S = k B K A , allowing them to use this
single value as a shared secret.
For example, if Alice has a message m to send Bob, she could hash the shared secret h =
H(S), compute x = m + h, and send x to Bob. Bob computes h 0 = H(S), calculates
m = x − h 0 , and learns m.
An external observer would not be able to easily calculate the shared secret due to the ‘Diffie-
Hellman Problem’ (DHP), which says finding S from K A and K B is very difficult. Also, the DLP
prevents them from finding k A or k B . 15
