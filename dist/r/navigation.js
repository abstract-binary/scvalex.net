document.addEvent("keyup", function(e) {
    switch(e.key) {
        case "left":
            prevLink = $("prevLink");
            if (prevLink) {
                window.location = prevLink.href;
            }
            break;
        case "right":
            nextLink = $("nextLink");
            if (nextLink) {
                window.location = nextLink.href;
            }
            break;
    }
});
