// https://www.kirupa.com/html5/get_element_position_using_javascript.htm
function offsetTop(el) {
    var yPos = 0;

    while (el) {
        if (el.tagName == "BODY") {
            // deal with browser quirks with body/window/document and page scroll
            var yScroll = el.scrollTop || 0;

            yPos += (el.offsetTop - yScroll + el.clientTop);
        } else {
            // for all other non-BODY elements
            yPos += (el.offsetTop - el.scrollTop + el.clientTop);
        }

        el = el.offsetParent;
    }
    return yPos;
}

let headings = [];

function updateTocHighlight() {
    let toc = document.querySelector("#floating-toc");
    let foundHeading = null;
    for (let heading of headings) {
        if (window.scrollY + 200 >= heading.offset) {
            foundHeading = heading;
        } else {
            break;
        }
    }
    if (foundHeading) {
        const re = new RegExp(`/#${foundHeading.id}$`);
        for (let link of toc.querySelectorAll("a")) {
            if (re.test(link.href)) {
                link.classList.add("selected");
            } else {
                link.classList.remove("selected");
            }
        }
    } else {
        toc.querySelectorAll("a").forEach((elem) => {
            elem.classList.remove("selected");
        });
    }
}

function configureToc() {
    if (document.querySelector("html").clientWidth >= 1280) {
        let fixedToc = document.querySelector("#fixed-toc");
        if (fixedToc) {
            fixedToc.style.display = "none";
        }
        let toc = document.querySelector("#floating-toc");
        if (toc) {
            toc.style.display = "block";
            for (let heading of document.querySelectorAll("h3,h4")) {
                if (heading.id) {
                    headings.push({id: heading.id, offset: offsetTop(heading)});
                }
            }
            document.addEventListener("scroll", function(_) {
                updateTocHighlight();
            });
            updateTocHighlight();
        }
    } else {
        let fixedToc = document.querySelector("#fixed-toc");
        if (fixedToc) {
            fixedToc.style.display = "block";
        }
        let toc = document.querySelector("#floating-toc");
        if (toc) {
            toc.style.display = "none";
        }
    }
}

window.addEventListener("load", (_) => {
    configureToc();
    window.addEventListener("resize", (_) => {
        configureToc();
    });
});
