function watch_for_live_reload() {
    const ws = new WebSocket("ws://" + location.host + "/live-reload");;
    ws.onmessage = (event) => {
        if (event.data === "reload") {
            ws.close();
            document.location.reload();
        } else {
            console.log("Unhandled live-reload event:", event);
        }
    };
    ws.onclose = (err) => {
        console.log("Live-reload socket error", err);
        setTimeout(watch_for_live_reload, 1000);
    };
    const lrSource = new EventSource("/live-reload");
}
watch_for_live_reload();
