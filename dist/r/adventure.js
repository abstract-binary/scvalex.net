/* Logging */

function log(s) {
    if (console && console.log) {
        console.log(s);
    }
}

/* Countdown latch */

function Countdown(n, f) {
    var c = new Object();

    c.tick = function () {
        if (n > 0) {
            n--;

            if (n == 0)
                f();
        } else {
            throw new Error("ticked non active semaphore");
        }
    };

    Object.freeze(c);
    return c;
}

/* Scene */

function Scene(canvas) {
    var s = new Object();

    s.canvas = canvas;
    s.width = canvas.width;
    s.height = canvas.height;
    s.context = canvas.getContext("2d");
    s.sprites = {};
    canvas.addEvent("click", function (event) {
        var x = event.page.x - canvas.offsetLeft;
        var y = event.page.y - canvas.offsetTop;

        Object.each(s.sprites, function (sprite) {
            if (sprite.onClick != undefined) {
                if (sprite.x <= x && x <= sprite.x + sprite.width
                    && sprite.y <= y && y <= sprite.y + sprite.height)
                    sprite.onClick(x, y);
            }
        });
    });
    s.addSprite = function(sprite) {
        if (s.sprites[sprite.id] != undefined)
            throw new Error("sprite " + sprite.id + " already exists");
        s.sprites[sprite.id] = sprite;
    };
    s.removeSprite = function(id) {
        if (s.sprites[id] == undefined)
            throw new Error("sprite " + id + " not found");
        delete s.sprites[id];
    };
    s.redraw = function() {
        s.context.fillStyle = "rgb(20, 20, 20)";
        s.context.fillRect(0, 0, s.canvas.width, s.canvas.height);

        // FIXME Sort by z-coordinate first
        Object.each(s.sprites, function (sprite) {
            sprite.draw(s.context);
        });
    }

    return s;
}

/* Sprites */

function Sprite(id, x, y, width, height, draw) {
    var s = new Object();

    s.id = id;
    s.x = x;
    s.y = y;
    s.width = width;
    s.height = height;

    s.draw = function (ctx) {
        draw(ctx, s.x, s.y);
    };

    return s;
}

// FIXME img should not be a parameter here
function Coin(scene, img, say) {
    var config = window.localStorage;
    if (config.getItem("coinState") == null)
        config.setItem("coinState", 0);

    var c = new Sprite(
        "coin",
        (scene.width - img.height) / 2,
        (scene.height - img.width) / 3,
        img.height,
        img.width,
        function (ctx, x, y) {
            var state = config.getItem("coinState");
            if (state < 2) {
                ctx.drawImage(img, x, y);
            } else {
                ctx.beginPath();
                ctx.strokeStyle = "rgb(160, 160, 160)";
                ctx.lineWidth = 8;
                ctx.arc(c.x + c.width / 2,
                        c.y + c.height / 2,
                        c.width / 2,
                        0.0,
                        Math.PI * 2.0,
                        false);
                ctx.stroke();
            }
        });

    c.onClick = function(x, y) {
        var state = config.getItem("coinState");
        if (state == 0) {
            say("The adventurer observes the coin.  \
He notes the glistening metal, the fine engravings, and concludes that this is a normal coin.  \
In fact, he is surprised by the utter banality of the coin.");
            state = 1;
        } else if (state == 1) {
            say("The adventurer takes the coin.");
            state = 2;
        } else if (state == 2) {
            say("The adventurer observes the desolate and empty space, \
much like his soul after all his dreams had been crushed.");
            state = 3;
        } else if (state == 3) {
            say("*wind blowing*");
        } else {
            throw new Error("unknown state " + state);
        }
        config.setItem("coinState", state);
        scene.redraw();
    };

    return c;
}

/* Resource loading */

function loadImages(images, f) {
    var loadedImages = {};
    var countdown = new Countdown(Object.keys(images).length, function () {
        f(loadedImages);
    });
    for (imgName in images) {
        var img = new Image();
        img.src = images[imgName];
        img.addEvent("load", function () {
            loadedImages[imgName] = img;
            countdown.tick();
        });
    }
}

/* Narration */

function Narration(container) {
    var n = new Object();

    n.say = function (text) {
        var li = new Element("li", {
            html: text
        });
        container.empty();
        li.inject(container);
    }

    return n;
}

/* Main */

document.addEvent("domready", function() {
    var canvas = $("drawingCanvas")
    var scene = new Scene(canvas);
    var narration = new Narration($("narrationArea"));

    var images = {
        "fivePence": "../../r/5p.png"
    };
    loadImages(images, function (images) {
        var coin = new Coin(scene, images["fivePence"], narration.say);
        scene.addSprite(coin);
        scene.redraw();

        log("Ready player one");
    });
});
