function storageGet(key) {
    const path = document.location.pathname;
    return window.localStorage.getItem(`${path}/${key}`);
}

function storageSet(key, value) {
    const path = document.location.pathname;
    window.localStorage.setItem(`${path}/${key}`, value);
}

async function digestMessage(crypto, message) {
    const encoder = new TextEncoder();
    const data = encoder.encode(message);
    const hash = await crypto.digest('SHA-256', data);
    return hash;
}

async function solveChallenge(crypto, prompt, path, feedback, comment) {
    let answer = '';
    let attempts = 0;
    for (;;) {
        answer = '' + Math.random();
        let challenge_and_answer =
            prompt.nonce + "Path" + path + "Feedback" + feedback
            + "Comment" + comment + "Answer" + answer;
        attempts += 1;
        if (attempts % 1000 == 0) {
            console.log("[captcha] Attempts made:", attempts);
        }
        let digest = new DataView(await digestMessage(
            crypto,
            challenge_and_answer
        ));
        if (digest.getUint32(0, false) < prompt.difficulty) {
            console.log(`[captcha] Solved after ${attempts} attempts`);
            break;
        }
    }
    return answer;
}

async function sendFeedback(formTrigger) {
    let feedbackElement = document.querySelector("input[name='rating']:checked");
    let feedback = feedbackElement && feedbackElement.value || "No feedback";
    let commentElement = document.querySelector("textarea[name='comment']");
    let comment = commentElement && commentElement.value || "No comment";
    let ratingWrapper = document.querySelector("#ratingWrapper");
    if ((feedbackElement && feedbackElement.value) || (commentElement && commentElement.value)) {
        if (ratingWrapper){
            ratingWrapper.classList.remove("border-2");
        }
        if (formTrigger) {
            formTrigger.disabled = true;
        }
        let path = document.location.pathname;
        fetch("/feedback")
            .then(response => response.json())
            .catch(error => {
                console.log("Error getting challenge", error);
                if (formTrigger) {
                    formTrigger.disabled = false;
                }
            })
            .then(challenge => solveChallenge(crypto.subtle, challenge.prompt, path, feedback, comment)
                  .then(answer => {
                      return {challenge: challenge, answer: answer};
                  }))
            .then(({challenge, answer}) =>
                fetch("/feedback", {
                    method: "post",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        "feedback": feedback,
                        "prompt": challenge.prompt,
                        "signature": challenge.signature,
                        "answer": answer,
                        "comment": comment,
                        "path": path,
                    })
                }))
            .then(response => response.text())
            .catch(error => {
                console.log("Error sending feedback", error);
                if (formTrigger) {
                    formTrigger.disabled = false;
                }
            })
            .then(data => {
                console.log("Feedback response:", data);
                storageSet("feedback", feedback);
                if (comment != "No comment") {
                    storageSet("comment", comment);
                }
                const sentTimestamp = Date.now();
                storageSet("sentTimestamp", sentTimestamp);
                document.querySelector("#sentTimestampLabel").innerText =
                    "Sent at " + (new Date(sentTimestamp)).toLocaleString();
                if (formTrigger) {
                    formTrigger.value = "Sent";
                }
            });
    } else {
        if (ratingWrapper) {
            ratingWrapper.classList.add("border-2");
        }
    }
}

if (typeof window !== "undefined") {
    const oldFeedback = storageGet("feedback");
    if (oldFeedback) {
        for (let el of document.querySelectorAll("input[name='rating']")) {
            if (el.value == oldFeedback) {
                el.checked = true;
                break;
            }
        }
    }
    let commentElement = document.querySelector("textarea[name='comment']");
    const oldComment = storageGet("comment");
    if (oldComment) {
        commentElement.value = oldComment;
    }
    const oldSentTimestamp = storageGet("sentTimestamp");
    if (oldSentTimestamp) {
        document.querySelector("#sentTimestampLabel").innerText =
            "Sent at "
            + (new Date(new Number(oldSentTimestamp))).toLocaleString();
    }
    for (let el of document.querySelectorAll("input[name='rating']")) {
        el.addEventListener("click", (event) => {
            sendFeedback();
        });
    }
    window.addEventListener("DOMContentLoaded", (event) => {
        let form = document.querySelector("#feedbackForm");
        let formTrigger = document.querySelector("input[value='Send']");
        commentElement.addEventListener("keydown", function(event) {
            if ((event.ctrlKey || event.metaKey)
                && (event.keyCode == 13 || event.keyCode == 10)) {
                form.dispatchEvent(new SubmitEvent("submit", {
                    submitter: formTrigger,
                    cancelable: true
                }));
            }
        });
        form.addEventListener("submit", function(event) {
            event.preventDefault();
            sendFeedback(formTrigger);
        });
    });
}

if (module === undefined) {
    var module = {
        exports: {},
    };
}

module.exports = {
    solveChallenge: solveChallenge,
};
