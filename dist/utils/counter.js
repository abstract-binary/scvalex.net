var svg = React.createFactory("svg");
var g = React.createFactory("g");
var circle = React.createFactory("circle");
var text = React.createFactory("text");

var Counter = React.createClass({
  getInitialState: function() {
    return {clicks: 0, locked: false};
  },
  increment: function(event) {
    event.stopPropagation();
    event.preventDefault();
    if (this.state.locked) {
      return;
    }
    this.setState({clicks: this.state.clicks + 1, locked: true});
    setTimeout(_.bind(function() {
      console.log("unlocked state");
      this.setState({locked: false});
    }, this), 400);
  },
  render: function() {
    return svg(_.extend({style: {cursor: "pointer"},
                         onClick: this.increment, onTouchStart: this.increment}, this.props),
               [g({transform: "translate(" + (this.props.width / 2) + ", " + (this.props.height / 2) + ")"},
                  [circle({cx: 0, cy: 0, r: this.props.radius,
                           fill: "none", stroke: "#333", strokeWidth: 40}),
                   text({x: 0, y: 0,
                         style: {fontFamily: "monospace",
                                 fontSize: this.props.radius,
                                 fill: "#333",
                                 stroke: "#333",
                                 textAnchor: "middle",
                                 dominantBaseline: "middle"}},
                        this.state.clicks)])]);
  },
});
var counter = React.createFactory(Counter);

function counterInit() {
  var width = window.innerWidth;
  var height = window.innerHeight - 20;
  var radius = _.min([width, height]) / 3;
  React.initializeTouchEvents(true);
  React.render(
    counter({height: height, width: width, radius: radius}),
    document.getElementById("counterContainer")
  );
}
