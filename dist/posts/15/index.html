<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Phantom Types</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <link rel="stylesheet" href="/r/screen.css" media="screen">
  <link rel="stylesheet" href="/r/print.css" media="print">
  <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
  <link rel="stylesheet" href="/r/syntax.css">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/15/">
  <meta name="description" content="There are two ways of looking at the type-system of a language: as a set of rules which must be followed for the program to compile, and as a tool…">
  <link rel="icon" type="image/png" href="/r/icon.png">
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <header id="postHeader">
    <a href="/"><img src="/r/title_post.png" width="111" height="120" alt="scvalex.net"></a>
  </header>
  <div id="content">
    <article id="standalonePost">
      <div class="postTitle">
        <h2 title="Stats Words: 310 SLOC: 9 Effort: 0">15. Phantom Types</h2><time datetime="2012-12-17">2012-12-17</time>
      </div>
      <div class="post">
        <p>There are two ways of looking at the type-system of a language: as a set of rules which must be followed for the program to compile, and as a tool to make code more expressive. In this post, we talk about phantoms types, which is one way of doing the latter.</p>
        <p>To quote the <a href="http://www.haskell.org/haskellwiki/Phantom_type">Hakell Wiki</a>:</p>
        <blockquote>
          <p>A phantom type is a parametrised type whose parameters do not all appear on the right-hand side of its definition.</p>
        </blockquote>
        <p>That’s a bit obtuse, but an <a href="https://en.wikibooks.org/wiki/Haskell/Phantom_types">example</a> will make it clear:</p>
        <pre class="sourceCode haskell"><code class="sourceCode haskell"><span class="kw">data</span> <span class="dt">Zero</span> <span class="fu">=</span> <span class="dt">Zero</span>
<span class="kw">data</span> <span class="dt">Succ</span> a <span class="fu">=</span> <span class="dt">Succ</span> a

<span class="kw">data</span> <span class="dt">Vec</span> n a <span class="fu">=</span> <span class="dt">Vec</span> [a]

<span class="kw">type</span> <span class="dt">Vec2d</span> a <span class="fu">=</span>
    <span class="dt">Vec</span> (<span class="dt">Succ</span> (<span class="dt">Succ</span> <span class="dt">Zero</span>)) a
<span class="kw">type</span> <span class="dt">Vec3d</span> a <span class="fu">=</span>
    <span class="dt">Vec</span> (<span class="dt">Succ</span> (<span class="dt">Succ</span> (<span class="dt">Succ</span> <span class="dt">Zero</span>))) a

<span class="ot">transpose ::</span> <span class="dt">Vec2d</span> a <span class="ot">-&gt;</span> <span class="dt">Vec2d</span> a
<span class="ot">add ::</span> <span class="dt">Vec</span> n a <span class="ot">-&gt;</span> <span class="dt">Vec</span> n a <span class="ot">-&gt;</span> <span class="dt">Vec</span> n a</code></pre>
        <p><code>Zero</code> and <code>Succ</code> are the usual encoding of <a href="https://en.wikipedia.org/wiki/Peano_numbers">Peano Numbers</a>. <code>Vec n a</code> represents an <code>n</code>-dimensional vector with elements of type <code>a</code> – note that <code>n</code> does not appear on the right-hand side of the data declaration. Using these types, we define <code>Vec2d</code> and <code>Vec3d</code> such that they cannot be unified with each other. Now, we can define <code>transpose</code> such that it only works on 2-dimensional vectors, and <code>add</code> such that it only works on vectors with the same size. Using these the wrong way leads to compile-time errors; so, we’ve gotten more static checks out of the language.</p>
        <p>There are many other things you can do with phantom types: You can use them to implement a type-safe variant of the <a href="https://en.wikipedia.org/wiki/Builder_pattern">Builder</a> design pattern in <a href="http://blog.rafaelferreira.net/2008/07/type-safe-builder-pattern-in-scala.html">Scala</a>, or in <a href="http://james-iry.blogspot.co.uk/2010/10/phantom-types-in-haskell-and-scala.html">Haskell</a>. You can use them to <a href="http://www.haskell.org/haskellwiki/Phantom_type#The_use_of_a_type_system_to_guarantee_well-formedness.">embed a language</a> in Haskell, and ensure that expressions are well-formed. You can use them to distinguish between two variants of the same data-structure – in <a href="http://www.haskell.org/haskellwiki/Phantom_type#Simple_examples">this</a> case, validated and unvalidated input have different types, but share the same definition. You can use them to ensure that <a href="http://neilmitchell.blogspot.co.uk/2007/04/phantom-types-for-real-problems.html">needed operations are executed</a> in the right order. You can even use them to <a href="http://ocaml.janestreet.com/?q=node/11">enforce access control</a> on data structures.</p>
        <p>These aren’t just a Haskell idiom; you can use phantom types in <a href="http://gabrielsw.blogspot.ro/2012/09/phantom-types-in-java.html">Java</a>, <a href="http://james-iry.blogspot.co.uk/2010/10/phantom-types-in-haskell-and-scala.html">Scala</a>, <a href="http://ocaml.janestreet.com/?q=node/11">OCaml</a>, and most languages that have type systems.</p>
      </div>
    </article>
    <div id="postNavigation">
      <ul>
        <li>
          <a id="prevLink" rel="prev" href="/posts/14/" title="Looper">⋘ Prev</a>
        </li>
        <li>
          <a href="/posts/">All Posts</a>
        </li>
        <li>
          <a id="nextLink" rel="next" href="/posts/16/" title="Debugging Field Guide">Next ⋙</a>
        </li>
      </ul>
    </div>
  </div>
  <footer id="postFooter">
    <ul>
      <li class="copyright">© 2012 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code></li>
    </ul>
    <ul>
      <li>
        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a>
      </li>
      <li>
        <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
      </li>
    </ul>
    <ul>
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/atom.xml">RSS Feed</a>
      </li>
    </ul>
  </footer>
</body>
</html>
