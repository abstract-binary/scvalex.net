<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">
  <title>Binary Numbers: Counting Bits « Computer programming</title>
  <meta name="description" content="In a previous article, I described the basics of binary arithmetic and gave a function to display the binary representation of a number. Here, we'll look at several ways to count the set (1) bits in a number.">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp9/">
  <link rel="stylesheet" href="/r/wp-style-11.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li>
        <a id="prevLink" rel="prev" href="/posts/cp8/">⋘ Prev</a>
      </li>
      <li>
        <a href="/posts/">All Posts</a>
      </li>
      <li>
        <a id="nextLink" rel="next" href="/posts/cp10/">Next ⋙</a>
      </li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-11-06</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: Binary Numbers: Counting&nbsp;Bits">Binary Numbers: Counting&nbsp;Bits</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p>In a previous <a href="/posts/cp7/">article</a>, I described the basics of binary arithmetic and gave a function to display the binary representation of a number. Here, we’ll look at several ways to count the set (<em>1</em>) bits in a number.</p>
              <p>First of all, why would you want to count bits? Bitsets. If you use bitsets as a fast <a href="https://en.wikipedia.org/wiki/Set">set</a> implementation, you might want to find out how many elements there are in the set. I used this in a <a href="/posts/cp8/">sudoku programme</a> to memorise which digits can’t be placed in a particular cell. Bit counting functions are also used extensively in graphics (bitmap manipulations).</p>
              <p>Here’s <em>22</em> in binary:<br>
              <code>00010110</code></p>
              <p>From the binary representation, it’s obvious that there are <em>3</em> set bits and <em>5</em> unset bits. How do you count them? I’ll give three methods.</p>
              <h5>Classic, let’s iterate through every bit, solution</h5>
              <p>The idea behind this is simple: take every bit in the number (<em>n</em>) and check if it’s <em>1</em>. To do this, you can simply use a variable (<em>i</em>):</p>
              <ol>
                <li>initialise <em>i</em> with <em>1</em></li>
                <li>check if <em>n AND i</em> is greater than zero and if so, increment a counter</li>
                <li>multiply <em>i</em> by <em>2</em></li>
                <li>if <em>i &lt; n</em>, go to 2</li>
              </ol>
              <p>Here’s the code:</p>
              <pre data-highlight="code" class="cpp">

/* Count the ON bits in n using an iterative algorithm */
int bitcount(int n) {
        int tot = 0;

        int i;
        for (i = 1; i &lt;= n; i = i&lt;&lt;1)
                if (n & i)
                        ++tot;

        return tot;
}
</pre>
              <p>This isn’t bad and works in <strong>O(lg(n))</strong> time, but if you know (you probably will) if the number is made up mostly of ones or zeros, use one of the following algorithms.</p>
              <h5>Sparse ones algorithm</h5>
              <p>This solution relies on the following observation:<br>
              <code>22<sub>10</sub> = 00010110<sub>2</sub><br>
              22 - 1 = 21<sub>10</sub> = 00010101<sub>2</sub><br>
              22 AND 21 = 00010100<br></code></p>
              <p>Notice what happened: by logically ANDing <em>22</em> and <em>21</em>, you get a number whose binary representation is the same as <em>22</em> but with the last <em>1</em> flipped to <em>0</em>.</p>
              <p>The idea behind this algorithm is to logically AND <em>n</em> and <em>n - 1</em> until <em>n</em> is <em>0</em>. The number of times necessary to do this will the the number of <em>1</em> bits.</p>
              <p>Here’s the code:</p>
              <pre data-highlight="code" class="cpp">

/* Counts the ON bits in n. Use this if you know n is mostly 0s */
int bitcount_sparse_ones(int n) {
        int tot = 0;

        while (n) {
                ++tot;
                n &amp;= n - 1;
        }

        return tot;
}
</pre>
              <p>Why call it <em>sparse ones</em>? Look at the algorithm, and apply it to a few numbers on a piece of paper. You’ll notice that you go through the inner loop the <em>x</em> times, where <em>x</em> is the number of <em>1s</em> in the number. So the time is <strong>O(x)</strong>, and it’s best to use it if there are <strong>few ones</strong> in the number.</p>
              <h5>Dense ones algorithm</h5>
              <p>But what if your number is mostly <em>1</em>? The solution is obvious: flip every bit in <em>n</em>, then apply the sparse ones algorithm:</p>
              <pre data-highlight="code" class="cpp">

/* Counts the ON bits in n. Use this if you know n is mostly 1s */
int bitcount_dense_ones(int n) {
        int tot = 0;

        n ^= (unsigned int)-1;

        while (n) {
                ++tot;
                n &amp;= n - 1;
        }

        return sizeof(int) * 8 - tot;
}
</pre>
              <h5>Full source</h5>
              <p>Here’s the full C source to the programme (<a href="/r/bit.c" title="bit.c">bit.c</a>):<br>
              <strong>NOTE</strong>: Source is mangled by Wordpress. Don’t copy-paste; download the <a href="/r/bit.c" title="bit.c">file</a>.</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;

/* Print n as a binary number */
void printbits(int n) {
        unsigned int i, step;

        if (0 == n) { /* For simplicity's sake, I treat 0 as a special case*/
                printf("0000");
                return;
        }

        i = 1&lt;&gt;= 4; /* In groups of 4 */
        while (step &gt;= n) {
                i &gt;&gt;= 4;
                step &gt;&gt;= 4;
        }

        /* At this point, i is the smallest power of two larger or equal to n */
        while (i &gt; 0) {
                if (n & i)
                        printf("1");
                else
                        printf("0");
                i &gt;&gt;= 1;
        }
}

/* Count the ON bits in n using an iterative algorithm */
int bitcount(int n) {
        int tot = 0;

        int i;
        for (i = 1; i &lt;= n; i = i&lt;&lt;1)
                if (n & i)
                        ++tot;

        return tot;
}

/* Counts the ON bits in n. Use this if you know n is mostly 0s */
int bitcount_sparse_ones(int n) {
        int tot = 0;

        while (n) {
                ++tot;
                n &amp;= n - 1;
        }

        return tot;
}

/* Counts the ON bits in n. Use this if you know n is mostly 1s */
int bitcount_dense_ones(int n) {
        int tot = 0;

        n ^= (unsigned int)-1;

        while (n) {
                ++tot;
                n &amp;= n - 1;
        }

        return sizeof(int) * 8 - tot;
}

int main(int argc, char *argv[]) {
        int i;
        for (i = 0; i &lt; 23; ++i) {
                printf("%d = ", i);
                printbits(i);
                printf("\tON bits: %d %d %d", bitcount(i), bitcount_sparse_ones(i), bitcount_dense_ones(i));
                printf("\n");
        }

        return 0;
}
</pre>
              <p>That’s it. If you’re intrested in a few more (slightly esoteric) algorithms, see <a href="https://web.archive.org/web/20080401105012/http://infolab.stanford.edu/~manku/bitcount/bitcount.html">this article</a>.</p>
              <p>Good luck. Always open to comments.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-11-06 at 17:30</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter.css">
      <script type="text/javascript" src="/r/shCore.js"></script> 
      <script type="text/javascript" src="/r/shBrushCpp.js"></script> 
      <script type="text/javascript">

        dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
