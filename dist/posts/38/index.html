<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Faster Fibonacci</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <link rel="stylesheet" href="/r/screen.css" media="screen">
  <link rel="stylesheet" href="/r/print.css" media="print">
  <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
  <link rel="stylesheet" href="/r/syntax.css">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/38/">
  <meta name="description" content="Most programmers are familiar with the naïve \(O(2^n)\) and the memoized \(O(n)\) algorithms for computing the nth Fibonacci number. However, with a bit of math, you can get the time…">
  <link rel="icon" type="image/png" href="/r/icon.png">
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <header id="postHeader">
    <a href="/"><img src="/r/title_post.png" width="111" height="120" alt="scvalex.net"></a>
  </header>
  <div id="content">
    <article id="standalonePost">
      <div class="postTitle">
        <h2 title="Stats Words: 359 SLOC: 83 Effort: 147">38. Faster Fibonacci</h2><time datetime="2015-01-07">2015-01-07</time>
      </div>
      <div class="post">
        <p>Most programmers are familiar with the naïve <span class="math">\(O(2^n)\)</span> and the memoized <span class="math">\(O(n)\)</span> algorithms for computing the n<sup>th</sup> <a href="http://en.wikipedia.org/wiki/Fibonacci_number">Fibonacci number</a>. However, with a bit of math, you can get the time down to <span class="math">\(O(\log_2n)\)</span>.</p>
        <p>Let’s start with the basics. If we write <span class="math">\(F_n\)</span> for the n<sup>th</sup> Fibonnaci number, the values for the entire sequence are: <span class="math">\(F_1 = F_2 = 1\)</span>, <span class="math">\(F_n = F_{n-1} + F_{n-2}\)</span>. In <a href="https://gist.github.com/scvalex/d691cd5ddc9459fc0b39">code</a>, we write:</p>
        <pre class="sourceCode ocaml"><code class="sourceCode ocaml"><span class="kw">let</span> <span class="kw">rec</span> fib_naive n =
  <span class="kw">if</span> n &lt; <span class="dv">1</span>
  <span class="kw">then</span> failwith <span class="st">"undefined"</span>
  <span class="kw">else</span> <span class="kw">if</span> n = <span class="dv">1</span> || n = <span class="dv">2</span>
  <span class="kw">then</span> big_int_of_int <span class="dv">1</span>
  <span class="kw">else</span> add_big_int (fib_naive (n - <span class="dv">1</span>))
                   (fib_naive (n - <span class="dv">2</span>))
;;</code></pre>
        <p>Every non-base call of <code>fib_naive</code> does two more calls to itself. This is <span class="math">\(O(2^n)\)</span>. We can do better by memoizing the results or by computing the values iteratively:</p>
        <pre class="sourceCode ocaml"><code class="sourceCode ocaml"><span class="kw">let</span> fib_memo =
  <span class="kw">let</span> res = Hashtbl<span class="kw">.</span>create <span class="dv">16</span> <span class="kw">in</span>
  Hashtbl<span class="kw">.</span>add res <span class="dv">1</span> (big_int_of_int <span class="dv">1</span>);
  Hashtbl<span class="kw">.</span>add res <span class="dv">2</span> (big_int_of_int <span class="dv">1</span>);
  <span class="kw">let</span> <span class="kw">rec</span> fib n =
    <span class="kw">if</span> n &lt; <span class="dv">1</span> <span class="kw">then</span>
      failwith <span class="st">"undefined"</span>;
    <span class="kw">match</span> Hashtbl<span class="kw">.</span>find res n <span class="kw">with</span>
    | resx -&gt;
      resx
    | <span class="kw">exception</span> _ -&gt;
      <span class="kw">let</span> resx =
        add_big_int (fib (n - <span class="dv">1</span>)) (fib (n - <span class="dv">2</span>))
      <span class="kw">in</span>
      Hashtbl<span class="kw">.</span>add res n resx;
      resx
  <span class="kw">in</span>
  fib
;;</code></pre>
        <pre class="sourceCode ocaml"><code class="sourceCode ocaml"><span class="kw">let</span> fib_iter n =
  <span class="kw">if</span> n &lt; <span class="dv">1</span> <span class="kw">then</span>
    failwith <span class="st">"undefined"</span>;
  <span class="kw">let</span> one = big_int_of_int <span class="dv">1</span> <span class="kw">in</span>
  <span class="kw">if</span> n = <span class="dv">1</span> || n = <span class="dv">2</span>
  <span class="kw">then</span>
    one
  <span class="kw">else</span>
    <span class="kw">let</span> <span class="kw">rec</span> loop a b n =
      <span class="kw">if</span> n = <span class="dv">0</span>
      <span class="kw">then</span> b
      <span class="kw">else</span> loop b (add_big_int a b) (n - <span class="dv">1</span>)
    <span class="kw">in</span>
    loop one one (n - <span class="dv">2</span>)
;;</code></pre>
        <p>We see that <code>fib_memo</code> is like <code>fib_naive</code>, but the computation for each <code>n</code> is done only once. Similarly, <code>fib_iter</code> is just an iteration up to <code>n</code>, where each step computes the next Fibonacci number. Both functions are <span class="math">\(O(n)\)</span>.</p>
        <p>Can we do better? Yes, we can. But first, we need to know about the following matrix.</p>
        <p><span class="math">\[ M = \begin{bmatrix} 1 & {\color{blue}1} \\ 1 & 0 \end{bmatrix} \\ M^2 = \begin{bmatrix} 2 & {\color{blue}1} \\ 1 & 1 \end{bmatrix} \quad M^3 = \begin{bmatrix} 3 & {\color{blue}2} \\ 2 & 1 \end{bmatrix} \\ M^4 = \begin{bmatrix} 5 & {\color{blue}3} \\ 3 & 2 \end{bmatrix} \quad M^5 = \begin{bmatrix} 8 & {\color{blue}5} \\ 5 & 3 \end{bmatrix} \]</span></p>
        <p>We notice that all the elements of <span class="math">\(M^n\)</span> are Fibonacci numbers: <span class="math">\(M^n_{0,0} = F_{n+1}\)</span>, <span class="math">\(M^n_{1,0} = M^n_{0,1} = F_n\)</span>, and <span class="math">\(M^n_{1,1} = F_{n-1}\)</span>. We can prove this easily by induction: the base case is obvious, and the induction step boils down to:</p>
        <p><span class="math">\[ \begin{align} M^n \times M &amp;= \begin{bmatrix} F_{n+1} & F_n \\ F_n & F_{n-1} \end{bmatrix} \times \begin{bmatrix} 1 & 1\\ 1 & 0 \end{bmatrix} \\ &amp;= \begin{bmatrix} F_{n+1} + F_n & F_{n+1} \\ F_n + F_{n-1} & F_n \end{bmatrix} \\ &amp;= \begin{bmatrix} F_{n+2} & F_{n+1} \\ F_{n+1} & F_n \end{bmatrix}\\ &amp;= M^{n+1} \end{align} \]</span></p>
        <p>So, in order to find the n<sup>th</sup> Fibonacci number, we need to compute <span class="math">\(M^n\)</span> and take the top-right or bottom-left corners. Since matrix multiplication is associative, we can use the usual <span class="math">\(O(\log_2n)\)</span> algorithm to compute <span class="math">\(M^n\)</span>:</p>
        <pre class="sourceCode ocaml"><code class="sourceCode ocaml"><span class="kw">let</span> <span class="kw">rec</span> pow ~mult ~n x =
  <span class="kw">if</span> n &lt; <span class="dv">1</span>
  <span class="kw">then</span>
    failwith <span class="st">"undefined"</span>
  <span class="kw">else</span> <span class="kw">if</span> n = <span class="dv">1</span>
  <span class="kw">then</span>
    x
  <span class="kw">else</span> <span class="kw">if</span> n <span class="kw">mod</span> <span class="dv">2</span> = <span class="dv">0</span>
  <span class="kw">then</span>
    <span class="kw">let</span> y = pow ~mult x ~n:(n / <span class="dv">2</span>) <span class="kw">in</span>
    mult y y
  <span class="kw">else</span>
    <span class="kw">let</span> y = pow ~mult x ~n:(n / <span class="dv">2</span>) <span class="kw">in</span>
    mult x (mult y y)
;;

<span class="kw">type</span> mat =
  { m00 : big_int; m01 : big_int;
    m10 : big_int; m11 : big_int;
  }

<span class="kw">let</span> mat_mult a b =
  <span class="kw">let</span> ( * ) = mult_big_int <span class="kw">in</span>
  <span class="kw">let</span> ( + ) = add_big_int <span class="kw">in</span>
  { m00 = a.m00 * b.m00 + a.m01 * b.m10;
    m01 = a.m00 * b.m10 + a.m01 * b.m11;
    m10 = a.m10 * b.m00 + a.m11 * b.m10;
    m11 = a.m10 * b.m10 + a.m11 * b.m11;
  }
;;

<span class="kw">let</span> m_fib =
  <span class="kw">let</span> big = big_int_of_int <span class="kw">in</span>
  { m00 = big <span class="dv">1</span>; m01 = big <span class="dv">1</span>;
    m10 = big <span class="dv">1</span>; m11 = big <span class="dv">0</span>;
  }
;;

<span class="kw">let</span> fib_mat n =
  <span class="kw">if</span> n &lt; <span class="dv">1</span> <span class="kw">then</span>
    failwith <span class="st">"undefined"</span>;
  <span class="kw">let</span> m = pow ~mult:mat_mult m_fib ~n <span class="kw">in</span>
  m.m01
;;</code></pre>
        <p>To summarise, we started off with the naïve exponential algorithm, then brought it down to linear time by memoizing or factoring out repeated computations, and finally rewrote it to work in logarithmic time by applying a bit of matrix math.</p>
        <hr>
        <p>As an aside, the <a href="http://plus.maths.org/content/life-and-numbers-fibonacci">story</a> behind the name “Fibonacci” is quite amusing. The mathematician’s name is really Leonardo Pisano. However, like most people in the middle ages, he included his father’s name when signing. Fibonacci literally means “son of Bonacci” and it’s the name he came to be know as after scholars misread his signature. Now, if we were to talk about his father, we’d of course talk about “Fibonacci’s father”, which entails a reference to his real name in Latin, followed by a dereference in English. Hilarious.</p>
      </div>
    </article>
    <div id="postNavigation">
      <ul>
        <li>
          <a id="prevLink" rel="prev" href="/posts/37/" title="An Optimisation Story">⋘ Prev</a>
        </li>
        <li>
          <a href="/posts/">All Posts</a>
        </li>
        <li>
          <a id="nextLink" rel="next" href="/posts/39/" title="mkstemp">Next ⋙</a>
        </li>
      </ul>
    </div>
  </div>
  <footer id="postFooter">
    <ul>
      <li class="copyright">© 2015 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code></li>
    </ul>
    <ul>
      <li>
        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a>
      </li>
      <li>
        <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
      </li>
    </ul>
    <ul>
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/atom.xml">RSS Feed</a>
      </li>
    </ul>
  </footer>
</body>
</html>
