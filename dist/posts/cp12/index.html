<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">
  <title>The Fractional Knapsack Problem « Computer programming</title>
  <meta name="description" content="In this article, I describe the greedy algorithm for solving the Fractional Knapsack Problem and give an implementation in C.">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp12/">
  <link rel="stylesheet" href="/r/wp-style-83.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li>
        <a id="prevLink" rel="prev" href="/posts/cp11/">⋘ Prev</a>
      </li>
      <li>
        <a href="/posts/">All Posts</a>
      </li>
      <li>
        <a id="nextLink" rel="next" href="/posts/cp13/">Next ⋙</a>
      </li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-11-20</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: The Fractional Knapsack&nbsp;Problem">The Fractional Knapsack&nbsp;Problem</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p>In this article, I describe the greedy algorithm for solving the Fractional Knapsack Problem and give an implementation in C.</p>
              <h4>The Problem</h4>
              <p>The <a href="https://en.wikipedia.org/wiki/Knapsack_problem">Fractional Knapsack Problem</a> usually sounds like this:</p>
              <blockquote>
                <p>Ted Thief has just broken into the Fort Knox! He sees himself in a room with <em>n</em> piles of gold dust. Because the each pile has a different purity, each pile also has a different value (<em>v[i]</em>) and a different weight (<em>c[i]</em>). Ted has a knapsack that can only hold <em>W</em> kilograms.</p>
                <p>Given <em>n</em>, <em>v</em>, <em>c</em> and <em>W</em>, calculate which piles Ted should completely put into his knapsack and which he should put only a fraction of.</p>
              </blockquote>
              <p>So, for this input:<br>
              <code>n = 5<br>
              c = {12, 1, 2, 1, 4}<br>
              v = {4, 2, 2, 1, 10}<br>
              W = 15<br></code></p>
              <p>Ted should take piles <em>2</em>, <em>3</em>, <em>4</em> and <em>5</em> completely and about <em>58%</em> of pile <em>1</em>.</p>
              <p>You’re usually dealling with a knapsack problem when you’re give the cost and the benefits of certain objects and asked to obtain the maximum benefit so that the sum of the costs is smaller than a given value. You’ve got the <strong>fractional</strong> knapsack problem when you can take fractions (as opposed to all or nothing) of the objects.</p>
              <h4>The Algorithm</h4>
              <p>This is a standard <a href="https://en.wikipedia.org/wiki/Greedy_algorithm">greedy</a> algorithm. In fact, it’s one of the classic examples.</p>
              <p>The idea is to calculate for each object the ratio of <em>value/cost</em>, and sort them according to this ratio. Then you take the objects with the highest ratios and add them until you can’t add the next object as whole. Finally add as much as you can of the next object.</p>
              <p>So, for our example:<br>
              <code>v = {4, 2, 2, 1, 10}<br>
              c = {12, 1, 2, 1, 4}<br>
              r = {1/3, 2, 1, 1, 5/2}<br></code></p>
              <p>From this it’s obvious that you should add the objects: 5, 2, 3, 4 and then as much as possible of 1.<br>
              The output of my programme is this:<br>
              <code>Added object 5 (10$, 4Kg) completly in the bag. Space left: 11.<br>
              Added object 2 (2$, 1Kg) completly in the bag. Space left: 10.<br>
              Added object 3 (2$, 2Kg) completly in the bag. Space left: 8.<br>
              Added object 4 (1$, 1Kg) completly in the bag. Space left: 7.<br>
              Added 58% (4$, 12Kg) of object 1 in the bag.<br>
              Filled the bag with objects worth 15.48$.</code></p>
              <h4>The Programme</h4>
              <p>Now, you could implement the algorithm as stated, but for practical reasons you may wish to trade speed for simplicity. That’s what I’ve done here: instead of sorting the objects, I simply go through them every time searching for the best ratio. This modification turns an <strong>O(n*lg(n))</strong> algorithm into an <strong>O(n<sup>2</sup>)</strong> one. For small values of <strong>n</strong>, this doesn’t matter and <strong>n</strong> is usually small.</p>
              <p>Here’s the code in C (<a href="/r/fractional_knapsack.c" title="fractional_knapsack.c">fractional_knapsack.c</a>):</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;

int n = 5; /* The number of objects */
int c[10] = {12, 1, 2, 1, 4}; /* c[i] is the *COST* of the ith object; i.e. what
                                YOU PAY to take the object */
int v[10] = {4, 2, 2, 1, 10}; /* v[i] is the *VALUE* of the ith object; i.e.
                                what YOU GET for taking the object */
int W = 15; /* The maximum weight you can take */

void simple_fill() {
        int cur_w;
        float tot_v;
        int i, maxi;
        int used[10];

        for (i = 0; i &lt; n; ++i)
                used[i] = 0; /* I have not used the ith object yet */

        cur_w = W;
        while (cur_w &gt; 0) { /* while there's still room*/
                /* Find the best object */
                maxi = -1;
                for (i = 0; i &lt; n; ++i)
                        if ((used[i] == 0) &amp;&
                                ((maxi == -1) || ((float)v[i]/c[i] &gt; (float)v[maxi]/c[maxi])))
                                maxi = i;

                used[maxi] = 1; /* mark the maxi-th object as used */
                cur_w -= c[maxi]; /* with the object in the bag, I can carry less */
                tot_v += v[maxi];
                if (cur_w &gt;= 0)
                        printf("Added object %d (%d$, %dKg) completly in the bag. Space left: %d.\n", maxi + 1, v[maxi], c[maxi], cur_w);
                else {
                        printf("Added %d%% (%d$, %dKg) of object %d in the bag.\n", (int)((1 + (float)cur_w/c[maxi]) * 100), v[maxi], c[maxi], maxi + 1);
                        tot_v -= v[maxi];
                        tot_v += (1 + (float)cur_w/c[maxi]) * v[maxi];
                }
        }

        printf("Filled the bag with objects worth %.2f$.\n", tot_v);
}

int main(int argc, char *argv[]) {
        simple_fill();

        return 0;
}
</pre>
              <p>That’s it. Good luck.</p>
              <p>Always open to comments.</p>
              <p>Update: The next article in this series is <a href="/posts/cp13/">The 0-1 Knapsack Problem</a>.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-11-20 at 16:35</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter.css">
      <script type="text/javascript" src="/r/shCore.js"></script> 
      <script type="text/javascript" src="/r/shBrushCpp.js"></script> 
      <script type="text/javascript">

       dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
