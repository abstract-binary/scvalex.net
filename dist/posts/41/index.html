<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>pingcat</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <link rel="stylesheet" href="/r/screen.css" media="screen">
  <link rel="stylesheet" href="/r/print.css" media="print">
  <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
  <link rel="stylesheet" href="/r/syntax.css">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/41/">
  <meta name="description" content="Most data on the Internet is transferred over TCP or UDP. The former works best when the transport needs to be reliable, and the latter is for when lower latency…">
  <link rel="icon" type="image/png" href="/r/icon.png">
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <header id="postHeader">
    <a href="/"><img src="/r/title_post.png" width="111" height="120" alt="scvalex.net"></a>
  </header>
  <div id="content">
    <article id="standalonePost">
      <div class="postTitle">
        <h2 title="Stats Words: 584 SLOC: 178 Effort: 681">41. pingcat</h2><time datetime="2015-02-02">2015-02-02</time>
      </div>
      <div class="post">
        <p>Most data on the Internet is transferred over TCP or UDP. The former works best when the transport needs to be reliable, and the latter is for when lower latency is more important than data loss or packet reordering. However, for a laugh, you can also transfer it over ICMP, aka <code>ping</code>.</p>
        <p>As a quick refresher, the <a href="http://linux.die.net/man/8/ping"><code>ping(8)</code></a> command works by sending ICMP <code>echo-request</code> packets with some random data, then waiting for ICMP <code>echo-reply</code> packets with the same data. The key point is that the data doesn’t really have to be random.</p>
        <pre class="sourceCode"><code>A                       B
|  echo-request(data)   |
| --------------------&gt; |
|                       |
|   echo-reply(data)    |
| &lt;-------------------- |
|                       |</code></pre>
        <p>Here, we’ll only be focusing on the interesting bits; the full code listing for <code>pingcat</code> is available <a href="https://gist.github.com/scvalex/148b2b4fbd1fe19d4d55">here</a>. Be aware that the program only works when run by <code>root</code> (or by messing with the sticky bit or capabilities). For the complete <code>ping(8)</code> implementation, see the <a href="http://www.linuxfoundation.org/collaborate/workgroups/networking/iputils"><code>iputils</code></a> package.</p>
        <p>The first problem is that we need to send ICMP packets. Looking at the docs for <a href="http://linux.die.net/man/2/socket"><code>socket(2)</code></a>, we see it’s easy to create TCP (<code>SOCK_STREAM</code>) or UDP (<code>SOCK_DGRAM</code>) sockets. With ICMP, it’s a bit trickier: we have to use a raw socket (<code>SOCK_RAW</code>), and compose the packets manually.</p>
        <pre class="sourceCode c"><code class="sourceCode c"><span class="dt">int</span> icmp_sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);</code></pre>
        <p>Composing <a href="https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol#ICMP_segment_structure">ICMP packets</a> sounds a bit daunting, but it’s particularly easy in C. All we need to do is allocate a chunk of memory, cast it to the right type, and fill in the fields.</p>
        <pre class="sourceCode c"><code class="sourceCode c">u_char buf[<span class="bn">0x10000</span>];
<span class="kw">struct</span> icmphdr *icp = (<span class="kw">struct</span> icmphdr *)buf;
icp-&gt;type = ICMP_ECHO;
icp-&gt;code = <span class="dv">0</span>;
icp-&gt;checksum = <span class="dv">0</span>;
icp-&gt;un.echo.sequence = count;  <span class="co">// an incrementing number</span>
icp-&gt;un.echo.id = id;           <span class="co">// something to identify this process</span>
<span class="co">// the first 8 bytes are the ICMP packet, the rest is the data</span>
memcpy(buf + <span class="dv">8</span>, data, datalen);
icp-&gt;checksum = in_cksum((u_short*)icp, datalen + <span class="dv">8</span>, <span class="dv">0</span>);</code></pre>
        <p>We still need to send out the packet. Strictly speaking, all we need to do is write it to the socket, but because we dislike copying data around, we’ll do it the hard way and use <a href="http://linux.die.net/man/2/writev">scatter-gather</a> <a href="http://linux.die.net/man/2/sendmsg">network</a> IO.</p>
        <pre class="sourceCode c"><code class="sourceCode c"><span class="kw">struct</span> sockaddr_in addr;
memset((<span class="dt">char</span> *)&amp;addr, <span class="dv">0</span>, <span class="kw">sizeof</span>(addr));
addr.sin_family = AF_INET;
inet_aton(<span class="st">"8.8.8.8"</span>, &amp;addr.sin_addr);
<span class="kw">struct</span> iovec iov = { buf, <span class="dv">0</span> };
iov.iov_len = datalen + <span class="dv">8</span>;
<span class="kw">struct</span> msghdr hdr = { &amp;addr, <span class="kw">sizeof</span>(addr),
                      &amp;iov, <span class="dv">1</span>, <span class="dv">0</span>, <span class="dv">0</span>, <span class="dv">0</span> };
sendmsg(icmp_sock, &amp;hdr, <span class="dv">0</span>);</code></pre>
        <p>That’s it for the sending side. As for receiving, it’s almost the inverse process: we open a raw socket, read packets from it, and parse them.</p>
        <pre class="sourceCode c"><code class="sourceCode c"><span class="dt">int</span> icmp_sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
u_char buf[<span class="bn">0x10000</span>];
<span class="kw">struct</span> iovec iov = { buf, <span class="kw">sizeof</span>(buf) };
<span class="kw">struct</span> iphdr *ip = (<span class="kw">struct</span> iphdr *)buf;
<span class="kw">struct</span> msghdr msg;
memset(&amp;msg, <span class="dv">0</span>, <span class="kw">sizeof</span>(msg));
msg.msg_name = <span class="dv">0</span>;
msg.msg_namelen = <span class="dv">0</span>;
msg.msg_iov = &amp;iov;
msg.msg_iovlen = <span class="dv">1</span>;
<span class="dt">int</span> datalen = recvmsg(icmp_sock, &amp;msg, MSG_DONTWAIT);</code></pre>
        <p>Parsing is a bit more complex because we get IP packets, so we first need to extract the ICMP part.</p>
        <pre class="sourceCode c"><code class="sourceCode c"><span class="dt">int</span> hlen = ip-&gt;ihl*<span class="dv">4</span>;
datalen -= hlen;
<span class="kw">struct</span> icmphdr *icp = (<span class="kw">struct</span> icmphdr *)(buf + hlen);</code></pre>
        <p>Next, we need to filter out the noise. Our ICMP socket will get a copy of every ICMP packet received by the host. Since we only care about <code>echo-reply</code> packets, we need to filter by <code>icp-&gt;type == ICMP_ECHOREPLY</code>. We also don’t care about packets from other processes, so we need to filter by <code>icp-&gt;un.echo.id == id</code>. Since ICMP packets may be reordered, dropped, or duplicated, we also need to inspect <code>icp-&gt;un.echo.sequence</code> to make sure it’s the reply to the right packet. Finally, to get to the data, we just skip the 8 bytes of ICMP header: <code>(char*)icp + 8</code>.</p>
        <p>And that’s all there is to it. Of course, the devil’s in details, as iputils’ <a href="http://www.linux-ipv6.org/gitweb/gitweb.cgi?p=gitroot/iputils.git;a=blob;f=ping.c;h=c0366cdacd626dde1669bf2d97588eb7c6ece3fd;hb=HEAD">ping</a> <a href="http://www.linux-ipv6.org/gitweb/gitweb.cgi?p=gitroot/iputils.git;a=blob;f=ping_common.c;h=8d6b145e8a92ec34691a947d6ea097e897c5da65;hb=HEAD">demonstrates</a>. For <a href="https://gist.github.com/scvalex/148b2b4fbd1fe19d4d55"><code>pingcat</code></a>, the difficulty lies in that we have to implement reliable transmission on top of an unreliable protocol, so we have to deal with the usual issues of missing, reordered, and duplicate packets. To keep the code clear, <code>pingcat</code> does the simple thing and sends data synchronously: it sends some data, waits for the reply, maybe it resends the data if the reply was corrupted or lost, but it never has more than one data chunk in-flight.</p>
        <p>One last interesting point about <code>echo-reply</code> packets is that they contain the full data sent. This essentially means we can use the network as a storage medium. The back of the envelope calculation for me goes like this: the ICMP round-trip time between me and <code>www.tv-tokyo.co.jp</code> is about 0.3s; my upload speed is about 6Mb/s; this means I can have about 1.8 Mb, or one copy of Hamlet by William Shakespeare, stored in the very links of the Internet at any given time.</p>
      </div>
    </article>
    <div id="postNavigation">
      <ul>
        <li>
          <a id="prevLink" rel="prev" href="/posts/40/" title="Variable Length Arrays">⋘ Prev</a>
        </li>
        <li>
          <a href="/posts/">All Posts</a>
        </li>
        <li>
          <a id="nextLink" rel="next" href="/posts/42/" title="10 years of blogs">Next ⋙</a>
        </li>
      </ul>
    </div>
  </div>
  <footer id="postFooter">
    <ul>
      <li class="copyright">© 2015 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code></li>
    </ul>
    <ul>
      <li>
        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a>
      </li>
      <li>
        <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
      </li>
    </ul>
    <ul>
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/atom.xml">RSS Feed</a>
      </li>
    </ul>
  </footer>
</body>
</html>
