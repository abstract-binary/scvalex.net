<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>An Optimisation Story</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <link rel="stylesheet" href="/r/screen.css" media="screen">
  <link rel="stylesheet" href="/r/print.css" media="print">
  <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
  <link rel="stylesheet" href="/r/syntax.css">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/37/">
  <meta name="description" content="Premature optimisation may be the root of all evil, but it’s also dammed fun. I recently needed an OCaml library for affine transformations: essentially, I needed to multiply 3x3 matrices…">
  <link rel="icon" type="image/png" href="/r/icon.png">
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <header id="postHeader">
    <a href="/"><img src="/r/title_post.png" width="111" height="120" alt="scvalex.net"></a>
  </header>
  <div id="content">
    <article id="standalonePost">
      <div class="postTitle">
        <h2 title="Stats Words: 325 SLOC: 119 Effort: 66">37. An Optimisation Story</h2><time datetime="2014-05-04">2014-05-04</time>
      </div>
      <div class="post">
        <p>Premature optimisation may be <a href="http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.103.6084">the root of all evil</a>, but it’s also dammed fun. I recently needed an OCaml library for <a href="https://en.wikipedia.org/wiki/Affine_transformation">affine transformations</a>: essentially, I needed to multiply <code>3x3</code> matrices together.</p>
        <p>Since the features I needed could be coded in 20 lines of code, it seemed like overkill to use a fully-featured matrix library. Following is a description of three implementations of <code>3x3</code> matrix multiplication and benchmark results.</p>
        <p>The simple implementation just stores the matrices as nested arrays and does the multiplication with three nested loops.</p>
        <pre class="sourceCode ocaml"><code class="sourceCode ocaml"><span class="kw">type</span> mat3 = float <span class="dt">array</span> <span class="dt">array</span>

<span class="kw">let</span> zero () =
  [| [| <span class="fl">0.0</span>; <span class="fl">0.0</span>; <span class="fl">0.0</span>; |]
   ; [| <span class="fl">0.0</span>; <span class="fl">0.0</span>; <span class="fl">0.0</span>; |]
   ; [| <span class="fl">0.0</span>; <span class="fl">0.0</span>; <span class="fl">0.0</span>; |]
  |]
;;

<span class="kw">val</span> ( * ) : mat3 -&gt; mat3 -&gt; mat3
<span class="kw">let</span> ( * ) m1 m2 =
  <span class="kw">let</span> m3 = zero () <span class="kw">in</span>
  <span class="kw">for</span> i = <span class="dv">0</span> <span class="kw">to</span> <span class="dv">2</span> <span class="kw">do</span>
    <span class="kw">for</span> j = <span class="dv">0</span> <span class="kw">to</span> <span class="dv">2</span> <span class="kw">do</span>
      <span class="kw">for</span> k = <span class="dv">0</span> <span class="kw">to</span> <span class="dv">2</span> <span class="kw">do</span>
        m3.(i).(j) &lt;- m3.(i).(j) +. m1.(i).(k) *. m2.(k).(j)
      <span class="kw">done</span>
    <span class="kw">done</span>
  <span class="kw">done</span>;
  m3
;;</code></pre>
        <p>There are several glaring problems with this code. The way it creates the result <code>mat3</code> is inefficient: it first creates a matrix of zeroes, then sets each element to a new value. In addition to doing two memory writes when only one is needed, every second write will trigger the <a href="https://realworldocaml.org/v1/en/html/understanding-the-garbage-collector.html#the-mutable-write-barrier">write barrier</a> further slowing down the code. We can easily fix this by expanding the loops and populating the result <code>mat3</code> in-place:</p>
        <pre class="sourceCode ocaml"><code class="sourceCode ocaml"><span class="kw">let</span> ( * ) m1 m2 =
  [| [| m1.(<span class="dv">0</span>).(<span class="dv">0</span>) *. m2.(<span class="dv">0</span>).(<span class="dv">0</span>) +. m1.(<span class="dv">0</span>).(<span class="dv">1</span>) *. m2.(<span class="dv">1</span>).(<span class="dv">0</span>) +. m1.(<span class="dv">0</span>).(<span class="dv">2</span>) *. m2.(<span class="dv">2</span>).(<span class="dv">0</span>)
      ; m1.(<span class="dv">0</span>).(<span class="dv">0</span>) *. m2.(<span class="dv">0</span>).(<span class="dv">1</span>) +. m1.(<span class="dv">0</span>).(<span class="dv">1</span>) *. m2.(<span class="dv">1</span>).(<span class="dv">1</span>) +. m1.(<span class="dv">0</span>).(<span class="dv">2</span>) *. m2.(<span class="dv">2</span>).(<span class="dv">1</span>)
...
      ; m1.(<span class="dv">2</span>).(<span class="dv">0</span>) *. m2.(<span class="dv">0</span>).(<span class="dv">2</span>) +. m1.(<span class="dv">2</span>).(<span class="dv">1</span>) *. m2.(<span class="dv">1</span>).(<span class="dv">2</span>) +. m1.(<span class="dv">2</span>).(<span class="dv">2</span>) *. m2.(<span class="dv">2</span>).(<span class="dv">2</span>) |]
  |]
;;</code></pre>
        <p>Benchmarking the simple code and the expanded code (and the third version explained below), we see there’s a marked improvement in speed.</p>
        <pre class="sourceCode"><code>┌─────────────────────────────┬──────────┬────────────┐
│ Name                        │ Time/Run │ Percentage │
├─────────────────────────────┼──────────┼────────────┤
│ Array_mat.mat_mult          │ 107.74ns │    100.00% │
│ Array_mat.Expanded.mat_mult │  41.12ns │     38.17% │
│ Record_mat.mat_mult         │  13.75ns │     12.77% │
└─────────────────────────────┴──────────┴────────────┘</code></pre>
        <p>Another problem is that a <code>mat3</code> is represented in memory as an array of 3 references to 3 other arrays of <code>float</code>s. This means that every array access will require following a pointer to a possibly distant memory location. We can get rid of the extra operation and achieve memory locality by representing a <code>mat3</code> as a record of only <code>float</code> fields; the OCaml compiler will store this internally as <a href="http://caml.inria.fr/pub/docs/manual-ocaml/intfc.html#sec418">an array of <code>float</code>s</a>.</p>
        <pre class="sourceCode ocaml"><code class="sourceCode ocaml"><span class="kw">type</span> mat3 = { m00 : float; m01 : float; m02 : float;
              m10 : float; m11 : float; m12 : float;
              m20 : float; m21 : float; m22 : float;
            }

<span class="kw">let</span> ( * ) m1 m2 =
  {
    m00 = m1.m00 *. m2.m00 +. m1.m01 *. m2.m10 +. m1.m02 *. m2.m20;
    m01 = m1.m00 *. m2.m01 +. m1.m01 *. m2.m11 +. m1.m02 *. m2.m21;
...
    m22 = m1.m20 *. m2.m02 +. m1.m21 *. m2.m12 +. m1.m22 *. m2.m22;
  }
;;</code></pre>
        <p>In addition to being eight times faster than the simple implementation, by representing matrices as records, we also rule out the possibility of structurally malformed matrices and get a compact notation for cells.</p>
        <hr>
        <p>The full code used in the benchmark is available <a href="https://gist.github.com/scvalex/d101d73d7dd4b9d542d6">here</a>. The next step would be to look at the generated code for <code>( * )</code> and fix any problems, but that’s too much premature optimisation even for me.</p>
      </div>
    </article>
    <div id="postNavigation">
      <ul>
        <li>
          <a id="prevLink" rel="prev" href="/posts/36/" title="Empirical Pi">⋘ Prev</a>
        </li>
        <li>
          <a href="/posts/">All Posts</a>
        </li>
        <li>
          <a id="nextLink" rel="next" href="/posts/38/" title="Faster Fibonacci">Next ⋙</a>
        </li>
      </ul>
    </div>
  </div>
  <footer id="postFooter">
    <ul>
      <li class="copyright">© 2014 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code></li>
    </ul>
    <ul>
      <li>
        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a>
      </li>
      <li>
        <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
      </li>
    </ul>
    <ul>
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/atom.xml">RSS Feed</a>
      </li>
    </ul>
  </footer>
</body>
</html>
