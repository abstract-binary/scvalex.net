<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">
  <title>Generating the Partitions of a Set « Computer programming</title>
  <meta name="description" content="Generating the Partitions of a Set in C">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp4/">
  <link rel="stylesheet" href="/r/wp-style-83.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">
  <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter.css">
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li>
        <a id="prevLink" rel="prev" href="/posts/cp3/">⋘ Prev</a>
      </li>
      <li>
        <a href="/posts/">All Posts</a>
      </li>
      <li>
        <a id="nextLink" rel="next" href="/posts/cp5/">Next ⋙</a>
      </li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-10-15</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: Generating the Partitions of a&nbsp;Set">Generating the Partitions of a&nbsp;Set</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p><a href="https://en.wikipedia.org/wiki/Partition_of_a_set">Wikipedia</a> defines the <strong>partition of a set</strong> as:</p>
              <blockquote>
                <p>In mathematics, a partition of a set X is a division of X into non-overlapping “parts” or “blocks” or “cells” that cover all of X. More formally, these “cells” are both collectively exhaustive and mutually exclusive with respect to the set being partitioned.</p>
              </blockquote>
              <p>A more <a href="https://www.google.com/search?q=define:succinct">succinct</a> definition is given by <a href="https://mathworld.wolfram.com/SetPartition.html">Mathworld</a>:</p>
              <blockquote>
                <p>A set partition of a set S is a collection of disjoint subsets of S whose union is S.</p>
              </blockquote>
              <p>Simply put, the <strong>partitions of a set <em>S</em></strong> are all the ways in which you can choose disjoint, non-empty subsets of S that unioned result in S.</p>
              <p>From now on, when I say a set of <em>n</em> elements, I mean <em>{1, 2, …, n}</em>. So, what are the subsets of <em>{1, 2, 3}</em>?<br>
              <code><em>{1, 2, 3}<br>
              {2, 3} {1}<br>
              {1, 3} {2}<br>
              {3} {1, 2}<br>
              {3} {2} {1}</em></code></p>
              <p>It’s obvious that these verify the definition: <em>{1}</em>, <em>{2}</em>, <em>{3}</em>, <em>{1, 2}</em>, <em>{1, 3}</em>, <em>{2, 3}</em> and <em>{1, 2, 3}</em> are all subsets of <em>{1, 2, 3}</em>. They’re all non-empty and, in any partition, the same element never appears twice. Finally, in a partitioning, the union of the partitions is the original set.</p>
              <p>In how many ways can you partition a set of <em>n</em> elements? There are many <a href="https://en.wikipedia.org/wiki/Partition_of_a_set#The_number_of_partitions">ways</a> to calculate this, but as far as I can tell, the easiest is using <a href="https://en.wikipedia.org/wiki/Catalan_number">Catalan</a> <a href="https://mathworld.wolfram.com/CatalanNumber.html">numbers</a>:<br>
              <img src="/r/cn.png" alt="Formula for the nth Catalan&nbsp;Number"></p>
              <p>If you check the formula for <em>3</em> you’ll see that it does give the correct answer: <em>5</em>.</p>
              <p>Ok. We know what a partitioning is, we know how many there are, but how do you generate them? This is the first algorithm I could think of. It may not be clear from the explanation why it works but try it on a piece of paper for <em>n=3</em> and it will become obvious. Here’s how I came up with it:</p>
              <p>First of all, how do you represent a partitioning of a set of <em>n</em> elements? The straight-forward way would be using a vector of n integers, each integer representing the number of the subset in which the corresponding element is in. If the corresponding element of <em>3</em> is <em>2</em>, that means that <em>3</em> is in the <em>2<sup>nd</sup></em> subset. So, given the set {1, 2, 3}:<br>
              <code>Partitioning -&gt; Encoding<br>
              {1, 2, 3} -&gt; (1, 1, 1)<br>
              {1} {2, 3} -&gt; (2, 1, 1)<br>
              {2} {1, 3} -&gt; (1, 2, 1)<br>
              {1, 2} {3} -&gt; (2, 2, 1)<br>
              {1} {2} {3} -&gt; (3, 2, 1)<br></code></p>
              <p>Notice that the encodings, written backwards are: <em>111</em>, <em>112</em>, <em>121</em>, <em>122</em> and <em>123</em>. From this you can guess how the generator works: more or less, <strong>generate all the numbers between <em>111</em> and <em>123</em> using only the digits <em>1</em>, <em>2</em> and <em>3</em></strong>:<br>
              <code><br>
              111<br>
              112<br>
              113<br>
              121<br>
              122<br>
              123<br></code></p>
              <p>That’s almost right. The encodings <em>(1, 1, 2)</em> and <em>(1, 1, 3)</em> translate into the same partitioning: <em>{1} {2, 3}</em>. If you do the same thing for a larger <em>n</em> you’ll notice this happening again and again. Fortunately, there’s an easy solution: <strong>never use a digit that’s more than <em>1</em> larger than any other digit in the encoding</strong>. i.e. You can’t use <em>(1, 1, 3)</em> because <em>3</em> is larger by <em>2</em> than the other digits in the encoding (<em>1</em> and <em>1</em>).</p>
              <p>To do this, I use another vector <em>m</em> with the following significance: <em>m[i]</em> is the largest of the first <em>i</em> elements in the encoding. This makes it very easy not to generate any duplicate partitionings.</p>
              <p>Here’s the code in C (<a href="/r/part.c" title="Paritioning a set in&nbsp;C">part.c</a>):</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;

/*
        printp
                - print out the partitioning scheme s of n elements
                as: {1, 2, 4} {3}
*/
void printp(int *s, int n) {
        /* Get the total number of partitions. In the exemple above, 2.*/
        int part_num = 1;
        int i;
        for (i = 0; i &lt; n; ++i)
                if (s[i] &gt; part_num)
                        part_num = s[i];

        /* Print the p partitions. */
        int p;
        for (p = part_num; p &gt;= 1; --p) {
                printf("{");
                /* If s[i] == p, then i + 1 is part of the pth partition. */
                for (i = 0; i &lt; n; ++i)
                        if (s[i] == p)
                                printf("%d, ", i + 1);
                printf("\\b\\b} ");
        }
        printf("\\n");
}

/*
        next
                - given the partitioning scheme represented by s and m, generate
                the next

        Returns: 1, if a valid partitioning was found
                0, otherwise
*/
int next(int *s, int *m, int n) {
        /* Update s: 1 1 1 1 -&gt; 2 1 1 1 -&gt; 1 2 1 1 -&gt; 2 2 1 1 -&gt; 3 2 1 1 -&gt;
        1 1 2 1 ... */
        /*int j;
        printf(" -&gt; (");
        for (j = 0; j &lt; n; ++j)
                printf("%d, ", s[j]);
        printf("\\b\\b)\\n");*/
        int i = 0;
        ++s[i];
        while ((i &lt; n - 1) &amp;& (s[i] &gt; m[i] + 1)) {
                s[i] = 1;
                ++i;
                ++s[i];
        }

        /* If i is has reached n-1 th element, then the last unique partitiong
        has been found*/
        if (i == n - 1)
                return 0;

        /* Because all the first i elements are now 1, s[i] (i + 1 th element)
        is the largest. So we update max by copying it to all the first i
        positions in m.*/
        int max = s[i];
        for (i = i - 1; i &gt;= 0; --i)
                m[i] = max;

/*      for (i = 0; i &lt; n; ++i)
                printf("%d ", m[i]);
        getchar();*/
        return 1;
}

int main(int argc, char *argv[]) {
        int s[16]; /* s[i] is the number of the set in which the ith element
                        should go */
        int m[16]; /* m[i] is the largest of the first i elements in s*/

        int n = 3;
        int i;
        /* The first way to partition a set is to put all the elements in the same
           subset. */
        for (i = 0; i &lt; n; ++i) {
                s[i] = 1;
                m[i] = 1;
        }

        /* Print the first partitioning. */
        printp(s, n);

        /* Print the other partitioning schemes. */
        while (next(s, m, n))
                printp(s, n);

        return 0;
}
</pre>
              <p>The code is heavily commented, but I’ll happily respond to any questions. This is also what I used to generate all the above listings. Try decommenting some of the code to see how the programme works. Good luck!</p>
              <p>P.S. Every encoding after <em>(3, 2, 1)</em> yields a duplicate partitioning. For fun, try proving this mathematically.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-10-15 at 17:44</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <script type="text/javascript" src="/r/shCore.js"></script> 
      <script type="text/javascript" src="/r/shBrushCpp.js"></script> 
      <script type="text/javascript">

        dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
