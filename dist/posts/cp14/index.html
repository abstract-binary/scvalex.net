<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="/r/wp-banner-styles.css">
  <title>One Source Shortest Path: The Bellman-Ford Algorithm « Computer programming</title>
  <meta name="description" content="In this article, I describe the Bellman-Ford algorithm for finding the one-source shortest paths in a graph, give an informal proof and provide the source code in C for a simple implementation.">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/cp14/">
  <link rel="stylesheet" href="/r/wp-style-83.css" type="text/css">
  <link rel="stylesheet" href="/r/h4-global.css" type="text/css">
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <div id="postNavigation">
    <ul>
      <li>
        <a id="prevLink" rel="prev" href="/posts/cp13/">⋘ Prev</a>
      </li>
      <li>
        <a href="/posts/">All Posts</a>
      </li>
      <li>
        <a id="nextLink" rel="next" href="/posts/cp15/">Next ⋙</a>
      </li>
    </ul>
  </div>
  <div id="rap">
    <div id="main">
      <div id="content">
        <div class="post">
          <p class="post-date">2007-11-29</p>
          <div class="post-info">
            <h2 class="post-title"><a href="index.html" rel="bookmark" title="Permanent Link: One Source Shortest Path: The Bellman-Ford&nbsp;Algorithm">One Source Shortest Path: The Bellman-Ford&nbsp;Algorithm</a></h2>Posted by scvalex<br>
            &nbsp;
          </div>
          <div class="post-content">
            <div class="snap_preview">
              <p>In this article, I describe the Bellman-Ford algorithm for finding the one-source shortest paths in a graph, give an informal proof and provide the source code in C for a simple implementation.</p>
              <p>To understand this you should know what a graph is, and how to store one in memory. If in doubt check <a href="/posts/cp10/">this</a> and <a href="https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)">this</a>.</p>
              <p>Another solution to this problem is <a href="/posts/cp15/">Dijkstra’s algorithm</a>.</p>
              <h4>The Problem</h4>
              <p>Given the following graph, calculate the length of the shortest path from <strong>node 1</strong> to <strong>node 2</strong>.<br>
              <img src="/r/bf1.png" alt="bf1.png"></p>
              <p>It’s obvious that there’s a direct route of length <em>6</em>, but take a look at path: <em>1 -&gt; 4 -&gt; 3 -&gt; 2</em>. The length of the path is <em>7 - 3 - 2 = 2</em>, which is less than <em>6</em>. BTW, you don’t need negative edge weights to get such a situation, but they do clarify the problem.</p>
              <p>This also suggests a property of shortest path algorithms: to find the shortest path form <em>x</em> to <em>y</em>, you need to know, beforehand, the shortest paths to <em>y</em>’s neighbours. For this, you need to know the paths to <em>y</em>’s neighbours’ neighbours… In the end, you must calculate the shortest path to the <a href="https://en.wikipedia.org/wiki/Connected_component_(graph_theory)">connected component</a> of the graph in which <em>x</em> and <em>y</em> are found.</p>
              <p>That said, you usually calculate <strong>the shortest path to all nodes</strong> and then pick the ones you’re intrested in.</p>
              <h4>The Algorithm</h4>
              <p>The <a href="https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm">Bellman-Ford</a> algorithm is one of the classic solutions to this problem. It calculates the shortest path to all nodes in the graph from a single source.</p>
              <p>The basic idea is simple:<br>
              Start by considering that the shortest path to all nodes, less the source, is infinity. Mark the length of the path to the source as <em>0</em>:<br>
              <img src="/r/bf2.png" alt="bf2.png"></p>
              <p>Take every edge and try to <em>relax</em> it:<br>
              <img src="/r/bf3.png" alt="bf3.png"></p>
              <p><strong>Relaxing</strong> an edge means checking to see if the path to the node the edge is pointing to can’t be shortened, and if so, doing it. In the above graph, by checking the <strong>edge 1 -&gt; 2</strong> of length <em>6</em>, you find that the length of the shortest path to <strong>node 1</strong> plus the length of the <strong>edge 1 -&gt; 2</strong> is less then infinity. So, you replace infinity in <strong>node 2</strong> with <em>6</em>. The same can be said for edge <em>1 -&gt; 4</em> of length <em>7</em>. It’s also worth noting that, practically, you can’t relax the edges whose start has the shortest path of length infinity to it.</p>
              <p>Now, you apply the previous step <em>n - 1</em> times, where n is the number of nodes in the graph. In this example, you have to apply it <em>4</em> times (that’s <em>3</em> more times).<br>
              <img src="/r/bf4.png" alt="bf4.png"></p>
              <p><img src="/r/bf5.png" alt="bf5.png"></p>
              <p><img src="/r/bf6.png" alt="bf6.png"></p>
              <p>That’s it, here’s the algorithm in a condensed form:</p>
              <pre data-highlight="code" class="cpp">

void bellman_ford(int s) {
        int i, j;

        for (i = 0; i &lt; n; ++i)
                d[i] = INFINITY;

        d[s] = 0;

        for (i = 0; i &lt; n - 1; ++i)
                for (j = 0; j &lt; e; ++j)
                        if (d[edges[j].u] + edges[j].w &lt; d[edges[j].v])
                                d[edges[j].v] = d[edges[j].u] + edges[j].w;
}
</pre>
              <p>Here, <strong>d[i]</strong> is the shortest path to node <strong>i</strong>, <strong>e</strong> is the number of edges and <strong>edges[i]</strong> is the <strong>i</strong>-th edge.</p>
              <p>It may not be obvious why this works, but take a look at what is certain after each step. After the first step, any path made up of at most <em>2</em> nodes will be optimal. After the step <em>2</em>, any path made up of at most <em>3</em> nodes will be optimal… After the <em>(n - 1)</em>-th step, any path made up of at most <em>n</em> nodes will be optimal.</p>
              <h4>The Programme</h4>
              <p>The following programme just puts the <strong>bellman_ford</strong> function into context. It runs in <strong>O(VE)</strong> time, so for the example graph it will do something on the lines of <strong>5 * 9 = 45</strong> relaxations. Keep in mind that this algorithm works quite well on graphs with few edges, but is very slow for dense graphs (graphs with almost <em>n<sup>2</sup></em> edges). For graphs with lots of edges, you’re better off with <a href="/posts/cp15/">Dijkstra’s algorithm</a>.</p>
              <p>Here’s the source code in C (<a href="/r/bellmanford.c" title="bellmanford.c">bellmanford.c</a>):</p>
              <pre data-highlight="code" class="cpp">

#include &lt;stdio.h&gt;

typedef struct {
        int u, v, w;
} Edge;

int n; /* the number of nodes */
int e; /* the number of edges */
Edge edges[1024]; /* large enough for n &lt;= 2^5=32 */
int d[32]; /* d[i] is the minimum distance from node s to node i */

#define INFINITY 10000

void printDist() {
        int i;

        printf("Distances:\n");

        for (i = 0; i &lt; n; ++i)
                printf("to %d\t", i + 1);
        printf("\n");

        for (i = 0; i &lt; n; ++i)
                printf("%d\t", d[i]);

        printf("\n\n");
}

void bellman_ford(int s) {
        int i, j;

        for (i = 0; i &lt; n; ++i)
                d[i] = INFINITY;

        d[s] = 0;

        for (i = 0; i &lt; n - 1; ++i)
                for (j = 0; j &lt; e; ++j)
                        if (d[edges[j].u] + edges[j].w &lt; d[edges[j].v])
                                d[edges[j].v] = d[edges[j].u] + edges[j].w;
}

int main(int argc, char *argv[]) {
        int i, j;
        int w;

        FILE *fin = fopen("dist.txt", "r");
        fscanf(fin, "%d", &amp;n);
        e = 0;

        for (i = 0; i &lt; n; ++i)
                for (j = 0; j &lt; n; ++j) {
                        fscanf(fin, "%d", &amp;w);
                        if (w != 0) {
                                edges[e].u = i;
                                edges[e].v = j;
                                edges[e].w = w;
                                ++e;
                        }
                }
        fclose(fin);

        /* printDist(); */

        bellman_ford(0);

        printDist();

        return 0;
}
</pre>
              <p>And here’s the input file used in the example (<a href="/r/dist1.txt" title="dist.txt">dist.txt</a>):<br>
              <code>5<br>
              0 6 0 7 0<br>
              0 0 5 8 -4<br>
              0 -2 0 0 0<br>
              0 0 -3 9 0<br>
              2 0 7 0 0</code></p>
              <p>That’s an <a href="https://en.wikipedia.org/wiki/Adjacency_matrix">adjacency matrix</a>.</p>
              <p>That’s it. Have fun. Always open to comments.</p>
            </div>
            <div class="post-info"></div>
            <div class="post-footer">
              &nbsp;
            </div>
          </div>
        </div>
      </div>
      <div id="sidebar">
        <h2>Archived Entry</h2>
        <ul>
          <li><strong>Post Date :</strong></li>
          <li>2007-11-29 at 17:50</li>
          <li><strong>Category :</strong></li>
        </ul>
      </div>
      <p id="footer"><a href="https://compprog.wordpress.com/">Originally published on WordPress.com</a>. — Theme: Connections by <a href="https://web.archive.org/web/20080322203714/http://www.vanillamist.com/blog" rel="designer">Patricia Müller</a></p>
      <link type="text/css" rel="stylesheet" href="/r/SyntaxHighlighter.css">
      <script type="text/javascript" src="/r/shCore.js"></script> 
      <script type="text/javascript" src="/r/shBrushCpp.js"></script> 
      <script type="text/javascript">

        dp.SyntaxHighlighter.HighlightAll('code');
      </script>
    </div>
  </div>
</body>
</html>
