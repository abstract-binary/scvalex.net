<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Amortized Analysis</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <link rel="stylesheet" href="/r/screen.css" media="screen">
  <link rel="stylesheet" href="/r/print.css" media="print">
  <link rel="stylesheet" href="/r/mobile.css" media="only screen and (max-device-width: 1257px)">
  <link rel="stylesheet" href="/r/syntax.css">
  <link href="/atom.xml" type="application/atom+xml" rel="alternate" title="scvalex.net">
  <link rel="canonical" href="https://scvalex.net/posts/34/">
  <meta name="description" content="The asymptotic complexity of a loop which does at most \(N\) constant-time operations is \(O(N)\). \(M\) such loops will then have \(O(M * N)\) time complexity. But not always. If…">
  <link rel="icon" type="image/png" href="/r/icon.png">
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  <script async defer data-website-id="21fa18dc-48e0-481f-9b16-0a3b7605bf71" src="https://umami.scvalex.net/script.js"></script>
</head>
<body>
  <header id="postHeader">
    <a href="/"><img src="/r/title_post.png" width="111" height="120" alt="scvalex.net"></a>
  </header>
  <div id="content">
    <article id="standalonePost">
      <div class="postTitle">
        <h2 title="Stats Words: 664 SLOC: 9 Effort: 140">34. Amortized Analysis</h2><time datetime="2013-10-12">2013-10-12</time>
      </div>
      <div class="post">
        <p>The asymptotic complexity of a loop which does at most <span class="math">\(N\)</span> constant-time operations is <span class="math">\(O(N)\)</span>. <span class="math">\(M\)</span> such loops will then have <span class="math">\(O(M * N)\)</span> time complexity. But not always. If you’re careful, you can sometimes do <span class="math">\(M\)</span> loops of <span class="math">\(O(N)\)</span> in only <span class="math">\(O(M)\)</span> time.</p>
        <p>For an example, we’re going to look at a binary counter. Concretely, we have an array of <span class="math">\(N\)</span> bits representing a number in binary form. We will analyze the asymptotic complexity of <code>incr()</code> which increments the binary counter.</p>
        <pre class="sourceCode"><code>incr(0000_0000) = 0000_0001
incr(0000_0001) = 0000_0010
incr(0000_0010) = 0000_0011
incr(0000_0011) = 0000_0100</code></pre>
        <p>The algorithm for <code>incr()</code> is deceptively simple. Start at the right-most digit. As long as the current digit is <span class="math">\(1\)</span>, set it to <span class="math">\(0\)</span> and move left. When we reach a <span class="math">\(0\)</span>, set it to <span class="math">\(1\)</span> and stop. If we reach the left end of the array before finding a <span class="math">\(0\)</span>, we’ve overflown.</p>
        <pre class="sourceCode python"><code class="sourceCode python"><span class="kw">def</span> incr(counter):
    i = <span class="dt">len</span>(counter) - <span class="dv">1</span>
    <span class="kw">while</span> i &gt;= <span class="dv">0</span> and counter[i] == <span class="dv">1</span>:
        counter[i] = <span class="dv">0</span>
        i -= <span class="dv">1</span>
    <span class="kw">if</span> i &gt;= <span class="dv">0</span>:
        counter[i] = <span class="dv">1</span>
    <span class="kw">else</span>:
        <span class="kw">raise</span> <span class="ot">OverflowError</span>()</code></pre>
        <p>Since the loop executes <span class="math">\(N\)</span> steps in the worst case, the complexity of <code>incr()</code> is <span class="math">\(O(N)\)</span>. Therefore, <span class="math">\(M\)</span> calls to <code>incr()</code> will have <span class="math">\(O(M * N)\)</span> complexity, right? Strictly speaking yes, but by careful counting, we can tighten the bound to <span class="math">\(O(M)\)</span>.</p>
        <p>When figuring out the complexity, we normally count the cost of each operation as <span class="math">\(1\)</span>: at each iteration, the loop will do <span class="math">\(2\)</span> loop comparisons and at most <span class="math">\(2\)</span> assignments. So, each loop iteration costs <span class="math">\(O(2 + 2) = O(1)\)</span>, and <span class="math">\(N\)</span> of them cost <span class="math">\(O(N * 1) = O(N)\)</span>.</p>
        <p>If we look at the loop more closely, we see that it doesn’t always do <span class="math">\(N\)</span> iterations. In fact, it does exactly as many iterations as there are <span class="math">\(1\)</span>s at the right of the array. In other words, a call to <code>incr()</code> always does zero or more sequences of <code>i &gt;= 0, counter[i] == 1, counter[i] = 0, i -= 1</code> and then exactly one <code>i &gt;= 0, counter[i] == 1, i &gt;= 0, counter[i] = 1</code>. The most important detail is that the number of times it goes through the first sequence is upper bounded by the number of times it previously went through the second one.</p>
        <p>With this is mind, let’s revise our way of counting operations such that we pay for some of them in advance. More precisely, we now count executing <code>counter[i] = 1</code> as <span class="math">\(5\)</span> instead of as <span class="math">\(1\)</span>. Since <span class="math">\(O(5) = O(1)\)</span>, we haven’t changed the asymptotic complexity of <code>incr()</code>, but if we consider multiple invocations of <code>incr()</code>, we can apply the following reasoning. A call to <code>incr()</code> consists of several runs through the first sequence of cost <span class="math">\(4\)</span>, and one final run through the second sequence of revised cost <span class="math">\(3 + 5 = 7\)</span>. But, since we started with an array of <span class="math">\(0\)</span>s and only the second sequence sets elements to <span class="math">\(1\)</span>, we know that every run through the first sequence must have been preceded by a run through the second one. Furthermore, since we’re overpaying the cost of the second sequence by exactly the cost of the first one, we claim that the every run through the first sequence is pre-payed. So, the cost of the loop is just <span class="math">\(O(1)\)</span>, and, by extension, the cost of <code>incr()</code> is also <span class="math">\(O(1)\)</span>.</p>
        <p>To recap, we started by analyzing the complexity of a function. We noticed that its execution follows a very specific pattern. We decided to overpay part of the function’s invocation cost such that future invocations will already be partially payed for. In other words, we amortized the cost of the function across multiple calls to it. Finally, we noticed that by doing this, the complexity dropped from <span class="math">\(O(N)\)</span> to <span class="math">\(O(1)\)</span>.</p>
        <p>It’s like magic: a sleight of hand and the <span class="math">\(N\)</span> is gone. But, like with real magic, the <span class="math">\(N\)</span> isn’t actually gone and we haven’t actually made the function any faster. What we have done is improve our cost model for it.</p>
        <hr>
        <p>This is amortized analysis, a way to improve our understanding of a function’s cost. More precisely, this is <em>aggregate analysis</em>. For a more formal presentation of this and other kinds of amortized analysis, consult <a href="http://www.amazon.co.uk/Introduction-Algorithms-T-Cormen/dp/0262533057">Introduction to Algorithms</a>.</p>
      </div>
    </article>
    <div id="postNavigation">
      <ul>
        <li>
          <a id="prevLink" rel="prev" href="/posts/33/" title="Cycling Arrays">⋘ Prev</a>
        </li>
        <li>
          <a href="/posts/">All Posts</a>
        </li>
        <li>
          <a id="nextLink" rel="next" href="/posts/35/" title="OCaml's Compare">Next ⋙</a>
        </li>
      </ul>
    </div>
  </div>
  <footer id="postFooter">
    <ul>
      <li class="copyright">© 2013 Alexandru Scvorțov <code>(λyz.mailyscvalexznet)@.</code></li>
    </ul>
    <ul>
      <li>
        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a>
      </li>
      <li>
        <a rel="license" href="https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12">EUPL-1.2-or-later</a>
      </li>
    </ul>
    <ul>
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/atom.xml">RSS Feed</a>
      </li>
    </ul>
  </footer>
</body>
</html>
