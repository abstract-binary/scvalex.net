use crate::OrError;
use liquid_core::Expression;
use liquid_core::FilterParameters;
use liquid_core::FromFilterParameters;
use liquid_core::Result;
use liquid_core::Runtime;
use liquid_core::{Display_filter, Filter, FilterReflection, ParseFilter};
use liquid_core::{Value, ValueView};

fn absolutize_urls(
    input: &dyn ValueView,
    site_base_url: &str,
    page_rel_url: &str,
) -> OrError<Value> {
    use lol_html::{element, html_content::Element, rewrite_str, Settings};
    if input.is_nil() {
        return Ok(Value::Nil);
    }
    let html = input.to_kstr();
    let absolutize_f = |attr| {
        move |el: &mut Element| {
            if let Some(href) = el.get_attribute(attr) {
                if href.starts_with('/') {
                    el.set_attribute(attr, &format!("{site_base_url}{href}"))
                        .unwrap();
                } else if href.starts_with('#') {
                    el.set_attribute(
                        attr,
                        &format!("{site_base_url}{page_rel_url}{href}"),
                    )
                    .unwrap();
                }
            }
            Ok(())
        }
    };
    let html = rewrite_str(
        &html,
        Settings {
            element_content_handlers: vec![
                element!("a[href]", absolutize_f("href")),
                element!("img[src]", absolutize_f("src")),
            ],
            ..Settings::default()
        },
    )?;
    Ok(Value::scalar(html))
}

#[derive(Clone, ParseFilter, FilterReflection)]
#[filter(
    name = "absolutize_urls",
    description = "Makes all relative URLs absolute.",
    parameters(AbsolutizeUrlsArgs),
    parsed(AbsolutizeUrlsFilter)
)]
pub struct AbsolutizeUrls;

#[derive(Debug, FromFilterParameters, Display_filter)]
#[name = "absolutize_urls"]
struct AbsolutizeUrlsFilter {
    #[parameters]
    args: AbsolutizeUrlsArgs,
}

#[derive(Debug, FilterParameters)]
struct AbsolutizeUrlsArgs {
    #[parameter(
        description = "The base URL of the site. E.g. https://scvalex.net",
        arg_type = "str"
    )]
    site_base_url: Expression,
    #[parameter(
        description = "The relative URL of the page. E.g. /posts/69/",
        arg_type = "str"
    )]
    page_rel_url: Expression,
}

impl Filter for AbsolutizeUrlsFilter {
    fn evaluate(&self, input: &dyn ValueView, runtime: &dyn Runtime) -> Result<Value> {
        let args = self.args.evaluate(runtime)?;
        absolutize_urls(input, &args.site_base_url, &args.page_rel_url).map_err(|err| {
            liquid_core::Error::with_msg(format!("Error in AbsolutizeUrls: {err}"))
        })
    }
}
