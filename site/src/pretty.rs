use crate::OrError;
use camino::Utf8Path;
use log::{debug, error};
use std::process::{Command, Output};

pub fn prettify_command<P: AsRef<Utf8Path>>(
    path: P,
) -> Option<(&'static str, &'static [&'static str])> {
    let path = path.as_ref();
    let extension = path.extension();
    let filename = path.file_name();
    if extension == Some("html") && filename != Some("google66b4f0b4f789f10d.html") {
        Some((
            "tidy",
            &[
                "-modify",
                "-quiet",
                "-wrap",
                "0",
                "-indent",
                "--tidy-mark",
                "false",
                "{}",
            ],
        ))
    } else if extension == Some("xml") && filename != Some("BingSiteAuth.xml") {
        Some((
            "tidy",
            &[
                "-modify",
                "-quiet",
                "-wrap",
                "0",
                "-xml",
                "-indent",
                "--tidy-mark",
                "false",
                "{}",
            ],
        ))
    } else if extension == Some("css")
        && filename != Some("katex.min.css")
        && filename != Some("syntax.css")
        && filename != Some("screen.css")
    {
        Some(("node", &["$NODE_PATH/.bin/perfectionist", "{}", "{}"]))
    } else {
        None
    }
}

pub fn run_pretty_command<P: AsRef<Utf8Path>>(
    (cmd, args): &(&str, &[&str]),
    path: P,
) -> OrError<()> {
    let path = path.as_ref();
    debug!("PRETTY {path}");
    let node_path =
        std::env::var("NODE_PATH").unwrap_or_else(|_| "./node_modules".to_string());
    let args: Vec<String> = args
        .iter()
        .map(|arg| {
            if *arg == "{}" {
                path.to_string()
            } else {
                arg.replace("$NODE_PATH", &node_path)
            }
        })
        .collect();
    let Output {
        stdout,
        stderr,
        status,
    } = Command::new(cmd).args(&args).output()?;
    if !status.success() {
        error!(
            "FAILED PRETTY stdout ({path}): {}",
            std::str::from_utf8(&stdout)?
        );
        error!(
            "FAILED PRETTY stderr ({path}): {}",
            std::str::from_utf8(&stderr)?
        );
    }
    Ok(())
}
