use crate::{config::DIST_DIR, web_server, OrError};
use camino::Utf8PathBuf;
use clap::Parser;
use eyre::WrapErr;
use std::net::SocketAddr;

#[derive(Parser)]
pub struct Opts {
    #[clap(long, default_value = DIST_DIR)]
    dist_dir: Utf8PathBuf,

    #[clap(long, default_value = "127.0.0.1:3000", env = "LISTEN_ON")]
    listen_on: SocketAddr,

    #[clap(long, env = "FEEDBACK_SECRET_KEY")]
    feedback_secret_key: String,

    #[clap(long, env = "FEEDBACK_SMTP_KEY")]
    feedback_smtp_key: String,
}

pub fn run(
    Opts {
        dist_dir,
        listen_on,
        feedback_secret_key,
        feedback_smtp_key,
    }: Opts,
) -> OrError<()> {
    let runtime = tokio::runtime::Builder::new_multi_thread()
        .thread_name("web-server")
        .enable_all()
        .build()?;
    let quit_sig = async {
        _ = tokio::signal::ctrl_c().await;
        log::info!("Shutting down");
    };
    runtime.block_on(async move {
        web_server::serve(web_server::Opts {
            reload_rx: None,
            dist_dir,
            listen_on: Some(listen_on),
            feedback_secret_key,
            feedback_smtp_key,
        })
        .await
        .wrap_err("starting server")?
        .serve
        .with_graceful_shutdown(quit_sig)
        .await
        .wrap_err("running server")?;
        Ok::<(), eyre::Error>(())
    })?;
    Ok(())
}
