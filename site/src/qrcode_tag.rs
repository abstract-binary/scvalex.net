use liquid::{model::ObjectIndex, reflection::TagReflection};
use liquid_core::{error::ResultLiquidReplaceExt, Expression, ParseTag, Renderable};
use qrcodegen::{self, QrCodeEcc};

#[derive(Clone)]
pub struct QrCodeTag;

#[derive(Debug)]
struct QrCode {
    text: Expression,
}

impl TagReflection for QrCodeTag {
    fn tag(&self) -> &'static str {
        "qrcode"
    }

    fn description(&self) -> &'static str {
        ""
    }
}

impl ParseTag for QrCodeTag {
    fn parse(
        &self,
        mut arguments: liquid_core::TagTokenIter,
        _options: &liquid_core::Language,
    ) -> liquid_core::Result<Box<dyn liquid_core::Renderable>> {
        let text: Expression = arguments
            .expect_next("Text expected")?
            .expect_value()
            .into_result()?;
        arguments.expect_nothing()?;
        Ok(Box::new(QrCode { text }))
    }

    fn reflection(&self) -> &dyn TagReflection {
        self
    }
}

impl Renderable for QrCode {
    fn render_to(
        &self,
        writer: &mut dyn std::io::Write,
        runtime: &dyn liquid_core::Runtime,
    ) -> liquid_core::Result<()> {
        let text = self.text.evaluate(runtime)?;
        let qr = qrcodegen::QrCode::encode_text(
            text.as_view().to_kstr().as_index(),
            QrCodeEcc::Medium,
        )
        .replace("Failed to generate QR code")?;
        write!(writer, "{}", to_svg_string(&qr, 0)).replace("Failed to render")?;
        Ok(())
    }
}

fn to_svg_string(qr: &qrcodegen::QrCode, border: i32) -> String {
    assert!(border >= 0, "Border must be non-negative");
    let mut result = String::new();
    let dimension = qr
        .size()
        .checked_add(border.checked_mul(2).unwrap())
        .unwrap();
    result += &format!(
        r###"<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 {0} {0}" stroke="none" width=300 height=300>
        "###,
        dimension
    );
    result += r###"  <rect width="100%" height="100%" fill="#FFFFFF"/>
              "###;
    result += r###"  <path d=""###;
    for y in 0..qr.size() {
        for x in 0..qr.size() {
            if qr.get_module(x, y) {
                if x != 0 || y != 0 {
                    result += " ";
                }
                result += &format!("M{},{}h1v1h-1z", x + border, y + border);
            }
        }
    }
    result += r###"" fill="#000000"/>
              "###;
    result += "</svg>";
    result
}
