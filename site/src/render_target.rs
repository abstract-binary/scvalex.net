use crate::{
    config::DIST_DIR, markdown_html, pretty, template_store::TemplateStore,
    together_metas::TogetherMetasLiquid, OrError,
};
use camino::{Utf8Path, Utf8PathBuf};
use eyre::{bail, eyre};
use itertools::Itertools;
use jiff::civil::Date;
use serde::{Deserialize, Serialize};
use std::{fmt, fs};
use voca_rs::manipulate::slugify;

#[derive(Clone)]
pub struct RenderTarget {
    pub name: String,
    /// The file path of this target, not including the top-level dir
    /// like `templates/` or `pages/`.
    pub subpath: Utf8PathBuf,
    input_data: InputData,
    /// This will get set during the rendering process.
    pub content_html: Option<String>,
}

#[derive(Clone)]
enum InputData {
    Html {
        text: String,
        meta: im::HashMap<String, String>,
    },
    MarkdownWithFrontmatter {
        text: String,
        meta: Meta,
    },
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum Meta {
    Blogpost(Blogpost),
    StandalonePage(StandalonePage),
    BookshelfBook(BookshelfBook),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct StandalonePage {
    title: String,
    date: Date,
    description: String,
    layout: Option<String>,

    /// The URL slug is derived from the title.
    #[serde(skip_deserializing)]
    url_slug: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Blogpost {
    title: String,
    date: Date,

    #[serde(default)]
    drafts: u64,
    math: Option<bool>,

    /// The post number is inferred from the filename.  Failure is a
    /// hard error.
    #[serde(skip_deserializing)]
    pub post_number: u64,

    /// The URL slug is derived from the title.
    #[serde(skip_deserializing)]
    url_slug: String,

    /// The description is the first paragraph of the rendered HTML.
    #[serde(skip_deserializing)]
    description: String,

    image: Option<String>,

    #[serde(default)]
    pub categories: Vec<String>,

    #[serde(default)]
    pub draft: bool,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct BookshelfBook {
    title: String,
    author: String,
    link: String,
    pub read: Date,
    cover_image: String,
    cover_image_height: u32,
    cover_image_width: u32,
    categories: Vec<String>,
    opinion: String,

    /// The URL slug is derived from the title.
    #[serde(skip_deserializing)]
    pub url_slug: String,
}

#[derive(Clone, Copy, Debug)]
pub enum InputFormat {
    Html,
    MarkdownWithFrontmatter,
    Xml,
}

impl RenderTarget {
    pub fn new<P: AsRef<Utf8Path>, R: AsRef<Utf8Path>>(
        name: &str,
        src: P,
        subpath: R,
        input_format: InputFormat,
    ) -> OrError<Self> {
        let src = src.as_ref();
        let data = fs::read_to_string(src)?;
        let (meta_lines, text) = {
            let mut lines = data.lines();
            if let Some("---") = lines.next() {
                let meta_lines = lines
                    .take_while_ref(|line: &&str| *line != "---")
                    .join("\n");
                let data_lines = lines.skip(1).join("\n");
                (Some(meta_lines), data_lines)
            } else {
                (None, data)
            }
        };
        match input_format {
            InputFormat::MarkdownWithFrontmatter => {
                if let Some(meta_lines) = meta_lines {
                    let mut meta: Meta = toml::from_str(&meta_lines)?;
                    match &mut meta {
                        Meta::Blogpost(blogpost) => {
                            blogpost.post_number = post_number(name)
                                .ok_or_else(|| eyre!("missing post number for {name}"))?;
                            blogpost.url_slug = slugify(&blogpost.title);
                            // This HTML doesn't have any liquid partials
                            // applied, so it's only good for the first
                            // paragraph (where we're careful not to use
                            // partials).
                            if let Some((first_paragraph, _)) =
                                markdown_html::split_first_paragraph(&text)
                            {
                                let first_paragraph = markdown_html::to_html(
                                    &first_paragraph,
                                    markdown_html::Pass::First,
                                )?;
                                blogpost.description = first_paragraph;
                            }
                        }
                        Meta::StandalonePage(standalone_page) => {
                            standalone_page.url_slug = slugify(&standalone_page.title);
                        }
                        Meta::BookshelfBook(bookshelf_book) => {
                            bookshelf_book.url_slug = slugify(&bookshelf_book.title);
                        }
                    }
                    Ok(Self {
                        name: name.to_string(),
                        subpath: subpath.as_ref().to_path_buf(),
                        input_data: InputData::MarkdownWithFrontmatter { text, meta },
                        // This will be set during the rendering
                        // process once we have all the partials
                        // loaded.
                        content_html: None,
                    })
                } else {
                    bail!("Frontmatter missing on {src}");
                }
            }
            InputFormat::Html | InputFormat::Xml => {
                Ok(Self {
                    name: name.to_string(),
                    subpath: subpath.as_ref().to_path_buf(),
                    input_data: InputData::Html {
                        text,
                        meta: if let Some(str) = meta_lines {
                            toml::from_str(&str)?
                        } else {
                            im::HashMap::new()
                        },
                    },
                    // This will never be set.
                    content_html: None,
                })
            }
        }
    }

    /// Render a template to disk.
    ///
    /// This is a noop for internal pages that start with `_`.
    ///
    /// For markdown pages, this will also set `self.content_html`.
    /// This may fail if the template tries to access `together_metas`
    /// fields that weren't populated yet.  The idea is that this
    /// function will be run multiple times; see
    /// `render_store::render_all`.
    pub fn render(
        &mut self,
        template_store: &TemplateStore,
        together_metas: &TogetherMetasLiquid,
        page_meta: im::HashMap<String, String>,
        pretty: bool,
    ) -> OrError<()> {
        if let Some(dist_path) = self.dist_path()? {
            match &self.input_data {
                InputData::Html { text, meta } => {
                    if let Some(parent_dir) = dist_path.parent() {
                        std::fs::create_dir_all(parent_dir)?;
                    }
                    template_store.render_standalone(
                        &template_store.parse_standalone(text)?,
                        &dist_path,
                        together_metas,
                        page_meta.union(meta.clone()),
                    )?;
                }
                InputData::MarkdownWithFrontmatter { text, meta } => {
                    if self.content_html.is_none() {
                        self.content_html = Some(template_store.render_content_html(
                            text,
                            &dist_path,
                            together_metas,
                            meta,
                        )?);
                    }
                    if let Some(layout) = meta.layout() {
                        if let Some(parent_dir) = dist_path.parent() {
                            std::fs::create_dir_all(parent_dir)?;
                        }
                        template_store.render_with_layout_and_content(
                            &layout,
                            &dist_path,
                            together_metas,
                            meta,
                            self.content_html.as_ref().unwrap(),
                        )?;
                    }
                }
            }
            if pretty {
                if let Some(command) = pretty::prettify_command(&dist_path) {
                    pretty::run_pretty_command(&command, &dist_path)?;
                }
            }
        }
        Ok(())
    }

    pub fn meta(&self) -> Option<Meta> {
        match &self.input_data {
            InputData::Html { .. } => None,
            InputData::MarkdownWithFrontmatter { meta, .. } => Some(meta.clone()),
        }
    }

    pub fn list_page_meta(&self) -> Option<&im::HashMap<String, String>> {
        match &self.input_data {
            InputData::Html { meta, .. } => {
                if self.name.ends_with("_index.html") {
                    Some(meta)
                } else {
                    None
                }
            }
            InputData::MarkdownWithFrontmatter { .. } => None,
        }
    }

    pub fn dist_path(&self) -> OrError<Option<Utf8PathBuf>> {
        if self.subpath.as_str().contains("/_") {
            Ok(None)
        } else {
            match self.subpath.extension() {
                Some("md") | Some("html") | None => {
                    let file_stem = self.subpath.file_stem().ok_or_else(|| {
                        eyre!("Render not implemented for files like {}", self.subpath)
                    })?;
                    let parent = self.subpath.parent().ok_or_else(|| {
                        eyre!("Render needs a parent dir for {}", self.subpath)
                    })?;
                    let subpath = if file_stem == "index" {
                        parent.join("index.html")
                    } else {
                        parent.join(file_stem).join("index.html")
                    };
                    Ok(Some(Utf8PathBuf::new().join(DIST_DIR).join(subpath)))
                }
                Some(_) => {
                    Ok(Some(Utf8PathBuf::new().join(DIST_DIR).join(&self.subpath)))
                }
            }
        }
    }
}

impl fmt::Debug for RenderTarget {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut res = f.debug_struct("RenderTarget");
        res.field("name", &self.name);
        res.field("subpath", &self.subpath);
        match &self.input_data {
            InputData::Html { text, meta } => {
                res.field("meta", &format!("{meta:?}"));
                res.field("text", &format!("{} chars", text.len()));
            }
            InputData::MarkdownWithFrontmatter { text, meta } => {
                res.field("text", &format!("{} chars", text.len()));
                res.field("meta", &meta);
            }
        }
        res.finish()
    }
}

impl Meta {
    pub fn layout(&self) -> Option<String> {
        match self {
            Meta::Blogpost(..) => Some("layouts/blog_post.liquid".to_string()),
            Meta::StandalonePage(standalone_page) => Some(
                standalone_page
                    .layout
                    .clone()
                    .unwrap_or_else(|| "layouts/standalone_page.liquid".to_string()),
            ),
            Meta::BookshelfBook(..) => None,
        }
    }
}

fn post_number(name: &str) -> Option<u64> {
    name.strip_prefix("posts/")
        .and_then(|number_dot_md| number_dot_md.strip_suffix(".md"))
        .and_then(|number| number.parse().ok())
}
