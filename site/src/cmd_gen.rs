use crate::{
    config::DIST_DIR, external::External, file_watcher::FileTracker, run_mode::RunMode,
    web_server, work_planner::WorkPlanner, OrError,
};
use clap::Parser;
use eyre::{eyre, WrapErr};
use log::{error, info};
use std::{net::SocketAddr, sync::mpsc};

#[derive(Parser)]
pub struct Opts {
    /// Watch for changes and re-generate
    #[clap(long)]
    watch: bool,

    #[clap(long)]
    no_pretty: bool,

    #[clap(long, default_value = "127.0.0.1:3000", env = "LISTEN_ON")]
    listen_on: SocketAddr,

    #[clap(long, env = "FEEDBACK_SECRET_KEY")]
    feedback_secret_key: Option<String>,

    #[clap(long, env = "FEEDBACK_SMTP_KEY")]
    feedback_smtp_key: Option<String>,
}

pub fn run(
    Opts {
        watch,
        no_pretty,
        listen_on,
        feedback_secret_key,
        feedback_smtp_key,
    }: Opts,
) -> OrError<()> {
    let run_mode = if watch {
        RunMode::Continuous
    } else {
        RunMode::OneShot
    };
    run_once(
        run_mode,
        !no_pretty,
        listen_on.clone(),
        feedback_secret_key.as_ref(),
        feedback_smtp_key.as_ref(),
    )?;
    match run_mode {
        RunMode::Continuous => {}
        RunMode::OneShot => {
            // Run again to account for any changes an external tool
            // might have made in response to the first run.
            run_once(
                run_mode,
                !no_pretty,
                listen_on,
                feedback_secret_key.as_ref(),
                feedback_smtp_key.as_ref(),
            )?;
        }
    }
    Ok(())
}

pub fn run_once(
    run_mode: RunMode,
    pretty: bool,
    listen_on: SocketAddr,
    feedback_secret_key: Option<&String>,
    feedback_smtp_key: Option<&String>,
) -> OrError<()> {
    let (shutdown_tx, shutdown_rx) = mpsc::channel();
    let _: External = External::new(run_mode, shutdown_tx.clone())?;
    let (file_updates_tx, file_updates_rx) = mpsc::channel();
    let _: FileTracker =
        FileTracker::new(file_updates_tx, run_mode, shutdown_tx.clone())?;
    let (work_updates_tx, work_updates_rx) = mpsc::channel();
    let _: WorkPlanner = WorkPlanner::new(
        file_updates_rx,
        work_updates_tx,
        run_mode,
        pretty,
        shutdown_tx,
    )?;
    match run_mode {
        RunMode::OneShot => {
            for update in work_updates_rx.iter() {
                info!("{update:?}");
            }
        }
        RunMode::Continuous => {
            use tokio::{runtime, sync::watch};
            let runtime = runtime::Builder::new_multi_thread()
                .thread_name("web-server")
                .enable_all()
                .build()?;
            let (reload_tx, reload_rx) = watch::channel(());
            let feedback_secret_key = feedback_secret_key
                .expect("Must provide FEEDBACK_SECRET_KEY for --watch")
                .to_string();
            let feedback_smtp_key = feedback_smtp_key
                .expect("Must provide FEEDBACK_SMTP_KEY for --watch")
                .to_string();
            // Don't wait on the JoinHandle here because we don't care about
            // the web server closing cleanly.
            runtime.spawn(async move {
                web_server::serve(web_server::Opts {
                    reload_rx: Some(reload_rx),
                    dist_dir: DIST_DIR.into(),
                    listen_on: Some(listen_on),
                    feedback_secret_key,
                    feedback_smtp_key,
                })
                .await
                .wrap_err("starting dev server")?
                .serve
                .await
                .wrap_err("running dev server")?;
                Err::<(), _>(eyre!("dev server exited"))
            });
            for update in work_updates_rx.iter() {
                info!("Sending work update to reload_tx: {update:?}");
                if let Err(err) = reload_tx.send(()) {
                    error!("No one watching for reloads: {err}");
                    break;
                }
            }
        }
    }

    info!("Work updates done. Waiting for threads...");

    while shutdown_rx.recv().is_ok() {
        // waiting for all the shutdown senders to close
    }
    info!("Threads done");

    Ok(())
}
