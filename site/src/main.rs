mod absolutize_urls;
mod break_code_tag;
mod cmd_gen;
mod cmd_serve;
mod config;
mod external;
mod feedback;
mod file_watcher;
mod highlighter;
mod markdown_html;
mod pretty;
mod qrcode_tag;
mod render_store;
mod render_target;
mod run_mode;
mod template_store;
mod together_metas;
mod unclassify;
mod web_server;
mod web_state;
mod work_planner;

use clap::{Parser, Subcommand};
use log::error;
use std::{
    sync::mpsc,
    thread::{self, JoinHandle},
};

#[derive(Parser)]
struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,

    /// Enable debug logging
    #[clap(long)]
    debug: bool,
}

#[derive(Subcommand)]
enum Cmd {
    Gen(cmd_gen::Opts),
    Serve(cmd_serve::Opts),
}

pub type OrError<T> = eyre::Result<T>;

fn main() -> OrError<()> {
    let opts = Opts::parse();
    color_eyre::install()?;
    tracing_subscriber::fmt()
        .with_env_filter(&format!(
            "{},hyper::proto=warn",
            if opts.debug { "debug" } else { "info" }
        ))
        .init();
    match opts.cmd {
        Cmd::Gen(opts) => cmd_gen::run(opts)?,
        Cmd::Serve(opts) => cmd_serve::run(opts)?,
    }
    Ok(())
}

pub struct Shutdown;

pub(crate) fn spawn_thread_or_err<F>(
    name: &'static str,
    shutdown_tx: mpsc::Sender<Shutdown>,
    f: F,
) -> OrError<JoinHandle<()>>
where
    F: FnOnce() -> OrError<()> + Send + 'static,
{
    Ok(thread::Builder::new()
        .name(name.to_string())
        .spawn(move || {
            // This sender will be closed when the thread terminates.
            // When all the `Shutdown` senders close (which causes the
            // receiver to close), the main program ends.
            let _: mpsc::Sender<Shutdown> = shutdown_tx;
            if let Err(err) = f() {
                error!("Thread {name} crashed: {err}");
            }
        })?)
}
