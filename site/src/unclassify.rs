use crate::OrError;
use liquid_core::Result;
use liquid_core::Runtime;
use liquid_core::{Display_filter, Filter, FilterReflection, ParseFilter};
use liquid_core::{Value, ValueView};

fn unclassify(input: &dyn ValueView) -> OrError<Value> {
    use lol_html::{element, rewrite_str, Settings};
    if input.is_nil() {
        return Ok(Value::Nil);
    }
    let html = input.to_kstr();
    let html = rewrite_str(
        &html,
        Settings {
            element_content_handlers: vec![element!("*[class]", |el| {
                el.remove_attribute("class");
                Ok(())
            })],
            ..Settings::default()
        },
    )?;
    Ok(Value::scalar(html))
}

#[derive(Clone, ParseFilter, FilterReflection)]
#[filter(
    name = "unclassify",
    description = "Remove class attribute from HTML",
    parsed(UnclassifyFilter)
)]
pub struct Unclassify;

#[derive(Debug, Default, Display_filter)]
#[name = "unclassify"]
struct UnclassifyFilter;

impl Filter for UnclassifyFilter {
    fn evaluate(&self, input: &dyn ValueView, _runtime: &dyn Runtime) -> Result<Value> {
        unclassify(input).map_err(|err| {
            liquid_core::Error::with_msg(format!("Error in Unclassify: {err}"))
        })
    }
}
