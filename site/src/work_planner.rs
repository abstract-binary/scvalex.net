use crate::{
    config::{self, InputDir, DIST_DIR},
    file_watcher, pretty,
    render_store::RenderStore,
    render_target::{InputFormat, RenderTarget},
    run_mode::RunMode,
    spawn_thread_or_err,
    template_store::TemplateStore,
    OrError, Shutdown,
};
use camino::Utf8PathBuf;
use eyre::{bail, eyre};
use log::{debug, error, info};
use rayon::prelude::*;
use std::{fs, sync::mpsc};

/// The work planner receives a stream of file updates, decides what
/// work needs to be done as a result of them, then does it.
pub struct WorkPlanner {}

#[derive(Debug)]
enum WorkUnit {
    CopyFile {
        input_path: Utf8PathBuf,
        output_path: Utf8PathBuf,
    },
    DeleteFile(Utf8PathBuf),
    Prettify {
        path: Utf8PathBuf,
        command: (&'static str, &'static [&'static str]),
    },
    AddTemplate {
        path: Utf8PathBuf,
        name: String,
    },
    RemoveTemplate {
        name: String,
    },
    AddRender {
        name: String,
        src: Utf8PathBuf,
        subpath: Utf8PathBuf,
        input_format: InputFormat,
    },
    RemoveRender {
        subpath: Utf8PathBuf,
        name: String,
    },
    RenderAll,
    Multi(Vec<WorkUnit>),
    Noop,
}

#[derive(Debug)]
pub enum Update {
    AllWorkDone,
    PendingWorkDone,
}

impl WorkPlanner {
    pub fn new(
        file_updates_rx: mpsc::Receiver<file_watcher::Update>,
        updates_tx: mpsc::Sender<Update>,
        run_mode: RunMode,
        pretty: bool,
        shutdown_tx: mpsc::Sender<Shutdown>,
    ) -> OrError<Self> {
        let (todo_tx, todo_rx) = mpsc::channel();
        let work_schedule_thread =
            spawn_thread_or_err("work scheduler", shutdown_tx.clone(), move || {
                schedule_work(file_updates_rx, todo_tx)
            })?;
        let work_executor_thread =
            spawn_thread_or_err("work executor", shutdown_tx, move || {
                execute_work(todo_rx, updates_tx, pretty)
            })?;
        match run_mode {
            RunMode::Continuous => Ok(Self {}),
            RunMode::OneShot => {
                work_schedule_thread
                    .join()
                    .expect("Work scheduling thread panicked");
                work_executor_thread
                    .join()
                    .expect("Work executor thread panicked");
                Ok(Self {})
            }
        }
    }
}

fn schedule_work(
    file_updates_rx: mpsc::Receiver<file_watcher::Update>,
    todo_tx: mpsc::Sender<WorkUnit>,
) -> OrError<()> {
    for update in file_updates_rx.iter() {
        match update {
            file_watcher::Update::FileUpdate { path, kind } => {
                debug!("Handling update on {path} ({kind:?})");
                match (path, kind).try_into() {
                    Ok(work_unit) => todo_tx.send(work_unit)?,
                    Err(err) => error!("{err}"),
                }
            }
            file_watcher::Update::NoMorePendingUpdates => {
                info!("Batch of updates fully received");
                todo_tx.send(WorkUnit::RenderAll)?;
            }
        }
    }
    Ok(())
}

fn execute_work(
    todo_rx: mpsc::Receiver<WorkUnit>,
    updates_tx: mpsc::Sender<Update>,
    pretty: bool,
) -> OrError<()> {
    let template_store = TemplateStore::new()?;
    let render_store = RenderStore::new(pretty)?;
    let mut todo = vec![];
    while let Ok(work_unit) = todo_rx.recv() {
        match work_unit {
            WorkUnit::RenderAll => {
                if !todo.is_empty() {
                    debug!("Todo list has {} items", todo.len());
                    todo.par_iter().for_each(|work: &WorkUnit| {
                        work.execute(&template_store, &render_store)
                    });
                    todo.clear();
                    debug!("Todo list cleared");
                    WorkUnit::RenderAll.execute(&template_store, &render_store);
                    updates_tx.send(Update::PendingWorkDone)?;
                }
            }
            item => todo.push(item),
        }
    }
    updates_tx.send(Update::AllWorkDone)?;
    Ok(())
}

impl TryFrom<(Utf8PathBuf, file_watcher::Kind)> for WorkUnit {
    type Error = eyre::Error;

    fn try_from(
        (path, kind): (Utf8PathBuf, file_watcher::Kind),
    ) -> Result<Self, Self::Error> {
        use file_watcher::Kind;
        match kind {
            Kind::Add | Kind::Change => match config::input_dir(path.clone()) {
                Some((InputDir::Static, subpath)) => {
                    let output_path = Utf8PathBuf::new().join(DIST_DIR).join(&subpath);
                    let mut work = vec![WorkUnit::CopyFile {
                        input_path: path,
                        output_path: output_path.clone(),
                    }];
                    if let Some(command) = pretty::prettify_command(&output_path) {
                        work.push(WorkUnit::Prettify {
                            path: output_path,
                            command,
                        });
                    }
                    Ok(WorkUnit::Multi(work))
                }
                Some((InputDir::Templates, subpath)) => Ok(WorkUnit::AddTemplate {
                    path,
                    name: subpath.to_string(),
                }),
                Some((InputDir::Render, subpath)) => match subpath.extension() {
                    Some("html") => Ok(WorkUnit::AddRender {
                        src: path,
                        name: subpath.to_string(),
                        subpath,
                        input_format: InputFormat::Html,
                    }),
                    Some("md") => Ok(WorkUnit::AddRender {
                        src: path,
                        name: subpath.to_string(),
                        subpath,
                        input_format: InputFormat::MarkdownWithFrontmatter,
                    }),
                    Some("xml") => Ok(WorkUnit::AddRender {
                        src: path,
                        name: subpath.to_string(),
                        subpath,
                        input_format: InputFormat::Xml,
                    }),
                    Some(_) | None => {
                        bail!("Render not implemented for files like {subpath}",);
                    }
                },
                Some((InputDir::External { .. }, _)) => Ok(WorkUnit::Noop),
                None => {
                    bail!("Can't determine work for {path}");
                }
            },
            Kind::Remove => match config::input_dir(path.clone()) {
                Some((InputDir::Static, subpath)) => Ok(WorkUnit::DeleteFile(
                    Utf8PathBuf::new().join(DIST_DIR).join(&subpath),
                )),
                Some((InputDir::Templates, subpath)) => Ok(WorkUnit::RemoveTemplate {
                    name: subpath.to_string(),
                }),
                Some((InputDir::Render, subpath)) => Ok(WorkUnit::RemoveRender {
                    name: subpath.to_string(),
                    subpath,
                }),
                Some((InputDir::External { .. }, _)) => Ok(WorkUnit::Noop),
                None => {
                    bail!("Can't determine work for {path}");
                }
            },
        }
    }
}

impl WorkUnit {
    fn execute(&self, template_store: &TemplateStore, render_store: &RenderStore) {
        if let Err(err) = self.execute_or_err(template_store, render_store) {
            error!("Work item {self:?} failed:\n{err:?}");
        }
    }

    fn execute_or_err(
        &self,
        template_store: &TemplateStore,
        render_store: &RenderStore,
    ) -> OrError<()> {
        match self {
            Self::Noop => {}
            Self::Multi(work) => {
                let _: Vec<()> = work
                    .iter()
                    .map(|w| w.execute_or_err(template_store, render_store))
                    .collect::<OrError<Vec<()>>>()?;
            }
            Self::CopyFile {
                input_path,
                output_path,
            } => {
                debug!("COPY {input_path} -> {output_path}");
                fs::create_dir_all(
                    output_path
                        .parent()
                        .ok_or_else(|| eyre!("output path is somehow root"))?,
                )?;
                fs::copy(input_path, output_path)?;
            }
            Self::DeleteFile(path) => {
                debug!("RM {path}");
                fs::remove_file(path)?;
            }
            Self::Prettify { path, command } => {
                pretty::run_pretty_command(command, path)?;
            }
            Self::AddTemplate { path, name } => {
                debug!("TEMPLATE ADD {name}");
                template_store.add(name, path)?;
            }
            Self::RemoveTemplate { name } => {
                debug!("TEMPLATE RM {name}");
                template_store.remove(name);
            }
            Self::AddRender {
                name,
                subpath,
                src,
                input_format,
            } => {
                debug!("RENDER ADD {name} -> {subpath}");
                render_store.add(RenderTarget::new(name, src, subpath, *input_format)?);
            }
            Self::RemoveRender { name, subpath } => {
                debug!("RENDER RM {subpath}");
                fs::remove_file(Utf8PathBuf::new().join(DIST_DIR).join(subpath))?;
                render_store.remove(name);
            }
            Self::RenderAll => {
                debug!("RENDERALL");
                render_store.render_all(template_store)?;
                render_store.render_list_pages(template_store)?;
            }
        }
        Ok(())
    }
}
