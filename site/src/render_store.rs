use crate::{
    render_target::RenderTarget, template_store::TemplateStore,
    together_metas::TogetherMetas, OrError,
};
use eyre::{bail, eyre, WrapErr};
use itertools::Itertools;
use log::{debug, error, info};
use parking_lot::Mutex;
use rayon::prelude::*;
use std::{
    collections::{HashMap, HashSet},
    sync::{mpsc, Arc},
};
use voca_rs::case;

#[derive(Clone)]
pub struct RenderStore {
    render_targets: Arc<Mutex<HashMap<String, Arc<Mutex<RenderTarget>>>>>,
    pretty: bool,
}

impl RenderStore {
    pub fn new(pretty: bool) -> OrError<Self> {
        Ok(Self {
            render_targets: Arc::new(Mutex::new(HashMap::new())),
            pretty,
        })
    }

    pub fn add(&self, render_target: RenderTarget) {
        self.render_targets.lock().insert(
            render_target.name.clone(),
            Arc::new(Mutex::new(render_target)),
        );
    }

    pub fn remove(&self, name: &str) {
        self.render_targets.lock().remove(name);
    }

    pub fn render_all(&self, template_store: &TemplateStore) -> OrError<()> {
        let mut targets = self.targets();
        let errs = loop {
            let starting_targets_len = targets.len();
            let together_metas = TogetherMetas::new(&self.targets());
            let together_metas = together_metas.liquid()?;
            let errs = targets
                .par_iter()
                .filter_map(|x| {
                    let mut x = x.lock();
                    x.render(
                        template_store,
                        &together_metas,
                        im::HashMap::new(),
                        self.pretty,
                    )
                    .err()
                    .map(|err| (x.name.clone(), err))
                })
                .collect::<HashMap<String, eyre::Error>>();
            let targets_with_errs = errs.keys().cloned().collect::<HashSet<_>>();
            targets.retain(|x| {
                let x = x.lock();
                targets_with_errs.contains(&x.name)
            });
            if errs.is_empty() || starting_targets_len == targets.len() {
                break errs;
            } else {
                info!("Rendering incomplete. Retrying...");
            }
        };
        if !errs.is_empty() {
            for (name, err) in errs {
                error!("{name}: {err}");
            }
            bail!("Rendering errors (see above)");
        }
        Ok(())
    }

    pub fn render_list_pages(&self, template_store: &TemplateStore) -> OrError<()> {
        let targets = self.targets();
        let together_metas = TogetherMetas::new(&targets);
        let (errs_tx, errs_rx) = mpsc::channel();
        let index_targets: Vec<(RenderTarget, im::HashMap<String, String>)> = targets
            .into_iter()
            .filter_map(|x| {
                let x = x.lock();
                x.list_page_meta().map(|meta| (x.clone(), meta.clone()))
            })
            .collect_vec();
        let results = index_targets
            .par_iter()
            .map({
                let errs_tx = Mutex::new(errs_tx.clone());
                move |(index_target, index_meta)| {
                    let errs_tx = errs_tx.lock();
                    debug!("rendering {}", index_target.subpath);
                    let category_target = index_target.clone();
                    let mut index_target = index_target.clone();
                    let parent = index_target
                        .subpath
                        .parent()
                        .ok_or_else(|| eyre!("_index.html page has no parent"))?
                        .to_path_buf();
                    index_target.name = parent.join("index.html").to_string();
                    index_target.subpath = parent.clone();
                    let meta = {
                        [("main_index_page".into(), "true".into())]
                            .into_iter()
                            .collect()
                    };
                    if let Err(err) = index_target
                        .render(
                            template_store,
                            &together_metas.liquid()?,
                            meta,
                            self.pretty,
                        )
                        .wrap_err(format!("rendering {}", index_target.name))
                    {
                        errs_tx.send(err).unwrap();
                    }
                    if let Some(description_str) = index_meta.get("render_categories") {
                        let keys = together_metas.categories.keys().collect_vec();
                        let results = keys
                            .par_iter()
                            .map(|category| -> OrError<()> {
                                let mut category_index_target: RenderTarget =
                                    category_target.clone();
                                category_index_target.name =
                                    format!("{parent}/{category}/index.html");
                                category_index_target.subpath = parent.join(category);
                                let category_index_meta = {
                                    let mut meta = index_meta.clone();
                                    meta.entry("title".to_string()).and_modify(|title| {
                                        *title = title.replace(
                                            "All",
                                            &case::capitalize(category, true),
                                        );
                                    });
                                    meta.entry("description".to_string()).and_modify(
                                        |desc| {
                                            *desc = description_str
                                                .replace("CATEGORY", category);
                                        },
                                    );
                                    meta.insert(
                                        "selected_category".to_string(),
                                        category.to_string(),
                                    );
                                    meta
                                };
                                category_index_target
                                    .render(
                                        template_store,
                                        &together_metas
                                            .filter_posts_to_category(category)?
                                            .liquid()?,
                                        category_index_meta,
                                        self.pretty,
                                    )
                                    .wrap_err(format!(
                                        "rendering {}",
                                        category_index_target.name
                                    ))?;
                                Ok(())
                            })
                            .collect::<Vec<_>>();
                        for err in results.into_iter().filter_map(Result::err) {
                            errs_tx.send(err).unwrap();
                        }
                    }
                    Ok(())
                }
            })
            .collect::<Vec<_>>();
        for err in results.into_iter().filter_map(Result::err) {
            errs_tx.send(err).unwrap();
        }
        drop(errs_tx);
        let errs = errs_rx.iter().collect_vec();
        if !errs.is_empty() {
            bail!("Rendering errors: {errs:?}")
        }
        debug!("render_list_pages done");
        Ok(())
    }

    fn targets(&self) -> Vec<Arc<Mutex<RenderTarget>>> {
        self.render_targets
            .lock()
            .values()
            .map(Arc::clone)
            .collect()
    }
}
