use liquid::{model::ObjectIndex, reflection::TagReflection};
use liquid_core::{error::ResultLiquidReplaceExt, Expression, ParseTag, Renderable};

#[derive(Clone)]
pub struct BreakCodeTag;

#[derive(Debug)]
struct BreakCode {
    text: Expression,
}

impl TagReflection for BreakCodeTag {
    fn tag(&self) -> &'static str {
        "breakcode"
    }

    fn description(&self) -> &'static str {
        ""
    }
}

impl ParseTag for BreakCodeTag {
    fn parse(
        &self,
        mut arguments: liquid_core::TagTokenIter,
        _options: &liquid_core::Language,
    ) -> liquid_core::Result<Box<dyn liquid_core::Renderable>> {
        let text: Expression = arguments
            .expect_next("Text expected")?
            .expect_value()
            .into_result()?;
        arguments.expect_nothing()?;
        Ok(Box::new(BreakCode { text }))
    }

    fn reflection(&self) -> &dyn TagReflection {
        self
    }
}

impl Renderable for BreakCode {
    fn render_to(
        &self,
        writer: &mut dyn std::io::Write,
        runtime: &dyn liquid_core::Runtime,
    ) -> liquid_core::Result<()> {
        let text = self.text.evaluate(runtime)?;
        let broken_text = break_text(text.as_view().to_kstr().as_index());
        write!(writer, "<code>{broken_text}</code>").replace("Failed to render")?;
        Ok(())
    }
}

fn break_text(text: &str) -> String {
    text.replace('_', "_&ZeroWidthSpace;")
        .replace('.', ".&ZeroWidthSpace;")
        .replace('/', "/&ZeroWidthSpace;")
}
