use crate::{
    render_target::{Blogpost, BookshelfBook, Meta, RenderTarget},
    OrError,
};
use eyre::eyre;
use itertools::Itertools;
use parking_lot::Mutex;
use serde::Serialize;
use std::{
    cmp::Reverse,
    collections::{HashMap, HashSet},
    sync::Arc,
};

pub struct TogetherMetas {
    all_pages: Vec<(String, Option<ExtendedMeta<Meta>>)>,
    all_posts: Vec<ExtendedMeta<Blogpost>>,
    all_books: Vec<ExtendedMeta<BookshelfBook>>,
    posts_map: HashMap<u64, ExtendedMeta<Blogpost>>,
    pub categories: HashMap<String, Vec<ExtendedMeta<Blogpost>>>,
}

impl TogetherMetas {
    pub fn new(targets: &[Arc<Mutex<RenderTarget>>]) -> Self {
        let all_pages = targets
            .iter()
            .map(|x| {
                let x = x.lock();
                let emeta = x.meta().map(|meta| ExtendedMeta {
                    meta: Arc::new(meta),
                    content_html: Arc::new(x.content_html.clone()),
                });
                (x.name.clone(), emeta)
            })
            .collect::<Vec<(String, Option<ExtendedMeta<Meta>>)>>();
        let mut all_posts = all_pages
            .iter()
            .filter_map(|(_, emeta)| {
                emeta.as_ref().and_then(|emeta| {
                    if let Meta::Blogpost(blogpost) = &*emeta.meta {
                        if !blogpost.draft {
                            Some(ExtendedMeta {
                                meta: Arc::new(blogpost.clone()),
                                content_html: Arc::clone(&emeta.content_html),
                            })
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                })
            })
            .collect::<Vec<ExtendedMeta<Blogpost>>>();
        all_posts.sort_unstable_by_key(|x| Reverse(x.meta.post_number));
        let mut all_books = all_pages
            .iter()
            .filter_map(|(_, emeta)| {
                emeta.as_ref().and_then(|emeta| {
                    if let Meta::BookshelfBook(bookshelf_book) = &*emeta.meta {
                        Some(ExtendedMeta {
                            meta: Arc::new(bookshelf_book.clone()),
                            content_html: Arc::clone(&emeta.content_html),
                        })
                    } else {
                        None
                    }
                })
            })
            .collect::<Vec<_>>();
        all_books
            .sort_unstable_by_key(|x| Reverse((x.meta.read, x.meta.url_slug.clone())));
        let posts_map = all_posts
            .iter()
            .map(|emeta| (emeta.meta.post_number, emeta.clone()))
            .collect::<HashMap<u64, _>>();
        let mut categories = all_posts
            .iter()
            .flat_map(|emeta| {
                emeta
                    .meta
                    .categories
                    .iter()
                    .map(|cat| (cat.clone(), emeta.clone()))
            })
            .collect::<Vec<(String, ExtendedMeta<Blogpost>)>>()
            .into_iter()
            .fold(
                HashMap::new(),
                |mut acc: HashMap<String, Vec<ExtendedMeta<Blogpost>>>, (cat, emeta)| {
                    acc.entry(cat)
                        .and_modify(|cats| cats.push(emeta.clone()))
                        .or_insert_with(|| vec![emeta]);
                    acc
                },
            );
        for metas in categories.values_mut() {
            metas.sort_by(|x, y| y.meta.post_number.cmp(&x.meta.post_number));
        }
        Self {
            all_pages,
            all_posts,
            all_books,
            posts_map,
            categories,
        }
    }

    pub fn filter_posts_to_category(&self, category: &str) -> OrError<Self> {
        let category_posts: Vec<ExtendedMeta<Blogpost>> = self
            .categories
            .get(category)
            .ok_or_else(|| {
                eyre!(format!("category '{category}' not found in category map"))
            })?
            .to_vec();
        let category_post_numbers: HashSet<u64> = category_posts
            .iter()
            .map(|emeta| {
                // By construction, this cannot fail
                emeta.meta.post_number
            })
            .collect();
        let all_posts = self
            .all_posts
            .iter()
            .filter(|emeta| category_post_numbers.contains(&emeta.meta.post_number))
            .cloned()
            .collect();
        let posts_map = self
            .posts_map
            .iter()
            .filter_map(|(number, meta)| {
                if category_post_numbers.contains(number) {
                    Some((*number, meta.clone()))
                } else {
                    None
                }
            })
            .collect();
        Ok(Self {
            all_pages: self.all_pages.to_vec(),
            all_posts,
            all_books: vec![],
            posts_map,
            categories: self
                .categories
                .iter()
                .map(|(cat, metas)| (cat.clone(), metas.to_vec()))
                .collect(),
        })
    }

    pub fn liquid(&self) -> OrError<TogetherMetasLiquid> {
        Ok(TogetherMetasLiquid {
            posts: self
                .all_posts
                .iter()
                .map(|x| x.to_liquid_object())
                .try_collect()?,
            books: self
                .all_books
                .iter()
                .map(|x| x.to_liquid_object())
                .try_collect()?,
            posts_map: self
                .posts_map
                .iter()
                .map(|(number, x)| {
                    x.to_liquid_object()
                        .map(|object| (number.to_string(), object))
                })
                .try_collect()?,
        })
    }
}

#[derive(Clone)]
pub struct ExtendedMeta<M> {
    meta: Arc<M>,
    content_html: Arc<Option<String>>,
}

impl<M: Serialize> ExtendedMeta<M> {
    pub fn to_liquid_object(&self) -> OrError<liquid::Object> {
        use liquid::model::Value;
        let mut obj = liquid::Object::new();
        for (k, v) in liquid::to_object(&*self.meta)? {
            obj.insert(k, v);
        }
        if let Some(content_html) = self.content_html.as_ref() {
            obj.insert(
                "content_html".into(),
                Value::Scalar(content_html.clone().into()),
            );
        }
        Ok(obj)
    }
}

pub struct TogetherMetasLiquid {
    pub posts: Vec<liquid::Object>,
    pub books: Vec<liquid::Object>,
    pub posts_map: HashMap<String, liquid::Object>,
}
