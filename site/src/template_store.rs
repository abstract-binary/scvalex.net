use crate::{
    absolutize_urls::AbsolutizeUrls, break_code_tag::BreakCodeTag, config::DIST_DIR,
    markdown_html, qrcode_tag::QrCodeTag, render_target,
    together_metas::TogetherMetasLiquid, unclassify::Unclassify, OrError,
};
use camino::Utf8Path;
use eyre::{eyre, WrapErr};
use liquid::{
    partials::{EagerCompiler, InMemorySource},
    Parser, ParserBuilder, Template, ValueView,
};
use parking_lot::Mutex;
use std::{
    collections::HashMap,
    fs::{self, File},
    sync::Arc,
};

pub struct TemplateStore {
    state: Arc<Mutex<State>>,
}

struct State {
    parser: Option<Parser>,
    template_texts: HashMap<String, String>,
}

impl TemplateStore {
    pub fn new() -> OrError<Self> {
        Ok(Self {
            state: Arc::new(Mutex::new(State {
                parser: None,
                template_texts: HashMap::new(),
            })),
        })
    }

    pub fn add<P: AsRef<Utf8Path>>(&self, name: &str, path: P) -> OrError<()> {
        let text = fs::read_to_string(path.as_ref())?;
        let mut state = self.state.lock();
        state.template_texts.insert(name.to_string(), text);
        state.parser = None;
        Ok(())
    }

    pub fn remove(&self, name: &str) {
        let mut state = self.state.lock();
        state.template_texts.remove(&name.to_string());
        state.parser = None;
    }

    pub fn parse_standalone(&self, data: &str) -> OrError<Template> {
        let mut state = self.state.lock();
        match state.parser {
            None => {
                let mut templates = InMemorySource::new();
                for (n, t) in &state.template_texts {
                    templates.add(n.clone(), t.clone());
                }
                let parser = ParserBuilder::new()
                    .stdlib()
                    .tag(QrCodeTag)
                    .tag(BreakCodeTag)
                    .filter(AbsolutizeUrls)
                    .filter(Unclassify)
                    .partials(EagerCompiler::new(templates))
                    .build()?;
                state.parser = Some(parser);
            }
            Some(_) => {}
        }
        Ok(state.parser.as_ref().unwrap().parse(data)?)
    }

    pub fn render_standalone<P: AsRef<Utf8Path>>(
        &self,
        template: &Template,
        dest: P,
        together_metas: &TogetherMetasLiquid,
        meta: im::HashMap<String, String>,
    ) -> OrError<()> {
        let dest = dest.as_ref();
        let globals =
            self.prepare_globals(dest, together_metas, liquid::to_object(&meta)?)?;
        let globals = globals.to_map();
        template.render_to(
            &mut File::create(dest).wrap_err(dest.to_string())?,
            &globals,
        )?;
        Ok(())
    }

    pub fn render_content_html<P: AsRef<Utf8Path>>(
        &self,
        text: &str,
        dest: P,
        together_metas: &TogetherMetasLiquid,
        meta: &render_target::Meta,
    ) -> OrError<String> {
        let dest = dest.as_ref();
        let globals =
            self.prepare_globals(dest, together_metas, liquid::to_object(meta)?)?;
        let globals = globals.to_map();
        // First, pass the markdown text through liquid templating to
        // interpret any tags or partials.
        let mut text2: Vec<u8> = vec![];
        self.parse_standalone(text)?
            .render_to(&mut text2, &globals)
            .wrap_err("Rendering inner text")?;
        let mut html = markdown_html::to_html(
            std::str::from_utf8(&text2)?,
            markdown_html::Pass::Full,
        )?;
        if let Some((first_paragraph, rest)) = markdown_html::split_first_paragraph(&html)
        {
            html = [first_paragraph, rest].join("");
        }
        Ok(html)
    }

    pub fn render_with_layout_and_content<P: AsRef<Utf8Path>>(
        &self,
        layout: &str,
        dest: P,
        together_metas: &TogetherMetasLiquid,
        meta: &render_target::Meta,
        content_html: &str,
    ) -> OrError<()> {
        let dest = dest.as_ref();
        let template_text = format!(r###"{{% include "{layout}" %}}"###);
        let globals =
            self.prepare_globals(dest, together_metas, liquid::to_object(meta)?)?;
        let mut globals = globals.to_map();
        // Insert the generated HTML into the `content` tag, and
        // pass the *template* through liquid.
        globals.insert("content".to_string(), &content_html as &dyn ValueView);
        let template = self.parse_standalone(&template_text)?;
        // TODO Include title in prev and next link by replacing `title:
        // nil` with `title: prev_post.title` in blog_post_header.liquid.
        template
            .render_to(&mut File::create(dest)?, &globals)
            .context(format!(
                "Rendering {} with globals {:?}",
                dest,
                globals.keys()
            ))?;
        Ok(())
    }

    fn prepare_globals<'a, P: AsRef<Utf8Path>>(
        &self,
        dest: P,
        together_metas: &'a TogetherMetasLiquid,
        meta: liquid::Object,
    ) -> OrError<Globals<'a>> {
        Ok(Globals {
            url: url(dest)?,
            posts: &together_metas.posts,
            books: &together_metas.books,
            posts_map: &together_metas.posts_map,
            meta,
        })
    }
}

struct Globals<'a> {
    url: Option<String>,
    posts: &'a Vec<liquid::Object>,
    books: &'a Vec<liquid::Object>,
    posts_map: &'a HashMap<String, liquid::Object>,
    meta: liquid::Object,
}

impl<'a> Globals<'a> {
    fn to_map(&self) -> HashMap<String, &dyn ValueView> {
        let mut globals = HashMap::new();
        globals.insert("posts".to_string(), &self.posts as &dyn ValueView);
        globals.insert("books".to_string(), &self.books as &dyn ValueView);
        globals.insert("posts_map".to_string(), &self.posts_map as &dyn ValueView);
        if let Some(url) = self.url.as_ref() {
            globals.insert("url".to_string(), url as &dyn ValueView);
        }
        for (k, v) in &self.meta {
            globals.insert(k.to_string(), v as &dyn ValueView);
        }
        globals
    }
}

fn url<P: AsRef<Utf8Path>>(path: P) -> OrError<Option<String>> {
    let path = path.as_ref();
    if let Some("index.html") = path.file_name() {
        Ok(Some(
            path.strip_prefix(DIST_DIR)
                .map_err(|_| eyre!("output path did not begin with DIST_DIR: {path}"))?
                .parent()
                .map(|p| format!("/{p}/"))
                .unwrap_or_else(|| "/".to_string()),
        ))
    } else {
        Ok(None)
    }
}
