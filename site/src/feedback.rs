use axum::{debug_handler, extract::State, Json};
use eyre::bail;
use http::StatusCode;
use jiff::Zoned;
use mailgun_rs::{EmailAddress, Mailgun, MailgunRegion, Message};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use uuid::Uuid;

use crate::{web_state::SharedState, OrError};

#[derive(Debug, Serialize, Deserialize)]
struct FeedbackPrompt {
    timestamp: Zoned,
    nonce: Uuid,
    difficulty: u32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FeedbackSubmission {
    feedback: String,
    prompt: FeedbackPrompt,
    signature: String,
    answer: String,
    comment: String,
    path: String,
}

#[debug_handler(state = SharedState)]
pub async fn challenge_handler(
    State(state): State<SharedState>,
) -> Json<serde_json::Value> {
    let prompt = FeedbackPrompt {
        timestamp: Zoned::now(),
        nonce: Uuid::new_v4(),
        difficulty: 65536,
    };
    let signature = signature_hex(&state.feedback_secret_key, &prompt);
    #[derive(Serialize, Deserialize)]
    struct FeedbackChallenge {
        prompt: FeedbackPrompt,
        signature: String,
    }
    Json(serde_json::to_value(FeedbackChallenge { prompt, signature }).unwrap())
}

#[debug_handler(state = SharedState)]
pub async fn submit_handler(
    State(state): State<SharedState>,
    Json(payload): Json<FeedbackSubmission>,
) -> (StatusCode, String) {
    handle_eyre_error(submit_handler_internal(state, payload).await).await
}

async fn submit_handler_internal(
    state: SharedState,
    payload: FeedbackSubmission,
) -> OrError<String> {
    if payload.signature != signature_hex(&state.feedback_secret_key, &payload.prompt) {
        bail!("Invalid prompt")
    }
    if Zoned::now().timestamp().as_second()
        > 10 + payload.prompt.timestamp.timestamp().as_second()
    {
        bail!("Too slow")
    }
    let challenge_and_answer = payload.prompt.nonce.to_string()
        + "Path"
        + &payload.path
        + "Feedback"
        + &payload.feedback
        + "Comment"
        + &payload.comment
        + "Answer"
        + &payload.answer;
    log::info!("Verifying challenge {challenge_and_answer}");
    let mut h = Sha256::new();
    h.update(challenge_and_answer.as_bytes());
    let hb = h.finalize();
    if u32::from_be_bytes(hb[..4].try_into().unwrap()) >= payload.prompt.difficulty {
        bail!("Challenge failed");
    }
    let recipient = EmailAddress::address("feedback@scvalex.net");
    let message = Message {
        to: vec![recipient],
        subject: format!("Feedback: {}", payload.path),
        text: format!(
            "Feedback: {}\nComment: {}\nURL: https://scvalex.net{}",
            payload.feedback, payload.comment, payload.path
        ),
        ..Default::default()
    };
    let client = Mailgun {
        api_key: state.feedback_smtp_key.clone(),
        domain: "feedback.scvalex.net".into(),
    };
    let sender = EmailAddress::name_address("no-reply", "feedback@scvalex.net");
    client.send(MailgunRegion::EU, &sender, message)?;
    Ok("Ok".into())
}

fn signature_hex(feedback_secret_key: &str, prompt: &FeedbackPrompt) -> String {
    blake3::keyed_hash(
        feedback_secret_key.as_bytes().try_into().unwrap(),
        serde_json::to_string(prompt).unwrap().as_bytes(),
    )
    .to_hex()
    .to_string()
}

async fn handle_eyre_error(res: OrError<String>) -> (StatusCode, String) {
    match res {
        Ok(res) => (StatusCode::OK, res),
        Err(err) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Something went wrong: {err}"),
        ),
    }
}
