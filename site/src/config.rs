//! Configuration for the whole site generator.

use crate::run_mode::RunMode;
use camino::{Utf8Path, Utf8PathBuf};
use lazy_static::lazy_static;
use std::collections::HashMap;

#[derive(Debug)]
pub enum InputDir {
    Static,
    Templates,
    Render,
    External {
        cmd: &'static str,
        args: &'static [(RunMode, &'static [&'static str])],
    },
}

pub const DIST_DIR: &str = "dist";

pub const INPUT_DIRS: &[(&str, InputDir)] = {
    use InputDir::*;
    &[
        ("static", Static),
        ("styles_out", Static),
        ("templates", Templates),
        ("pages", Render),
        (
            "styles/screen3.css",
            External {
                cmd: "node",
                args: &[
                    (
                        RunMode::OneShot,
                        &[
                            "$NODE_PATH/.bin/tailwindcss",
                            "--minify",
                            "--no-autoprefixer",
                            "-o",
                            "styles_out/r/screen3.css",
                            "-i",
                            "styles/screen3.css",
                        ],
                    ),
                    (
                        RunMode::Continuous,
                        &[
                            "$NODE_PATH/.bin/tailwindcss",
                            "--minify",
                            "--no-autoprefixer",
                            "--watch",
                            "-o",
                            "styles_out/r/screen3.css",
                            "-i",
                            "styles/screen3.css",
                        ],
                    ),
                ],
            },
        ),
    ]
};

lazy_static! {
    static ref INPUT_DIR_MAP: HashMap<&'static str, &'static InputDir> = {
        INPUT_DIRS
            .iter()
            .map(|(prefix, kind)| (*prefix, kind))
            .collect()
    };
}

pub fn input_dir<P: AsRef<Utf8Path>>(
    path: P,
) -> Option<(&'static InputDir, Utf8PathBuf)> {
    if let Some(prefix) = path_prefix(path.as_ref()) {
        INPUT_DIR_MAP.get(prefix).map(|x| {
            (
                *x,
                path.as_ref().strip_prefix(prefix).unwrap().to_path_buf(),
            )
        })
    } else {
        None
    }
}

fn path_prefix(path: &Utf8Path) -> Option<&str> {
    use camino::Utf8Component;
    if let Some(Utf8Component::Normal(prefix)) = path.components().next() {
        Some(prefix)
    } else {
        None
    }
}
