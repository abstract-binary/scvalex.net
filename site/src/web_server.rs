use crate::{
    feedback,
    web_state::{AppState, SharedState},
    OrError,
};
use axum::{
    self,
    body::Body,
    debug_handler,
    extract::{ws::WebSocket, State, WebSocketUpgrade},
    response::Redirect,
    routing::{post, IntoMakeService},
    Router,
};
use camino::{Utf8Path, Utf8PathBuf};
use http::{HeaderValue, Request, Response, StatusCode, Uri};
use log::{debug, error, info};
use std::net::SocketAddr;
use tokio::{net::TcpListener, sync::watch};
use tower_http::{
    cors::CorsLayer,
    services::{ServeDir, ServeFile},
};

pub struct WebServer {
    pub serve: axum::serve::Serve<TcpListener, IntoMakeService<Router>, Router>,
    // Used in tests.
    #[allow(dead_code)]
    pub address: String,
}

pub struct Opts {
    /// If passed, the serve will run in "dev" mode, and the
    /// `/live-reload` endpoint will be available.  A message will be
    /// sent throug this WebSocket whenever a message is received from
    /// `reload_tx`.
    pub reload_rx: Option<watch::Receiver<()>>,

    /// Path to the directory of files to serve.
    pub dist_dir: Utf8PathBuf,

    /// The address to listen on.  If not specified, a random port on
    /// `127.0.0.1` will be used.
    pub listen_on: Option<SocketAddr>,

    /// The secret key used to cryptographically sign the feedback challenge.
    pub feedback_secret_key: String,

    /// SMTP auth credential.
    pub feedback_smtp_key: String,
}

pub async fn serve(opts: Opts) -> OrError<WebServer> {
    use axum::routing::get;
    use tower::ServiceBuilder;
    use tower_http::{compression::CompressionLayer, trace};
    use tracing::Level;
    let listen_on = opts
        .listen_on
        .unwrap_or_else(|| "127.0.0.1:0".parse().unwrap());
    let app = Router::new()
        .route("/live-reload", get(live_reload_handler))
        .route("/status", get(status_handler))
        .route(
            "/atom.xml",
            get(move |state| atom_handler(state, "atom.xml".to_string()))
                .layer(cors_allow_any()),
        )
        .route(
            "/books.xml",
            get(move |state| atom_handler(state, "books.xml".to_string()))
                .layer(cors_allow_any()),
        )
        .route("/feed/", get(|| async { Redirect::permanent("/atom.xml") }))
        .route("/feedback", get(feedback::challenge_handler))
        .route("/feedback", post(feedback::submit_handler))
        .route("/", get(file_handler))
        .route("/{*segment}", get(file_handler))
        .with_state(AppState::new(
            opts.reload_rx,
            opts.dist_dir,
            opts.feedback_secret_key,
            opts.feedback_smtp_key,
        ))
        .layer(
            ServiceBuilder::new().layer(CompressionLayer::new()).layer(
                trace::TraceLayer::new_for_http()
                    .on_request(trace::DefaultOnRequest::new().level(Level::INFO))
                    .on_response(trace::DefaultOnResponse::new().level(Level::INFO))
                    .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO)),
            ),
        );
    let listener = tokio::net::TcpListener::bind(listen_on).await?;
    let address = listener.local_addr()?.to_string();
    info!("Listening on http://{}/", address);
    let server = WebServer {
        serve: axum::serve(listener, app.into_make_service()),
        address,
    };
    Ok(server)
}

#[debug_handler(state = SharedState)]
async fn status_handler(State(state): State<SharedState>) -> String {
    [
        "Status: Ok".to_string(),
        format!(
            "Host: {}",
            nix::unistd::gethostname()
                .map(|s| s.into_string())
                .unwrap_or_else(|_| Ok("unknown".to_string()))
                .unwrap_or_else(|_| "invalid utf-8 string".to_string())
        ),
        format!("Uptime: {:?}", state.start_time.elapsed()),
        "".to_string(),
    ]
    .join("\n")
}

#[debug_handler(state = SharedState)]
async fn file_handler(
    uri: Uri,
    State(state): State<SharedState>,
) -> Result<Response<Body>, (StatusCode, String)> {
    use bstr::ByteSlice;
    use http::header::{CACHE_CONTROL, CONTENT_TYPE};
    use http_body_util::BodyExt;
    let mut res = get_static_file_from_dir(uri.clone(), &state.dist_dir).await?;
    if res.status() == StatusCode::NOT_FOUND {
        debug!("not found: {res:?}");
        not_found_response(&state.dist_dir).await
    } else {
        res.headers_mut()
            .insert(CACHE_CONTROL, HeaderValue::from_static("no-store"));
        let is_text_html = matches!(
            res.headers().get(CONTENT_TYPE).map(|x| x.to_str()),
            Some(Ok("text/html"))
        );
        if state.reload_rx.is_some() && is_text_html {
            debug!("found html: {res:?}");
            let body = res.body_mut();
            match body.collect().await {
                Ok(new_body) => {
                    let bytes = new_body.to_bytes().replace(
                        "</head>",
                        r###"<script src="/r/live-reload.js"></script></head>"###,
                    );
                    debug!("replaced data with {} bytes", bytes.len());
                    *body = Body::from(bytes);
                    Ok(res)
                }
                Err(err) => Err(internal_server_error(
                    err,
                    "Error munging HTML with live-reload",
                )),
            }
        } else {
            debug!("found file: {res:?}");
            Ok(res)
        }
    }
}

#[debug_handler(state = SharedState)]
async fn atom_handler(
    State(state): State<SharedState>,
    filename: String,
) -> Result<Response<Body>, (StatusCode, String)> {
    use http::header::CONTENT_TYPE;
    get_static_file(state.dist_dir.join(&filename))
        .await
        .map(|mut res| {
            res.headers_mut().insert(
                CONTENT_TYPE,
                HeaderValue::from_static("application/atom+xml"),
            );
            res
        })
}

#[debug_handler(state = SharedState)]
async fn live_reload_handler(
    State(state): State<SharedState>,
    ws: WebSocketUpgrade,
) -> Result<Response<Body>, (StatusCode, String)> {
    use axum::response::IntoResponse;
    if let Some(reload_rx) = &state.reload_rx {
        let reload_rx = reload_rx.clone();
        Ok(ws
            .on_failed_upgrade(|err| {
                error!("Live-reload websocket failed to upgrade: {err}")
            })
            .on_upgrade(|ws| handle_live_reload(reload_rx, ws))
            .into_response())
    } else {
        not_found_response(&state.dist_dir).await
    }
}

async fn handle_live_reload(reload_rx: watch::Receiver<()>, mut ws: WebSocket) {
    use tokio_stream::{wrappers::WatchStream, StreamExt};
    info!("Received live-reload subscription");
    let mut reload_stream = WatchStream::new(reload_rx.clone()).skip(1);
    while let Some(()) = reload_stream.next().await {
        info!("Live-reload event");
        if let Err(err) = ws.send("reload".into()).await {
            error!("Failed to send reload event: {err}");
            break;
        }
    }
}

async fn not_found_response(
    dist_dir: &Utf8Path,
) -> Result<Response<Body>, (StatusCode, String)> {
    get_static_file(dist_dir.join("404").join("index.html"))
        .await
        .map(|mut res| {
            *res.status_mut() = StatusCode::NOT_FOUND;
            res
        })
}

async fn get_static_file<P: AsRef<std::path::Path>>(
    file: P,
) -> Result<Response<Body>, (StatusCode, String)> {
    use tower::ServiceExt;
    ServeFile::new(file.as_ref())
        .oneshot(Request::new(()))
        .await
        .map(|res| res.map(Body::new))
        .map_err(|err| internal_server_error(err, "static file not found"))
}

async fn get_static_file_from_dir(
    uri: Uri,
    dist_dir: &Utf8Path,
) -> Result<Response<Body>, (StatusCode, String)> {
    use tower::ServiceExt;
    let req = Request::builder().uri(&uri).body(Body::empty()).unwrap();
    ServeDir::new(dist_dir)
        .oneshot(req)
        .await
        // .map(|res| res.map(boxed))
        .map(|res| res.map(Body::new))
        .map_err(|err| {
            internal_server_error(err, &format!("error getting static file: {uri}"))
        })
}

fn internal_server_error(err: impl std::fmt::Display, msg: &str) -> (StatusCode, String) {
    error!("Internal error: {msg}: {err}");
    (
        StatusCode::INTERNAL_SERVER_ERROR,
        "Internal Server Error. Please email site@scvalex.net".to_string(),
    )
}

fn cors_allow_any() -> CorsLayer {
    use tower_http::cors::Any;
    CorsLayer::new()
        .allow_methods([http::Method::GET])
        .allow_origin(Any)
}

#[cfg(test)]
mod tests {
    use super::*;
    use reqwest::StatusCode;
    use test_case::test_case;

    async fn setup() -> OrError<String> {
        let opts = Opts {
            reload_rx: None,
            dist_dir: Utf8PathBuf::new().join("..").join("dist"),
            listen_on: None,
            feedback_secret_key: "some key that is 32 chars longgg".into(),
            feedback_smtp_key: "smtp key".into(),
        };
        let WebServer { serve, address } = serve(opts).await?;
        let url = format!("http://{}", address);
        tokio::spawn(async { serve.await });
        Ok(url)
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn get_index() -> OrError<()> {
        let url = setup().await?;
        let resp = reqwest::get(format!("{url}")).await?;
        assert_eq!(resp.status(), StatusCode::OK);
        Ok(())
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn get_does_not_exist() -> OrError<()> {
        let url = setup().await?;
        let resp = reqwest::get(format!("{url}/does-not-exist")).await?;
        assert_eq!(resp.status(), StatusCode::NOT_FOUND);
        Ok(())
    }

    #[test_case("atom.xml")]
    #[test_case("books.xml")]
    #[tokio::test(flavor = "multi_thread")]
    async fn get_atom(filename: &str) -> OrError<()> {
        let url = setup().await?;
        let resp = reqwest::get(format!("{url}/{filename}")).await?;
        assert_eq!(resp.status(), StatusCode::OK);
        assert_eq!(
            resp.headers().get("content-type").unwrap(),
            "application/atom+xml"
        );
        assert_eq!(
            resp.headers().get("access-control-allow-origin").unwrap(),
            "*"
        );
        Ok(())
    }
}
