use camino::Utf8PathBuf;
use std::{sync::Arc, time::Instant};
use tokio::sync::watch;

pub type SharedState = Arc<AppState>;

pub struct AppState {
    pub reload_rx: Option<watch::Receiver<()>>,
    pub dist_dir: Utf8PathBuf,
    pub start_time: Instant,
    pub feedback_secret_key: String,
    pub feedback_smtp_key: String,
}

impl AppState {
    pub fn new(
        reload_rx: Option<watch::Receiver<()>>,
        dist_dir: Utf8PathBuf,
        feedback_secret_key: String,
        feedback_smtp_key: String,
    ) -> Arc<Self> {
        Arc::new(Self {
            reload_rx,
            dist_dir,
            start_time: Instant::now(),
            feedback_secret_key,
            feedback_smtp_key,
        })
    }
}
