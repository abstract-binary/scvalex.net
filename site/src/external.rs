use crate::{
    config::{InputDir, INPUT_DIRS},
    run_mode::RunMode,
    spawn_thread_or_err, OrError, Shutdown,
};
use eyre::{eyre, WrapErr};
use log::info;
use std::{
    io::{BufRead, BufReader, Read},
    process::{Command, Stdio},
    sync::mpsc,
};

/// Run external generators/watchers
pub struct External {}

impl External {
    pub fn new(run_mode: RunMode, shutdown_tx: mpsc::Sender<Shutdown>) -> OrError<Self> {
        for (dir, kind) in INPUT_DIRS {
            match kind {
                InputDir::External { cmd, args } => {
                    let node_path = std::env::var("NODE_PATH")
                        .unwrap_or_else(|_| "./node_modules".to_string());
                    let args = args
                        .iter()
                        .find_map(|(rm, args)| if *rm == run_mode { Some(args) } else { None })
                        .ok_or_else(|| {
                            eyre!("Could not find arguments for run mode {run_mode:?} for {dir}",)
                        })?.iter().map(|arg| arg.replace("$NODE_PATH", &node_path)).collect::<Vec<_>>();
                    let mut handle = Command::new(cmd)
                        .args(&args)
                        // .stdin(Stdio::null()) Closing stdin kill yarn
                        .stdout(Stdio::piped())
                        .stderr(Stdio::piped())
                        .spawn()
                        .wrap_err(format!("Running {} {}", cmd, args.join(" ")))?;
                    spawn_thread_or_err(
                        "stdout capture",
                        shutdown_tx.clone(),
                        move || {
                            display_stdout(dir, handle.stdout.take().unwrap())
                                .wrap_err(format!("Stdout capture for {dir} crashed"))
                        },
                    )?;
                    spawn_thread_or_err(
                        "stderr capture",
                        shutdown_tx.clone(),
                        move || {
                            display_stderr(dir, handle.stderr.take().unwrap())
                                .context(format!("Stderr capture for {dir} crashed"))
                        },
                    )?;
                    info!("EXTERNAL started for {dir}");
                }
                InputDir::Static | InputDir::Templates | InputDir::Render => {}
            }
        }
        Ok(Self {})
    }
}

fn display_stdout<R: Read>(tag: &str, reader: R) -> OrError<()> {
    let reader = BufReader::new(reader);
    for line in reader.lines() {
        let line = line?;
        info!("{tag}: {line}");
    }
    Ok(())
}

fn display_stderr<R: Read>(tag: &str, reader: R) -> OrError<()> {
    let reader = BufReader::new(reader);
    for line in reader.lines() {
        let line = line?;
        info!("{tag}: {line}");
    }
    Ok(())
}
