use crate::OrError;
use lazy_static::lazy_static;
use log::info;
use pulldown_cmark_escape::escape_html;
use std::collections::HashMap;
use tree_sitter::Language;
use tree_sitter_highlight::{
    Highlight, HighlightConfiguration, HighlightEvent, Highlighter,
};
use tree_sitter_language::LanguageFn;

pub fn highlight(text: &str, lang: &str) -> OrError<String> {
    let highlight_config = highlight_config(lang)?;
    match highlight_config {
        None => {
            let mut out = String::new();
            escape_html(&mut out, text)?;
            Ok(out)
        }
        Some((highlight_config, names)) => {
            let mut highlighter = Highlighter::new();
            let highlights = highlighter.highlight(
                &highlight_config,
                text.as_bytes(),
                None,
                |_| None,
            )?;
            let mut out = String::new();
            let mut last_span_start_start = 0;
            let mut last_span_start_end = 0;
            for event in highlights {
                let event = event?;
                match event {
                    HighlightEvent::Source { start, end } => {
                        escape_html(&mut out, &text[start..end])?;
                    }
                    HighlightEvent::HighlightStart(Highlight(idx)) => {
                        last_span_start_start = out.len();
                        out.push_str(&format!(
                            r###"<span class="token {}">"###,
                            names[idx].replace('.', "-"),
                        ));
                        last_span_start_end = out.len();
                    }
                    HighlightEvent::HighlightEnd => {
                        if out.len() == last_span_start_end {
                            info!(
                                "Removing empty span: {}",
                                &out[last_span_start_start..]
                            );
                            out.truncate(last_span_start_start);
                        } else {
                            out.push_str("</span>");
                        }
                    }
                }
            }
            Ok(out)
        }
    }
}

fn highlight_config(
    lang: &str,
) -> OrError<Option<(HighlightConfiguration, &'static [&'static str])>> {
    match HIGHLIGHTERS.get(lang) {
        None => Ok(None),
        Some((language, query, names)) => {
            let language = Language::new(*language);
            let mut config = HighlightConfiguration::new(language, lang, *query, "", "")?;
            config.configure(names);
            Ok(Some((config, names)))
        }
    }
}

type HighlightersType =
    HashMap<String, (LanguageFn, &'static str, &'static [&'static str])>;

lazy_static! {
    static ref HIGHLIGHTERS: HighlightersType = {
        let mut h = HashMap::new();
        h.insert(
            "rust".to_string(),
            (
                tree_sitter_rust::LANGUAGE,
                RUST_HIGHLIGHTS_QUERY,
                RUST_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "nix".to_string(),
            (
                tree_sitter_nix::LANGUAGE,
                NIX_HIGHLIGHTS_QUERY,
                NIX_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "bash".to_string(),
            (
                tree_sitter_bash::LANGUAGE,
                BASH_HIGHLIGHTS_QUERY,
                BASH_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "json".to_string(),
            (
                tree_sitter_json::LANGUAGE,
                JSON_HIGHLIGHTS_QUERY,
                JSON_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "c".to_string(),
            (
                tree_sitter_c::LANGUAGE,
                C_HIGHLIGHTS_QUERY,
                C_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "yaml".to_string(),
            (
                tree_sitter_yaml::LANGUAGE,
                YAML_HIGHLIGHTS_QUERY,
                YAML_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "python".to_string(),
            (
                tree_sitter_python::LANGUAGE,
                PYTHON_HIGHLIGHTS_QUERY,
                PYTHON_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "javascript".to_string(),
            (
                tree_sitter_javascript::LANGUAGE,
                JAVASCRIPT_HIGHLIGHTS_QUERY,
                JAVASCRIPT_HIGHLIGHT_NAMES,
            ),
        );
        h.insert(
            "toml".to_string(),
            (
                tree_sitter_toml::LANGUAGE,
                TOML_HIGHLIGHTS_QUERY,
                TOML_HIGHLIGHT_NAMES,
            ),
        );
        // TODO-someday Get a real ini tree-sitter.
        h.insert(
            "ini".to_string(),
            (
                tree_sitter_toml::LANGUAGE,
                TOML_HIGHLIGHTS_QUERY,
                TOML_HIGHLIGHT_NAMES,
            ),
        );
        h
    };
}

const RUST_HIGHLIGHT_NAMES: &[&str] = &[
    "attribute",
    "comment",
    "constant",
    "function",
    "function.builtin",
    "keyword",
    "operator",
    "property",
    "punctuation",
    "string",
    "string.special",
    "tag",
    "type",
    "type.builtin",
    "variable",
    "variable.builtin",
    "variable.parameter",
];

const RUST_HIGHLIGHTS_QUERY: &str = include_str!("../tree-sitter/rust-queries.scm");

const NIX_HIGHLIGHT_NAMES: &[&str] = &[
    "comment",
    "embedded",
    "function.builtin",
    "keyword",
    "number",
    "operator",
    "punctuation",
    "string",
    "variable.builtin",
    "variable.parameter",
];

const NIX_HIGHLIGHTS_QUERY: &str = include_str!("../tree-sitter/nix-queries.scm");

const BASH_HIGHLIGHT_NAMES: &[&str] = &[
    "comment", "constant", "embedded", "function", "number", "operator", "property",
    "string",
];

const BASH_HIGHLIGHTS_QUERY: &str = tree_sitter_bash::HIGHLIGHT_QUERY;

const JSON_HIGHLIGHT_NAMES: &[&str] = &["escape", "keyword", "string"];

const JSON_HIGHLIGHTS_QUERY: &str = include_str!("../tree-sitter/json-queries.scm");

const C_HIGHLIGHT_NAMES: &[&str] = &[
    "constant",
    "delimiter",
    "function",
    "keyword",
    "label",
    "number",
    "operator",
    "string",
    "type",
];

const C_HIGHLIGHTS_QUERY: &str = include_str!("../tree-sitter/c-queries.scm");

const YAML_HIGHLIGHT_NAMES: &[&str] = &[
    "constant",
    "boolean",
    // "string",
    "number",
    "comment",
    "type",
    "keyword",
    "field",
    "punctuation",
];

const YAML_HIGHLIGHTS_QUERY: &str = include_str!("../tree-sitter/yaml-queries.scm");

const PYTHON_HIGHLIGHT_NAMES: &[&str] = &[
    "comment",
    "constant",
    "constructor",
    "embedded",
    "escape",
    "function",
    "keyword",
    "number",
    "property",
    "string",
    "type",
];

const PYTHON_HIGHLIGHTS_QUERY: &str = include_str!("../tree-sitter/python-queries.scm");

const JAVASCRIPT_HIGHLIGHT_NAMES: &[&str] = &[
    "constant",
    "constructor",
    "delimiter",
    "embedded",
    "function",
    "keyword",
    "number",
    "operator",
    "punctuation",
    "string",
];

const JAVASCRIPT_HIGHLIGHTS_QUERY: &str = tree_sitter_javascript::HIGHLIGHT_QUERY;

const TOML_HIGHLIGHT_NAMES: &[&str] = &[
    "comment",
    "constant",
    "number",
    "operator",
    "punctuation",
    "string",
];

const TOML_HIGHLIGHTS_QUERY: &str = include_str!("../tree-sitter/toml-queries.scm");
