// Copyright 2015 Google Inc. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Originally copied from commit
// ea1b4cd4dbcf912129f14dd81ad4809f0d5d1999 in
// https://github.com/raphlinus/pulldown-cmark.git.

use crate::{highlighter, OrError};
use eyre::bail;
use pulldown_cmark::{
    Alignment, CodeBlockKind,
    Event::{self, *},
    HeadingLevel, LinkType, Options, Parser, Tag, TagEnd,
};
use pulldown_cmark_escape::{escape_href, escape_html, escape_html_body_text, StrWrite};
use std::{collections::HashMap, mem};

#[derive(Debug)]
pub enum Pass {
    First,
    Full,
}

/// Run the text through the markdown parser to generate HTML.  This
/// must happen after interpreting any template tags in the Markdown
/// itself.  Otherwise, quotes will be turned into smart-quotes here,
/// and that breaks liquid templating later.
pub fn to_html(text: &str, pass: Pass) -> OrError<String> {
    let mut options = Options::empty();
    options.insert(Options::ENABLE_STRIKETHROUGH);
    options.insert(Options::ENABLE_SMART_PUNCTUATION);
    options.insert(Options::ENABLE_TABLES);
    let parser = Parser::new_ext(text, options);
    gen_html(parser, pass)
}

pub fn split_first_paragraph(text: &str) -> Option<(String, String)> {
    let barrier = "<!-- more -->";
    text.find(barrier).map(|pos| {
        (
            text[..pos].to_string(),
            text[pos + barrier.len()..].to_string(),
        )
    })
}

enum TableState {
    Head,
    Body,
}

#[derive(Debug)]
enum SelectedWriter {
    FullText,
    HeadingText(HeadingLevel, Option<String>, Vec<String>),
    CodeBlock(String),
}

impl SelectedWriter {
    fn is_full_text(&self) -> bool {
        matches!(self, Self::FullText)
    }
}

struct HtmlWriter<I> {
    /// Iterator supplying events.
    iter: I,

    full_text: String,
    heading_text: String,
    codeblock_text: String,
    selected_writer: SelectedWriter,

    toc_position: Option<usize>,
    headings: Vec<(String, HeadingLevel, String)>,

    /// Whether or not the last write wrote a newline.
    end_newline: bool,

    table_state: TableState,
    table_alignments: Vec<Alignment>,
    table_cell_index: usize,
    numbers: HashMap<String, usize>,
    pass: Pass,
}

impl<'iter, I> HtmlWriter<I>
where
    I: Iterator<Item = Event<'iter>>,
{
    fn new(iter: I, pass: Pass) -> Self {
        Self {
            iter,
            full_text: String::new(),
            heading_text: String::new(),
            codeblock_text: String::new(),
            toc_position: None,
            headings: vec![],
            selected_writer: SelectedWriter::FullText,
            end_newline: true,
            table_state: TableState::Head,
            table_alignments: vec![],
            table_cell_index: 0,
            numbers: HashMap::new(),
            pass,
        }
    }

    fn writer(&mut self) -> &mut String {
        match self.selected_writer {
            SelectedWriter::FullText => &mut self.full_text,
            SelectedWriter::HeadingText(..) => &mut self.heading_text,
            SelectedWriter::CodeBlock(..) => &mut self.codeblock_text,
        }
    }

    /// Writes a new line.
    fn write_newline(&mut self) -> OrError<()> {
        self.end_newline = true;
        Ok(self.writer().write_str("\n")?)
    }

    /// Writes a buffer, and tracks whether or not a newline was written.
    #[inline]
    fn write(&mut self, s: &str) -> OrError<()> {
        self.writer().write_str(s)?;

        if !s.is_empty() {
            self.end_newline = s.ends_with('\n');
        }
        Ok(())
    }

    fn run(mut self) -> OrError<String> {
        while let Some(event) = self.iter.next() {
            match event {
                Start(tag) => {
                    self.start_tag(tag)?;
                }
                End(tag) => {
                    self.end_tag(tag)?;
                }
                Text(text) => match self.selected_writer {
                    SelectedWriter::FullText | SelectedWriter::HeadingText(..) => {
                        escape_html_body_text(self.writer(), &text)?;
                    }
                    SelectedWriter::CodeBlock(_) => {
                        self.write(&text)?;
                    }
                },
                Code(text) => {
                    self.write("<code>")?;
                    escape_html_body_text(self.writer(), &text)?;
                    self.write("</code>")?;
                }
                Html(html) => {
                    self.write(&html)?;
                }
                SoftBreak => {
                    self.write_newline()?;
                }
                HardBreak => {
                    self.write("<br />\n")?;
                }
                Rule => {
                    if self.end_newline {
                        self.write("<hr />\n")?;
                    } else {
                        self.write("\n<hr />\n")?;
                    }
                }
                FootnoteReference(name) => {
                    let len = self.numbers.len() + 1;
                    self.write("<sup class=\"footnote-reference\"><a href=\"#")?;
                    escape_html(self.writer(), &name)?;
                    self.write("\">")?;
                    let number = *self.numbers.entry(name.into_string()).or_insert(len);
                    write!(self.writer(), "{}", number)?;
                    self.write("</a></sup>")?;
                }
                TaskListMarker(true) => {
                    self.write(
                        "<input disabled=\"\" type=\"checkbox\" checked=\"\"/>\n",
                    )?;
                }
                TaskListMarker(false) => {
                    self.write("<input disabled=\"\" type=\"checkbox\"/>\n")?;
                }
                InlineHtml(str) => {
                    self.write(&str)?;
                }
                InlineMath(str) => self.write(&str)?,
                DisplayMath(str) => self.write(&str)?,
            }
        }
        self.finish()
    }

    fn finish(self) -> OrError<String> {
        if !self.selected_writer.is_full_text() {
            bail!(
                "Expected HTML generator to finish writing to FullText: {:?}",
                self.selected_writer
            );
        }
        let text = match self.toc_position {
            None => self.full_text,
            Some(pos) => {
                let mut text = String::new();
                if matches!(self.pass, Pass::Full) {
                    write_toc(&mut text, TocStyle::Floating, &self.headings)?;
                }
                text.write_str(&self.full_text[..pos])?;
                write_toc(&mut text, TocStyle::Fixed, &self.headings)?;
                text.write_str(&self.full_text[pos..])?;
                text
            }
        };
        Ok(text)
    }

    /// Writes the start of an HTML tag.
    fn start_tag(&mut self, tag: Tag<'iter>) -> OrError<()> {
        match tag {
            Tag::Paragraph => {
                if self.end_newline {
                    self.write("<p>")?;
                } else {
                    self.write("\n<p>")?;
                }
            }
            Tag::Heading {
                level, id, classes, ..
            } => {
                if !self.selected_writer.is_full_text() {
                    bail!("Got Heading not at top-level")
                }
                self.selected_writer = SelectedWriter::HeadingText(
                    level,
                    id.map(|x| x.to_string()),
                    classes.iter().map(|x| x.to_string()).collect::<Vec<_>>(),
                );
            }
            Tag::Table(alignments) => {
                self.table_alignments = alignments;
                self.write("<table>")?;
            }
            Tag::TableHead => {
                self.table_state = TableState::Head;
                self.table_cell_index = 0;
                self.write("<thead><tr>")?;
            }
            Tag::TableRow => {
                self.table_cell_index = 0;
                self.write("<tr>")?;
            }
            Tag::TableCell => {
                match self.table_state {
                    TableState::Head => {
                        self.write("<th")?;
                    }
                    TableState::Body => {
                        self.write("<td")?;
                    }
                }
                match self.table_alignments.get(self.table_cell_index) {
                    Some(&Alignment::Left) => {
                        self.write(" style=\"text-align: left\">")?;
                    }
                    Some(&Alignment::Center) => {
                        self.write(" style=\"text-align: center\">")?;
                    }
                    Some(&Alignment::Right) => {
                        self.write(" style=\"text-align: right\">")?;
                    }
                    _ => {
                        self.write(">")?;
                    }
                }
            }
            Tag::BlockQuote(_) => {
                if self.end_newline {
                    self.write("<blockquote>\n")?;
                } else {
                    self.write("\n<blockquote>\n")?;
                }
            }
            Tag::CodeBlock(info) => {
                if !self.selected_writer.is_full_text() {
                    bail!("Got CodeBlock not at top-level")
                }
                if !self.end_newline {
                    self.write_newline()?;
                }
                let lang = match info {
                    CodeBlockKind::Fenced(info) => {
                        let lang = info.split(' ').next().unwrap();
                        if lang.is_empty() {
                            None
                        } else {
                            Some(lang.to_string())
                        }
                    }
                    CodeBlockKind::Indented => None,
                };
                self.selected_writer =
                    SelectedWriter::CodeBlock(lang.unwrap_or_else(|| "null".to_string()));
            }
            Tag::List(Some(1)) => {
                if self.end_newline {
                    self.write("<ol>\n")?;
                } else {
                    self.write("\n<ol>\n")?;
                }
            }
            Tag::List(Some(start)) => {
                if self.end_newline {
                    self.write("<ol start=\"")?;
                } else {
                    self.write("\n<ol start=\"")?;
                }
                write!(self.writer(), "{}", start)?;
                self.write("\">\n")?;
            }
            Tag::List(None) => {
                if self.end_newline {
                    self.write("<ul>\n")?;
                } else {
                    self.write("\n<ul>\n")?;
                }
            }
            Tag::Item => {
                if self.end_newline {
                    self.write("<li>")?;
                } else {
                    self.write("\n<li>")?;
                }
            }
            Tag::Emphasis => {
                self.write("<em>")?;
            }
            Tag::Strong => {
                self.write("<strong>")?;
            }
            Tag::Strikethrough => {
                self.write("<del>")?;
            }
            Tag::Link {
                link_type: LinkType::Email,
                dest_url,
                title,
                ..
            } => {
                self.write("<a href=\"mailto:")?;
                escape_href(self.writer(), &dest_url)?;
                if !title.is_empty() {
                    self.write("\" title=\"")?;
                    escape_html(self.writer(), &title)?;
                }
                self.write("\">")?;
            }
            Tag::Link {
                dest_url, title, ..
            } => {
                self.write("<a href=\"")?;
                escape_href(self.writer(), &dest_url)?;
                if !title.is_empty() {
                    self.write("\" title=\"")?;
                    escape_html(self.writer(), &title)?;
                }
                self.write("\">")?;
            }
            Tag::Image {
                dest_url, title, ..
            } => {
                self.write("<img src=\"")?;
                escape_href(self.writer(), &dest_url)?;
                self.write("\" alt=\"")?;
                self.raw_text()?;
                if !title.is_empty() {
                    self.write("\" title=\"")?;
                    escape_html(self.writer(), &title)?;
                }
                self.write("\" />")?;
            }
            Tag::FootnoteDefinition(name) => {
                if self.end_newline {
                    self.write("<div class=\"footnote-definition\" id=\"")?;
                } else {
                    self.write("\n<div class=\"footnote-definition\" id=\"")?;
                }
                escape_html(self.writer(), &name)?;
                self.write("\"><sup class=\"footnote-definition-label\">")?;
                let len = self.numbers.len() + 1;
                let number = *self.numbers.entry(name.into_string()).or_insert(len);
                write!(self.writer(), "{}", number)?;
                self.write("</sup>")?;
            }
            Tag::HtmlBlock => {}
            Tag::MetadataBlock(_) => {
                self.write("<meta>")?;
            }
            Tag::DefinitionList => todo!(),
            Tag::DefinitionListTitle => todo!(),
            Tag::DefinitionListDefinition => todo!(),
        }
        Ok(())
    }

    fn end_tag(&mut self, tag: TagEnd) -> OrError<()> {
        match tag {
            TagEnd::Paragraph => {
                self.write("</p>\n")?;
            }
            TagEnd::Heading(_) => {
                let mut writer = SelectedWriter::FullText;
                mem::swap(&mut writer, &mut self.selected_writer);
                match &writer {
                    SelectedWriter::HeadingText(level, _id, classes) => {
                        let mut heading_text = String::new();
                        mem::swap(&mut heading_text, &mut self.heading_text);
                        let heading_id = {
                            use voca_rs::*;
                            manipulate::slugify(&strip::strip_tags(&heading_text))
                        };
                        if heading_id == "table-of-contents" {
                            self.toc_position = Some(self.writer().len());
                        } else {
                            self.write_heading(
                                *level,
                                &heading_id,
                                classes,
                                &heading_text,
                            )?;
                            self.headings.push((heading_id, *level, heading_text));
                        }
                    }
                    _ => {
                        bail!("Got Heading end tag without start tag");
                    }
                }
            }
            TagEnd::Table => {
                self.write("</tbody></table>\n")?;
            }
            TagEnd::TableHead => {
                self.write("</tr></thead><tbody>\n")?;
                self.table_state = TableState::Body;
            }
            TagEnd::TableRow => {
                self.write("</tr>\n")?;
            }
            TagEnd::TableCell => {
                match self.table_state {
                    TableState::Head => {
                        self.write("</th>")?;
                    }
                    TableState::Body => {
                        self.write("</td>")?;
                    }
                }
                self.table_cell_index += 1;
            }
            TagEnd::BlockQuote(_) => {
                self.write("</blockquote>\n")?;
            }
            TagEnd::CodeBlock => {
                let mut writer = SelectedWriter::FullText;
                mem::swap(&mut writer, &mut self.selected_writer);
                match &writer {
                    SelectedWriter::CodeBlock(lang) => {
                        let mut codeblock_text = String::new();
                        mem::swap(&mut codeblock_text, &mut self.codeblock_text);
                        self.write_codeblock(lang, &codeblock_text)?;
                    }
                    _ => {
                        bail!("Got CodeBlock end tag without start tag");
                    }
                }
            }
            TagEnd::List(true) => {
                self.write("</ol>\n")?;
            }
            TagEnd::List(false) => {
                self.write("</ul>\n")?;
            }
            TagEnd::Item => {
                self.write("</li>\n")?;
            }
            TagEnd::Emphasis => {
                self.write("</em>")?;
            }
            TagEnd::Strong => {
                self.write("</strong>")?;
            }
            TagEnd::Strikethrough => {
                self.write("</del>")?;
            }
            TagEnd::Link => {
                self.write("</a>")?;
            }
            TagEnd::Image => (), // shouldn't happen, handled in start
            TagEnd::FootnoteDefinition => {
                self.write("</div>\n")?;
            }
            TagEnd::HtmlBlock => {}
            TagEnd::MetadataBlock(_) => {
                self.write("</meta>")?;
            }
            TagEnd::DefinitionList => todo!(),
            TagEnd::DefinitionListTitle => todo!(),
            TagEnd::DefinitionListDefinition => todo!(),
        }
        Ok(())
    }

    fn write_heading(
        &mut self,
        level: HeadingLevel,
        id: &str,
        classes: &[String],
        text: &str,
    ) -> OrError<()> {
        if self.end_newline {
            self.end_newline = false;
            self.write("<")?;
        } else {
            self.write("\n<")?;
        }
        write!(self.writer(), "{}", level)?;
        self.write(" id=\"")?;
        escape_html(self.writer(), id)?;
        self.write("\"")?;
        let mut classes = classes.iter();
        if let Some(class) = classes.next() {
            self.write(" class=\"")?;
            escape_html(self.writer(), class)?;
            for class in classes {
                self.write(" ")?;
                escape_html(self.writer(), class)?;
            }
            self.write("\"")?;
        }
        self.write(">")?;
        self.write(&format!("<a href=\"#{}\">", id))?;
        self.write(text)?;
        self.write("</a>")?;
        self.write("</")?;
        write!(self.writer(), "{}", level)?;
        self.write(">\n")?;
        Ok(())
    }

    fn write_codeblock(&mut self, lang: &str, text: &str) -> OrError<()> {
        self.write(r###"<pre class="language-"###)?;
        escape_html(self.writer(), lang)?;
        self.write(r###""><code class="language-"###)?;
        escape_html(self.writer(), lang)?;
        self.write(r###"">"###)?;
        self.write(&highlighter::highlight(text, lang)?)?;
        self.write("</code></pre>\n")?;
        Ok(())
    }

    // run raw text, consuming end tag
    fn raw_text(&mut self) -> OrError<()> {
        let mut nest = 0;
        while let Some(event) = self.iter.next() {
            match event {
                Start(_) => nest += 1,
                End(_) => {
                    if nest == 0 {
                        break;
                    }
                    nest -= 1;
                }
                Html(text) | Code(text) | Text(text) => {
                    escape_html_body_text(self.writer(), &text)?;
                    self.end_newline = text.ends_with('\n');
                }
                InlineHtml(text) => {
                    self.write(&text)?;
                    self.end_newline = text.ends_with('\n');
                }
                SoftBreak | HardBreak | Rule => {
                    self.write(" ")?;
                }
                FootnoteReference(name) => {
                    let len = self.numbers.len() + 1;
                    let number = *self.numbers.entry(name.into_string()).or_insert(len);
                    write!(self.writer(), "[{}]", number)?;
                }
                TaskListMarker(true) => self.write("[x]")?,
                TaskListMarker(false) => self.write("[ ]")?,
                InlineMath(text) => self.write(&text)?,
                DisplayMath(text) => self.write(&text)?,
            }
        }
        Ok(())
    }
}

fn gen_html<'a, I>(iter: I, pass: Pass) -> OrError<String>
where
    I: Iterator<Item = Event<'a>>,
{
    HtmlWriter::new(iter, pass).run()
}

#[derive(Debug)]
enum TocStyle {
    Floating,
    Fixed,
}

fn write_toc(
    text: &mut String,
    style: TocStyle,
    headings: &[(String, HeadingLevel, String)],
) -> OrError<()> {
    match style {
        TocStyle::Floating => {
            text.write_str(r###"<div id="floating-toc" style="display: none;">"###)?
        }
        TocStyle::Fixed => text.write_str(r###"<div id="fixed-toc">"###)?,
    }
    text.write_str("<div>")?;
    match style {
        TocStyle::Floating => {
            text.write_str(
                r###"<h3>Contents <a href="#" class="underline text-sm">↑ top ↑</a></h3>"###,
            )?;
        }
        TocStyle::Fixed => text.write_str("<h3>Table of contents</h3>")?,
    }
    text.write_str("<ul>")?;
    for (id, level, heading) in headings {
        let class = match level {
            HeadingLevel::H1 | HeadingLevel::H2 | HeadingLevel::H3 => "",
            HeadingLevel::H4 => r###" class="ml-4""###,
            HeadingLevel::H5 => r###" class="ml-8""###,
            HeadingLevel::H6 => r###" class="ml-12""###,
        };
        text.write_str(&format!(
            r###"<li{class}><a href="#{id}">{heading}</a></li>"###
        ))?;
    }
    text.write_str("</ul>")?;
    text.write_str("</div>")?;
    text.write_str("</div>")?;
    Ok(())
}
