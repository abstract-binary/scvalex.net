use crate::{
    config::{InputDir, INPUT_DIRS},
    run_mode::RunMode,
    spawn_thread_or_err, OrError, Shutdown,
};
use camino::{Utf8Path, Utf8PathBuf};
use ignore::Walk;
use log::{debug, error, info};
use seahash::SeaHasher;
use std::{
    collections::HashMap, env, fs::File, hash::Hasher, io::Read, sync::mpsc,
    time::Duration,
};

/// FileTracker watches directories for changes, and sends
/// notifications via a `mpsc::Sender`.  Changes are only sent if the
/// the file contents checksum has changed.
pub struct FileTracker {}

#[derive(Debug)]
pub enum Update {
    FileUpdate { path: Utf8PathBuf, kind: Kind },
    NoMorePendingUpdates,
}

#[derive(Clone, Copy, Debug)]
pub enum Kind {
    Add,
    Change,
    Remove,
}

impl FileTracker {
    pub fn new(
        updates_tx: mpsc::Sender<Update>,
        run_mode: RunMode,
        shutdown_tx: mpsc::Sender<Shutdown>,
    ) -> OrError<Self> {
        let mut sums = HashMap::new();
        let dirs: Vec<String> = INPUT_DIRS
            .iter()
            .filter_map(|(s, kind)| match kind {
                InputDir::Static | InputDir::Render | InputDir::Templates => {
                    Some(s.to_string())
                }
                InputDir::External { .. } => None,
            })
            .collect();
        match run_mode {
            RunMode::OneShot => {
                rescan(&updates_tx, &dirs, &mut sums)?;
                Ok(Self {})
            }
            RunMode::Continuous => {
                rescan(&updates_tx, &dirs, &mut sums)?;
                spawn_thread_or_err("file watcher", shutdown_tx, move || {
                    run_watcher(updates_tx, dirs, sums)
                })?;
                Ok(Self {})
            }
        }
    }
}

fn rescan(
    updates_tx: &mpsc::Sender<Update>,
    dirs: &[String],
    sums: &mut HashMap<Utf8PathBuf, u64>,
) -> OrError<()> {
    for dir in dirs {
        for file in Walk::new(dir) {
            let file = file?;
            if !file.metadata()?.is_file() {
                continue;
            }
            let path: Utf8PathBuf = file.into_path().to_str().unwrap().into();
            if !is_temp_path(&path) {
                let hash = hash_file(&path)?;
                sums.insert(path.clone(), hash);
                send_update(updates_tx, path.clone(), Kind::Add)?;
            }
        }
    }
    info!("Rescan complete");
    updates_tx.send(Update::NoMorePendingUpdates)?;
    Ok(())
}

fn run_watcher(
    updates_tx: mpsc::Sender<Update>,
    dirs: Vec<String>,
    mut sums: HashMap<Utf8PathBuf, u64>,
) -> OrError<()> {
    use notify::RecursiveMode;
    let (tx, rx) = mpsc::channel();
    let mut debouncer =
        notify_debouncer_mini::new_debouncer(Duration::from_millis(250), move |res| {
            tx.send(res).unwrap()
        })?;
    for dir in &dirs {
        debouncer.watcher().watch(
            &dir.parse::<std::path::PathBuf>()?,
            RecursiveMode::Recursive,
        )?;
    }
    loop {
        let mut acts = vec![];
        match rx.recv()? {
            Err(errs) => error!("Watcher errors: {errs:?}"),
            Ok(events) if events.is_empty() => {}
            Ok(events) => {
                for ev in events {
                    if ev.path.try_exists()? {
                        acts.push((ev.path.to_str().unwrap().into(), Kind::Change))
                    } else {
                        acts.push((ev.path.to_str().unwrap().into(), Kind::Remove))
                    }
                }
            }
        }
        if !acts.is_empty() {
            let mut update_sent = false;
            for (path, kind) in acts.into_iter() {
                if is_temp_path(&path) {
                    continue;
                }
                match kind {
                    Kind::Remove => {
                        sums.remove(&path);
                        update_sent = true;
                        send_update(&updates_tx, path, kind)?;
                    }
                    Kind::Add | Kind::Change => {
                        if !path.is_file() {
                            continue;
                        }
                        let hash = hash_file(&path)?;
                        use std::collections::hash_map::Entry;
                        match sums.entry(path.clone()) {
                            Entry::Vacant(entry) => {
                                entry.insert(hash);
                                update_sent = true;
                                send_update(&updates_tx, path, kind)?;
                            }
                            Entry::Occupied(mut entry) => {
                                if hash != *entry.get() {
                                    entry.insert(hash);
                                    update_sent = true;
                                    send_update(&updates_tx, path, kind)?;
                                }
                            }
                        }
                    }
                }
            }
            if update_sent {
                updates_tx.send(Update::NoMorePendingUpdates)?;
            }
        }
    }
}

fn hash_file<P: AsRef<Utf8Path>>(path: P) -> OrError<u64> {
    let mut file = File::open(path.as_ref())?;
    let mut buf = [0; 4096];
    let mut hasher = SeaHasher::new();
    loop {
        match file.read(&mut buf)? {
            0 => return Ok(hasher.finish()),
            _ => hasher.write(&buf),
        }
    }
}

fn send_update(
    updates_tx: &mpsc::Sender<Update>,
    path: Utf8PathBuf,
    kind: Kind,
) -> OrError<()> {
    let cwd = env::current_dir()?;
    let path = if path.starts_with(&cwd) {
        path.strip_prefix(cwd)?.to_path_buf()
    } else {
        path
    };
    debug!("File updated ({kind:?}) '{path}'");
    Ok(updates_tx.send(Update::FileUpdate { path, kind })?)
}

fn is_temp_path<P: AsRef<Utf8Path>>(path: P) -> bool {
    let path = path.as_ref().as_str();
    path.ends_with('~') || path.ends_with(".bck")
}
