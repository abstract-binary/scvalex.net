# scvalex.net

## Architecture

See [ARCHITECTURE.md](./ARCHITECTURE.md).

## References:

- Liquid: https://shopify.github.io/liquid/

## License

All code here is licensed under the EUPL-1.2-or-later.  See the
[LICENSE](./LICENSE) file for details.
