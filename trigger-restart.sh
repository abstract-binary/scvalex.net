#!/bin/sh

set -e -x

curl "https://kubernetes.default.svc/apis/apps/v1/namespaces/default/deployments/scvalex-net" \
     --insecure \
     --silent \
     --show-error \
     -H "Authorization: Bearer $GITLAB_DEPLOYER_TOKEN" \
     -X PATCH \
     -H "Accept: application/json" \
     -H "Content-Type: application/strategic-merge-patch+json" \
     --data "@-" <<EOF
{
  "spec": {
    "template": {
      "metadata": {
        "annotations": {
          "scvalex.net/restartedAt": "$(date +%Y-%m-%d_%T%Z)"
        }
      }
    }
  }
}
EOF
