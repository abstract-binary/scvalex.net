#!/bin/sh

set -e -x

curl --output ~/tmp/cover.jpg "$1"
convert ~/tmp/cover.jpg -scale 300x "static/r/$2"
identify "static/r/$2"
