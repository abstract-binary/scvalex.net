---
title = "Mickey7"
author = "Edward Ashton"
link = "https://us.macmillan.com/books/9781250275035/mickey7"
cover_image = "/r/ashton-mickey7.jpg"
cover_image_height = 456
cover_image_width = 300
read = "2023-01-19"
categories = [ "novella", "sci-fi" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This book is set in a hard-ish sci-fi universe, it has characters with
conflicting motivations, and it is written well.  That said, the
entire book feels flat.

The protagonist narrator manages to deliver eating breakfast and
walking around with a doomsday device in exactly the same tone.  I
think this is on purpose and is meant to show how detached he feels
due to his semi-immortality, but it just makes everything seem
uninteresting.

The fact that the plot is mostly mundane day-to-day activities
compounds the flatness of the narration.  And this is another novella
disguised as a full-length novel which means there just isn't enough
text for much of anything that happens to develop.

I'd only recommend this to somebody who's already read ~all the other
sci-fi books out there and is looking for more.
