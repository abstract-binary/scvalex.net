---
title = "The Grid"
author = "Gretchen Bakke"
link = "https://www.bloomsbury.com/ca/grid-9781632865687/"
cover_image = "/r/bakke-the-grid.jpg"
cover_image_height = 456
cover_image_width = 300
read = "2022-03-28"
categories = [ "electricity", "real-world" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This would've made for a pretty good magazine article.  Instead, the
content was stretched to book length by the repetition of the few core
ideas and offtopic anecdotes.

I recommend "Living on the Grid" by William L. Thompson instead.
