---
title = "Network Effect"
author = "Martha Wells"
link = "https://www.marthawells.com/networkeffect.htm"
cover_image = "/r/wells-network-effect.jpg"
cover_image_height = 461
cover_image_width = 300
read = "2021-08-30"
categories = [ "sci-fi", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

Oh, wow: It's a full length novel in the Murderbot universe!  And it's
actually pretty good now that characters have space to develop and the
plot is long enough to be more than just "go to place and fight some
robot".

As far as sci-fi goes, this is pretty light reading, so I can
recommend it to anyone with even a passing interest.
