---
title = "Bad Blood"
author = "John Carreyrou"
link = "https://www.penguinrandomhouse.com/books/549478/bad-blood-by-john-carreyrou/"
cover_image = "/r/carreyrou-bad-blood.jpg"
cover_image_height = 462
cover_image_width = 300
read = "2021-05-28"
categories = [ "real-world", "business", "management" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This was a very entertaining read.  I think what happened at Theranos
was that they tried to "fake it til they made it", but they never did
make it, so they had to keep doubling-down.  Reading about all the
lies they told and how each one led to the next was very interesting.

I recommend this to anyone who has an interest in business,
management, or just wants to read an interesting story about something
that happened in real life.
