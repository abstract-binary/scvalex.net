---
title = "There Is No Antimemetics Division"
author = "qntm"
link = "https://qntm.org/scp"
cover_image = "/r/qntm-anti-mementics.jpg"
cover_image_height = 480
cover_image_width = 300
read = "2021-10-16"
categories = [ "sci-fi", "novel", "scp" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a collection of [SCP][scp-wiki] stories focused on entities
which can manipulate people's memories of them.

[scp-wiki]: https://scp-wiki.wikidot.com/

The book essentially asks the question "How do you fight something
that you can't remember exists?".  The answer isn't very satisfying in
my opinion, but everything up to the answer is a fun read.

I'd recommend this to anyone who's ever wanted to get into SCP, but
was put off by the sheer volume of literature surrounding the
foundation.
