---
title = "Soon I Will Be Invincible"
author = "Austin Grossman"
link = "https://www.austingrossman.com/soon-i-will-be-invincible"
cover_image = "/r/grossman-invincible.jpg"
cover_image_height = 458
cover_image_width = 300
read = "2023-07-26"
categories = [ "novel", "super-hero", "deconstruction" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I liked this novel when I first read it six years ago and I liked it
even more on a re-read.  It's a deconstruction of the super-hero genre
which asks the question of what happens to the super-hero team-up when
the heroes drift apart and what goes on through the mind of the
super-villain on his 13th attempt at world domination.  It's basically
The Boys, but with violence substituted by wit.

The story is just a generic "super-villain tries to take over the
world and super-heroes try to stop him" plot, but what the book is
actually about is an exploration of how people take on identities.
Half the chapters follow an up-and-coming hero with amnesia who's just
been invited to the big super-hero team.  Her story is that she
doesn't really know who she is, but is trying to become a successful
hero.  The other half of the chapters follow a super-villain who's on
his 13th attempt to take over the world.  His story is mostly
reflecting on how he got to where he is, all the people he's met,
butted heads with, and eventually drifted apart from.  The contrast
between the two viewpoints makes for interesting reading.

I recommend this to anyone who's into fiction, especially if they're
familiar with the tropes of the super-hero genre.
