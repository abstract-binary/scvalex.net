---
title = "The Perfect Run"
author = "Maxime J. Durand"
link = "https://www.royalroad.com/fiction/36735/the-perfect-run"
cover_image = "/r/durand-perfect-run_thumb.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-07-11"
categories = [ "sci-fi", "cyberpunk", "webnovel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I had fun reading this.  It's a time-loop story with super-hero
elements set in a cyberpunk post-apocalyptic world.

The prose is well-written, the plot gripped me, and many scenes were
funny, interesting, or just plain delightful to read.  Given that this
was originally published on Royal Road, I would've expected it to be
the usual LitRPG slog, but it's actually not: when we join our
protagonist, he's already had his time-loop powers for a very long
time and has already mastered them.  The story revolves around him
piecing together what's going on, facing his past, and planning for
the future.

The one thing that prevents me from just recommending it is that
there's a lot of side-questing towards the end.  I suspect the author
had a list of plot hooks he left in the story, realized he was
reaching the end, so he rushed to tie up **all** the loose ends.  It
adds some amount of world-building, but the main plot grinds to a halt
whilst this is going on.
