---
title = "Columbus Day"
author = "Craig Alanson"
link = "https://www.craigalanson.com/books"
cover_image = "/r/alanson-columbus-day.jpg"
cover_image_height = 400
cover_image_width = 300
read = "2022-02-17"
categories = [ "sci-fi", "novel" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This book tries to be a space opera seen from the point of view of a
human grunt in a setting where humanity has not actually developed
space travel and gets shanghaied into the galatic war.  To say good
things first, the setting seems interesting and it has a few fresh
takes on the usual galaxy-scale conflict trope.  But that's not enough
to carry the boring plot or the unwitty writing.

For instance, one of main character's traits is that he is a redneck
who repeatedly mentions his superiorirty to dem city folk.  And then
the super advanced AI with godlike powers chooses to present itself as
a Bud Light can.  I know the author was really pleased with himself
for this specific joke because he drew attention to it in every single
chapter in the second half of the book.  I think the writing tries to
be witty, but it just isn't.

The writing might get better in later books in the series, but there's
just better stuff to read.
