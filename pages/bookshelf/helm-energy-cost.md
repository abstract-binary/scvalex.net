---
title = "Cost of Energy Review"
author = "Dieter Helm"
link = "https://www.gov.uk/government/publications/cost-of-energy-independent-review"
cover_image = "/r/helm-energy-cost.jpg"
cover_image_height = 385
cover_image_width = 300
read = "2023-02-18"
categories = [ "real-world", "climate", "electricity" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This was a very good overview of the electricity system of the UK from
an organizational and regulatory point of view.  The focus of the
report is on how to achieve the climate change and energy security
goals while minimizing costs to consumers.

The first half introduces us to the mess that privatising the
electricity system caused and then the report goes over the patchwork
of regulations that tried to fix specific issues when they appeared.
The second half makes suggestions on how to radically simplify the
regulatory framework around generation, networks, supply, and taxation
of electricity.

As mentioned in the author's later book---Net Zero: How We Stop
Causing Climate Change---these recommendations were mostly ignored by
the UK government (they were probably too busy "getting Brexit done").

The text is very readable, but it's also fairly specialized.  I'd only
recommend this if you like reading about how regulations affect
industry participants.
