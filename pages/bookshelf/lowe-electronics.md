---
title = "Electronics for Dummies"
author = "Doug Lowe"
link = "https://www.dummies.com/book/technology/electronics/general-electronics/electronics-all-in-one-for-dummies-281694/"
cover_image = "/r/lowe-electronics.jpg"
cover_image_height = 376
cover_image_width = 300
read = "2023-03-31"
categories = [ "electronics", "embedded" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I found this to be an excellent intro to electronics, starting with
basic physics and building up to projects like Christmas lights and
animatronic props.  It covered a lot of subjects briefly with an
emphasis on practical knowledge and actually making things.

If you're thinking of getting into electronics, I recommend this over
a university textbook or random online resources.  Also, given the
price tag, it's great value for money.
