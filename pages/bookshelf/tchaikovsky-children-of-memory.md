---
title = "Children of Memory"
author = "Adrian Tchaikovsky"
link = "https://www.panmacmillan.com/authors/adrian-tchaikovsky/children-of-memory/9781529087215"
cover_image = "/r/tchaikovsky-children-memory.jpg"
cover_image_height = 461
cover_image_width = 300
read = "2023-01-23"
categories = [ "novel", "sci-fi" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is the third book in the series and I found it a much more
interesting read than the first two.  The others could be summarised
as "sci-fi things happen and then more sci-fi things happen".  This
book is the same, but it also has a mystery threaded through.  That
and new aliens aren't just more human-aliens; instead, they are
interesting starfish-aliens with a novel take on sentience.

I'd recommend it even if you haven't read the other books in the
series---there's a good summary of them at the start.
