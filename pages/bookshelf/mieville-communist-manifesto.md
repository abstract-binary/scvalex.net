---
title = "A Spectre, Haunting"
author = "China Miéville"
link = "https://www.haymarketbooks.org/books/1990-a-spectre-haunting"
cover_image = "/r/mieville-communist-manifesto.jpg"
cover_image_height = 461
cover_image_width = 300
read = "2023-02-05"
categories = [ "politics", "real-world" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I occasionally run into online people who call themselves
"communists", but I never understood how they rationalise this to
themselves.  As in, I find it easy to imagine how somebody might have
thought communism was worth a shot in 1848, but with almost two
hundred years of hindsight, it's a very different story now.

After reading this book, I finally get it.  The core trick is that
everything good gets the label "communism" and everything bad gets the
label "capitalism".  If something bad was done in a communist context,
then clearly it wasn't actually "communism".  And if something good
was done in a capitalist context, then it was clearly just to further
entrench "capitalism" and is thus actually bad in the long run.  Also,
anything currently bad because of "capitalism" will nebulously become
good under "communism".  Once you accept this framing, then
"communism" is clearly good and "capitalism" is clearly bad.

This book is a great read if you've ever wondered what happens when a
bunch of people adopt a particular label into their collective
identity and then engage in a ferocious hundred year long circle-jerk.
