---
title = "Coup d'État — A Practical Handbook"
author = "Edward N. Luttwak"
link = "https://www.hup.harvard.edu/catalog.php?isbn=9780674737266"
cover_image = "/r/luttwak-coup-handbook.jpg"
cover_image_height = 453
cover_image_width = 300
read = "2021-10-07"
categories = [ "politics", "real-world", "gift-book" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a good analysis on how coup d'états happen, what preconditions
are necessary, and how different countries have made themselves
coup-proof.

This is a good book to read if you've ever wondered why certain
countries (e.g. many African ones) seem to have coups every year and
why other countries (e.g. European ones) almost never get coup'd.

I think this is also a good book to gift.  Imagine: you sidle up
conspiratorially to the victim, you give them the book wrapped in
nondescript brown paper and whisper "This is one of my favourites.
I've taken the liberty of highlighting the interesting bits.  Also, my
GPG key is at the back... for when the time is right.".  Even if they
don't like the book, they get a story about how their crazy friend
gave them a how-to-coup-the-state book.
