---
title = "Maus"
author = "Art Spiegelman"
link = "https://www.penguinrandomhouse.com/books/171065/the-complete-maus-by-art-spiegelman/"
cover_image = "/r/spiegelman-maus.jpg"
cover_image_height = 413
cover_image_width = 300
read = "2021-08-09"
categories = [ "graphic-novel", "ww2" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a graphic novel telling the experiences of a Holocaust
survivor.  It's an interesting read and gives a glimpse into what
happened.  To say the obvious, it's not a fun read because most of the
characters end up begin murdered.
