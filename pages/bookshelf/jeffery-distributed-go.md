---
title = "Distributed Services with Go"
author = "Travis Jeffery"
link = "https://pragprog.com/titles/tjgo/distributed-services-with-go/"
cover_image = "/r/jeffery-distributed-go.jpg"
cover_image_height = 360
cover_image_width = 300
read = "2021-11-01"
categories = [ "programming", "go", "distributed-systems" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a pretty good intro to writing distributed programs in Go.
It's not amazing, but it does a good job of illustrating how easy it
is to combine different libraries into a fully-featured client and
server application.
