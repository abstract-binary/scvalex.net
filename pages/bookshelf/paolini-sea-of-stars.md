---
title = "To Sleep in a Sea of Stars"
author = "Christopher Paolini"
link = "https://www.paolini.net/works/to-sleep-in-a-sea-of-stars/"
cover_image = "/r/paolini-sea-of-stars-cover_thumb.jpg"
cover_image_height = 456
cover_image_width = 300
read = "2022-01-17"
categories = [ "sci-fi", "space-opera", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I have mixed feelings about this novel.  It is certainly a good
science fiction novel with lots of features.  It's got space travel,
hard sci-fi technology, ancient alien artifacts, several alien races
that actually feel alien, memorable characters that develop over the
course of the story, an evolving universe, and it's all tied together
in a single story that never gets dull.

My main issue with the book is that none of the elements felt great.
I finished reading it and I wasn't thinking about how good the book
had been---I was instead thinking about I'd read next.  This is unfair
because I think the book ticked off all the items on my "good sci-fi
checklist", but it felt like it did stopped at that and did nothing
more.

All said, I'd still recommend it.  It's a solid space opera, and
there's been a dearth in this genre in recent years.
