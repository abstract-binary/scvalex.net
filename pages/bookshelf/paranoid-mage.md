---
title = "Paranoid Mage #1—3"
author = "Inadvisably Compelled"
link = "https://www.royalroad.com/fiction/49879/paranoid-mage"
cover_image = "/r/paranoid-mage.jpg"
cover_image_height = 480
cover_image_width = 300
read = "2023-07-13"
categories = [ "webnovel", "litrpg", "fantasy" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a pretty good fantasy novel, despite its LitRPG web-novel
roots.  It has a likeable main character who acts with consistency,
decent world-building with a setting that feels lived-in, and good
storytelling with an overarching plot that doesn't just stand still
when the protagonist isn't driving it.

To pick one thing in particular, I really like how bureaucratic
incompetence plays a key role in the events of the series.  This feels
very realistic for an organisation that's supposed to be several
centuries old and gives the whole setting a groundedness that many
fantasy novels lack.

Another nice aspect is that the magic system has hard rules and the
protagonist doesn't progress by increasing his numbers, but by
figuring out new and better ways to exploit the rules.

I recommend this to anyone who's into fantasy or sci-fi novels.  The
problem with the series is that it gets less interesting as it goes
on, but that doesn't preclude the first few books from being good.
