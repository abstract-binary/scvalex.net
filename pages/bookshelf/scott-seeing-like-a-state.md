---
title = "Seeing Like a State"
author = "James C. Scott"
link = "https://yalebooks.yale.edu/book/9780300078152/seeing-like-a-state/"
cover_image = "/r/scott-seeing-like-a-state.jpg"
cover_image_height = 451
cover_image_width = 300
read = "2022-01-20"
categories = [ "systematics", "real-world" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is an interesting history book looking back at the failures of
scientific forestry, collectivization, artificial new country
capitals, and more.

The book doesn't really reach any compelling conclusions, but it does
have a few interesting ideas to take away.  The first is the concept
of legibility: it's well known that maps and models don't precisely
describe the reality they represent.  I've always thought of this as
just a warning to not blindly trust predictions made by models.  The
book has a different take: when states found that they didn't have the
bureaucratic capacity to model reality accurately, they made reality
more "legible" by, for instance, reducing the number of tree species
in a forest, or by relocating villages to farm grid-layout fields, or
by imposing arbitrary family names on a population that had none
before.

The second idea is that of the "modernist aesthetic".  The book points
out that many of the "scientific" things done in the 19th and 20th
centuries were really arbitrary decisions dressed up as science.  For
instance, cities were built with grid layouts and architects had some
sciencey-sounding theory about why this is better.  However, the
architects failed at the scientific process and didn't do the crucial
step of testing whether the new layout was actually better in
practice.  They just wanted something that looked nice on a map and
made some vague untested predictions about how this would work out in
practice.

I recommend this book to anyone with an interest in large-scale
systems.
