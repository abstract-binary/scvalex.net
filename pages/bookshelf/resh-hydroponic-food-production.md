---
title = "Hydroponic Food Production"
author = "Howard M. Resh"
link = "https://www.routledge.com/Hydroponic-Food-Production-A-Definitive-Guidebook-for-the-Advanced-Home/Resh/p/book/9780367678753"
cover_image = "/r/resh-hydroponic-food-production.jpg"
cover_image_height = 456
cover_image_width = 300
read = "2023-04-18"
categories = [ "hydroponics", "real-world", "textbook" ]
opinion = "must-read"
type = "BookshelfBook"
---

I might be premature in saying this, but I think this book got me into
hydroponics.  This is an excellent overview of how plants grow, how
different hydroponics setups work, and how various companies built up
their farms.

I found the many stories of how farms were incrementally built to be
particularly compelling.  It appears that getting plants to grow isn't
all that hard and the tech required is fairly simple; the difficulty
lies in increasing yields and scaling up operations.

I recommend this to anyone with a passing interest in growing
plants---traditionally or with hydroponics---and prefers an in-depth
introduction over context-less instructions.
