---
title = "How to Read a Financial Report"
author = "John A. Tracy, Tage C. Tracy"
link = "https://www.oreilly.com/library/view/how-to-read/9781119606468/"
cover_image = "/r/tracy-read-financial-report.jpg"
cover_image_height = 205
cover_image_width = 300
read = "2021-05-28"
reread = ["2023-07-04"]
categories = [ "real-world", "accounting", "investing" ]
opinion = "must-read"
type = "BookshelfBook"
---

I've read a bunch of books over the years on dissecting financial
statements and this was the best one by far.  The structure makes
sense, the prose is clear, and the extra-wide page layout makes for
good presentation.

The only real problem is that books like this tend to become
out-of-date fairly quickly as accounting standards evolve.  However,
Tracy & Tracy are pretty good at updating their books and the core
ideas don't ever really change.

I recommend this to anyone with an interest in accounting, especially
from an investment point of view.
