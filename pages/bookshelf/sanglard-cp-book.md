---
title = "The Book of CP-System"
author = "Fabien Sanglard"
link = "https://fabiensanglard.net/cpsb/"
cover_image = "/r/sanglard-cp-book.jpg"
cover_image_height = 370
cover_image_width = 300
read = "2023-03-04"
categories = [ "programming", "retro-computing" ]
opinion = "must-read"
type = "BookshelfBook"
---

I very much enjoyed reading this book.  The deep-dive into the
hardware of the CP-System in the first half of the book is the
clearest explanation of how a computer is put together that I've read
so far.  I finally understand exactly how memory busses and
memory-mapped IO work.  The overview of how one writes programs for
the CP-System in the second half was less interesting to me, but the
book more than made up for it with stories and interviews with the
programmers, artists, and musicians who wrote CP-System games.

I'd recommend this to anyone with an interest in computing.
