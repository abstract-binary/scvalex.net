---
title = "Weather For Dummies"
author = "John D. Cox"
link = "https://www.dummies.com/book/academics-the-arts/science/environmental-science/weather-for-dummies-282709/"
cover_image = "/r/cox-weather.jpg"
cover_image_height = 376
cover_image_width = 300
read = "2023-09-02"
categories = [ "weather", "real-world" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

I did not enjoy reading this book.  The signal-to-fluff ratio is
extremely low—each sentence of information is surrounded by multiple
paragraphs of drivel.  It's the usual house style of For Dummies
books, but turned up to eleven.

While I did learn quite a bit about how air moves and how weather
happens, I cannot, in good conscience, recommend this to anyone.  I
read this book because Bill Gates recom-mended it, but either there's
literally no other introduction to meteorology, or he was having a
bout of temporary insanity.
