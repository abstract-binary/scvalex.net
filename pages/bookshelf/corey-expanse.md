---
title = "The Expanse #1–3"
author = "James S.A. Corey"
link = "https://www.jamessacorey.com/writing-type/books/"
cover_image = "/r/corey-expanse.jpg"
cover_image_height = 359
cover_image_width = 310
read = "2023-06-24"
categories = [ "sci-fi", "hard-sci-fi", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I have mixed feelings about the Expanse books.  On the one hand,
they're shinning examples of hard sci-fi.  On the other, I find them
really annoying to read.

The setting is vast, feels lived-in, and has multiple conflicting
factions that are all justified in their goals.  The way the setting
evolves over the course of the books is very interesting and is the
primary draw in my opinion.

The problem is that the plots seem to be driven forward by idiots.
Each book presents a situation that keeps escalating because every
character is an idiot.  Every intrigue feels like it would just
unravel if even a single character stopped acting like an idiot.  I
assume the books are written like this to build tension, but I just
find them very annoying to read.

I recommend at least the first book to anyone who's into
sci-fi---maybe you won't have as much of a visceral reaction to the
writing as I did.
