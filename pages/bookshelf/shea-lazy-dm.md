---
title = "Return of the Lazy Dungeon Master"
author = "Michael E. Shea"
link = "https://shop.slyflourish.com/products/return-of-the-lazy-dungeon-master"
cover_image = "/r/shea-lazy-dm.jpg"
cover_image_height = 389
cover_image_width = 300
read = "2021-03-24"
categories = [ "ttrpg", "dnd" ]
opinion = "must-read"
type = "BookshelfBook"
---

I found this to be chock-full of good tips for preparing a D&D
session.  It emphasises that there are a few things that need to be
decided in advance and that the rest can be improvised.

I read this two years ago and, while I like to prepare more than the
author recommends, I still use the basic structure outlined in the
book for all my notes.

I think this is a good read for any aspiring or current DMs,
especially if they feel like they're spending *too long* preparing.
