---
title = "Why We Sleep"
author = "Matthew Walker"
link = "https://www.simonandschuster.com/books/Why-We-Sleep/Matthew-Walker/9781501144325"
cover_image = "/r/walker-why-we-sleep.jpg"
cover_image_height = 457
cover_image_width = 300
read = "2021-08-28"
categories = [ "self-help", "model" ]
opinion = "must-read"
type = "BookshelfBook"
---

The book gave me a model of sleep.  Before reading this, I had no idea
what sleep was for, why I'd be sometimes sleepless and other times
sleepy, what damage poor sleep would cause me, and why things like
coffee didn't seem to work most of the time.

I would recommend the first third of this book to everyone.

Mind you, this recommendation isn't to say the book is without fault.
It has the common problem that the content is frontloaded, many of the
stories and stats seem anecdotal, and it generally feels padded out.
Still, the first three chapters will provide anyone with a good
starting point for a personal model of sleep.
