---
title = "Six Sacred Swords 1-3"
author = "Andrew Rowe"
link = "https://andrewkrowe.wordpress.com/series-info/"
cover_image = "/r/rowe-sacred-swords.jpg"
cover_image_height = 460
cover_image_width = 320
read = "2021-11-29"
categories = [ "fantasy", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

These are fun sword-and-sorcery books with a flexible magic system.
The first book is meh, but the second and third ones narrate an
interesting fighting tournament with lots weird fighter/sorcerer
multi-classers.

This is very much a "read it for the fights, not the plot" kind of
series.
