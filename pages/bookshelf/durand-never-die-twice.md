---
title = "Never Die Twice"
author = "Maxime J. Durand"
link = "https://www.royalroad.com/fiction/32067/never-die-twice"
cover_image = "/r/durand-never-die-twice.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-08-02"
categories = [ "webnovel", "litrpg", "fantasy" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This was an interesting fantasy book.  It's got a Norse-inspired
setting and it's told from the perspective of a necromancer with
neutral-evil alignment.  This is the first book I've encountered where
the evil protagonist is played straight---they aren't some
misunderstood hero, they aren't an anti-hero, they're just evil, but
not comically so.

The aspect that prevents me from just recommending it is that it's a
sad book---it's a story where the necromancer wins.  I recall being
miserable while reading this and it wasn't a page-turner despite the
interesting story.
