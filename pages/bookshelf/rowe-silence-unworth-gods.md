---
title = "The Silence of Unworthy Gods"
author = "Andrew Rowe"
link = "https://andrewkrowe.wordpress.com/series-info/"
cover_image = "/r/rowe-silence-unworthy-gods.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-12-29"
categories = [ "fantasy", "magic-school", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is the fourth book in Andrew Rowe's "Arcane Ascension" series.
Unlike the third book, we actually have an overarching plot this time,
so it feels less like a sequence of random actions.

The series continues to have well-narrated fight scenes, characters
that make the most of their limited magical arsenal, and pretty good
overall writing.

I look forward to the next book in the series.
