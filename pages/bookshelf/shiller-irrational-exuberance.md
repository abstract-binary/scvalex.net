---
title = "Irrational Exuberance"
author = "Robert J. Shiller"
link = "https://press.princeton.edu/books/paperback/9780691173122/irrational-exuberance"
cover_image = "/r/shiller-irrational-exuberance.jpg"
cover_image_height = 453
cover_image_width = 300
read = "2021-05-01"
categories = [ "real-world", "investing" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I am writing this two years after having finished the book and I can
only remember a single takeaway from it.  That said, it was a very
important takeaway.

Shiller points out that housing prices have only gone up historically.
There have been periods when prices didn't go up as fast or have even
stagnated, but they have never gone down (with the exception of cities
that are dying).  So, "waiting for the bubble to pop" doesn't work.
That said, waiting for mortgages to be cheaper still sounds plausible,
but Shiller didn't investigate this.

I think the book is a good theoretical investing read.  In addition to
housing prices, it also goes over the [CAPE][cape] which is always a
good thing to know about.

[cape]: https://www.investopedia.com/terms/c/cape-ratio.asp
