---
title = "Biological Pest Control"
author = "Julian Ives"
link = "https://www.dragonfli.co.uk/collections/ladybirds/products/a-gardeners-guide-to-biological-pest-control-using-natural-predators-in-the-garden-book"
cover_image = "/r/ives-pest-control.jpg"
cover_image_height = 396
cover_image_width = 300
read = "2023-07-13"
categories = [ "gardening", "real-world" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

As a person who had done no gardening and knew next to nothing about
insects, I found this book to be very informative.

The text provides an overview of different settings, what harmful
insects are likely to appear and when, and which parasites or
predators are recommended for reducing the pest populations.  The book
is also full of photos of both insects and the signs that they might
be around.

I recommend this to anyone who's new to growing plants or is having
trouble dealing with unwanted bugs.
