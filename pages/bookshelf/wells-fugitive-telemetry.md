---
title = "Fugitive Telemetry"
author = "Martha Wells"
link = "https://www.marthawells.com/murderbot6.htm"
cover_image = "/r/wells-fugitive-tellemetry.jpg"
cover_image_height = 465
cover_image_width = 300
read = "2022-06-23"
categories = [ "sci-fi", "novella" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This is another novella set in the murderbot universe sold for the
price of a full-length novel.  Like the other books in the series,
it's solid sci-fi, but it's not good value-for-money.
