---
title = "Goodbye, Eri"
author = "Tatsuki Fujimoto"
link = "https://www.viz.com/goodbye-eri"
cover_image = "/r/fujimoto-goodbye-eri.jpg"
cover_image_height = 429
cover_image_width = 300
read = "2023-08-13"
categories = [ "manga" ]
opinion = "must-read"
type = "BookshelfBook"
---

This is the best manga I've read so far.  It's a 200 page oneshot by
the author of Chainsaw Man.  I won't say much about it for fear of
spoiling it, but I think it showcases the author's excellent character
work, visual story-telling, and off-kilter sense of humour which is
both funny and thought-provoking.  This work is also much tighter
narratively than the author's other more serial work.
