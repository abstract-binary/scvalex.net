---
title = "The Gulag Archipelago"
author = "Aleksandr Solzhenitsyn"
link = "https://en.wikipedia.org/wiki/The_Gulag_Archipelago"
cover_image = "/r/solzhenitsyn-gulag-archipelago.jpg"
cover_image_height = 460
cover_image_width = 300
read = "2022-08-22"
categories = [ "real-world", "history" ]
opinion = "must-read"
type = "BookshelfBook"
---

This was an unpleasant book.  What shocked me the most was how little
padding it has: every paragraph describes some cruelty inflicted by
the soviets on their own people, and every chapter describes some
systematic way in which the cruelties were doled out.

The book is essentially a hit-piece on communism.  It argues that
communism cannot function without death camps, and since death camps
are clearly evil, communism is also evil.  This argument is more
concise and harder to argue against than any argument I've found
before.

The big problem with the book is that it is un-sourced.  As in, the
sources are the author's own experiences in the gulag and the
testimonies that he personally collected.  We'll never know the true
scale of what happened (whether larger or smaller) because the soviets
killed most of the witnesses and their records were either fabricated
or destroyed.
