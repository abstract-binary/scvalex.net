---
title = "Dichronauts"
author = "Greg Egan"
link = "https://www.gregegan.net/DICHRONAUTS/DICHRONAUTS.html"
cover_image = "/r/egan-dichronauts.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-04-27"
categories = [ "sci-fi", "alien-physics" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is another Greg Egan book with alien physics.  The gimmick this
time is that the universe has two dimensions of space and two
dimensions of time.  The result is a very weird world.  The author's
[website][1] has more details on how the physics work out.

[1]: https://www.gregegan.net/DICHRONAUTS/DICHRONAUTS.html
