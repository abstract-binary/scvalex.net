---
title = "A Clockwork Orange"
author = "Anthony Burgess"
link = "https://www.anthonyburgess.org/a-clockwork-orange/"
cover_image = "/r/burgess-clockwork-orange.jpg"
cover_image_height = 489
cover_image_width = 300
read = "2021-10-04"
categories = [ "novel", "fiction" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This novel sets itself apart with its setting and main character.  The
book takes place in a version of post-war Britain which took a turn
towards extreme violence, and the story is told from the perspective
of a character who's almost a sociopath.  It's an interesting read
because everything is played straight: the violent society isn't some
cautionary tale; it's just how things could have turned out.
Similarly, the main character isn't some dark anti-hero who is really
good underneath the hard edges; instead, it's just a very misguided
youth who enjoys dishing out a bit of the "ultra-violence" and feels
no remorse for it.

This is one of those books that's on many reading lists and it
deserves its place, but there's nothing really unique about it to make
me want to recommend it to anyone.
