---
title = "Build a Weather Station with Elixir and Nerves"
author = "Alexander Koutmos, Bruce A. Tate, Frank Hunleth"
link = "https://pragprog.com/titles/passweather/build-a-weather-station-with-elixir-and-nerves/"
cover_image = "/r/koutmos-weather-station-elixir.jpg"
cover_image_height = 360
cover_image_width = 300
read = "2023-01-12"
categories = [ "programming", "elixir", "embedded" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This book is a solid tutorial in adding some I2C sensors to a
RaspberryPi, getting data out of them with [Nerves][nerves], setting
up [TimescaleDB][timescaledb] with a [Phoenix][phoenix] web frontend,
and visualizing sensor data in [Grafana][grafana].  The end result
isn't much code, but the difficulty in this sort of project is
figuring out what components to use and how they fit together.  The
authors do a good job of explaining this.

[nerves]: https://nerves-project.org/
[timescaledb]: https://docs.timescale.com/
[phoenix]: https://www.phoenixframework.org/
[grafana]: https://grafana.com/oss/grafana/

I usually complain that books like this could've just been online
tutorials, but "ebook sold online" is close enough, and this one
actually felt worth the price tag.

I recommend this book to anyone who's interested in the above topics,
but knows nothing about them.
