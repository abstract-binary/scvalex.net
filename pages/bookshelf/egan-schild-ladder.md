---
title = "Schild's Ladder"
author = "Greg Egan"
link = "https://www.gregegan.net/SCHILD/SCHILD.html"
cover_image = "/r/egan-schild-ladder.jpg"
cover_image_height = 462
cover_image_width = 300
read = "2021-09-12"
categories = [ "sci-fi", "alien-physics" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This is another of Greg Egan's books with weird physics.  This one is
about the "quantum graphs that underlie all the constituents of
matter".

I have no idea if this is plausible physics or not.  As I couldn't
follow the explanations, it all seemed like magic to me.
Unfortunately, without the weird physics aspect, the book is just a
boring fantasy novel with uninteresting characters.
