---
title = "L'apprenti épouvanteur"
author = "Joseph Delaney"
link = "https://josephdelaneyauthor.com/spooksworld/books-uk/the-wardstone-chronicles/"
cover_image = "/r/delaney-apprentice.jpg"
cover_image_height = 350
cover_image_width = 300
read = "2021-08-05"
categories = [ "novel", "fantasy", "young-adult" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

I read these novels in French to brush up on my French, and I think
they're fine for this purpose.  Otherwise, they're pretty simple
young-adult books about a kid who's training to be a wizard.

The books are fine for what they are, and hey, the protagonist isn't a
slaver!  In that regard, these are better stories than other popular
young-adult titles.  Still, I wouldn't recommend them to anyone
outside the target demographic.
