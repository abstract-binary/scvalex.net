---
title = "Security Analysis"
author = "Benjamin Graham & David L. Dodd"
link = "https://www.mhprofessional.com/securityanalysis7"
cover_image = "/r/graham-security-analysis.jpg"
cover_image_height = 449
cover_image_width = 300
read = "2023-08-25"
categories = [ "investing", "accounting", "real-world" ]
opinion = "must-read"
type = "BookshelfBook"
---

I found this to be a very insightful book.  It's strange to say so,
but out of the tens of investing books I've read, this 1934 text is
the best by far.

The authors have the unique approach of reasoning about investments
from the ground up.  Every chapter introduces some aspect of investing
that needs consideration, followed by many real-world examples.  These
are, of course, horribly outdated, but because they're always used in
support of some abstract bit of reasoning, they still feel relevant.

It's also shocking how little the world of investing has changed in
ninety years.  If you strip the references to years and hide the names
of companies, the text could easily pass for something written in the
last ten years.  For instance, they talk about the housing and
mortgages bubble, two different sector bubbles, a general mania that
distorted all prices, and countless scams.

That said, the seventh edition is certainly an odd beast.  You have to
read through over a hundred pages of introductions and forwards to get
to the first word written by the authors.  Overall, I think a third of
the book is now written by other people.  All these additions do serve
to re-ground the book into our times, but it's not something I've seen
done elsewhere.

I strongly recommend this to anyone who has investments.  It clocks in
at over 800 dense pages, but it's absolutely worth the time
investment.
