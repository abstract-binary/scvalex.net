---
title = "The Goal"
author = "Eliyahu M. Goldratt"
link = "https://northriverpress.com/the-goal-a-business-graphic-novel-2/"
cover_image = "/r/goldratt-the-goal.jpg"
cover_image_height = 434
cover_image_width = 300
read = "2021-07-17"
categories = [ "graphic-novel", "business", "management", "systematics" ]
opinion = "must-read"
type = "BookshelfBook"
---

I enjoyed reading this business-themed graphic novel.  While it
doesn't have a complicated message, it doesn't come off as preachy and
it also doesn't over-explain.

A snide review would be to say that the book could've just been a
paragraph: "When trying to improve an existing system, start by
defining the metric that is the goal, then understand how the system
affects said metric, and finally fix the bottlenecks that prevent the
goal from being reached.  It is very important to keep the goal in
mind at all times and not be distracted by other metrics."

That said, the book does a good job of illustrating the above in an
engaging way.  It also happens to be the only book I've found that
warns against doing haphazard "improvements" or "optimisations", a
problem which is endemic in software development.

I'd recommend this to basically everyone, especially because it's not
long.
