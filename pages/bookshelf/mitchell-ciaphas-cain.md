---
title = "Ciaphas Cain — Books 1-10"
author = "Sandy Mitchell"
link = "https://www.blacklibrary.com/authors/sandy-mitchell"
cover_image = "/r/mitchell-ciaphas-cain.jpg"
cover_image_height = 460
cover_image_width = 300
read = "2021-04-12"
categories = [ "novel", "sci-fi" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

The Ciaphas Cain books are very fun reads.  My only real complaint is
that there aren't more of them.

Mind you, these aren't high literature.  The characters are simple and
unchanging.  The setting is Warhammer 40k, so while it has novelty, it
feels samey after a while.  And the books have essentially just one
joke: Ciaphas Cain is a coward, but has to act heroically because of
his station.  That said, the writing carries the series and makes all
of Ciaphas Cain's escapades entertaining to read.  It's also pretty
good hard-ish sci-fi.

If you're on the fence, I'd say watch [this short][short] to decide if
you like the character.

[short]: https://youtu.be/oRozBAIbaG4
