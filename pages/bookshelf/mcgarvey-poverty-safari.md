---
title = "Poverty Safari"
author = "Darren McGarvey"
link = "https://www.panmacmillan.com/authors/darren-mcgarvey/poverty-safari/9781529006346"
cover_image = "/r/mcgarvey-poverty-safari.jpg"
cover_image_height = 462
cover_image_width = 300
read = "2023-06-02"
categories = [ "real-world" ]
opinion = "must-read"
type = "BookshelfBook"
---

I found this book to be very interesting.  The author writes about his
experience growing up poor, going through a very rough patch in his
twenties when he became homeless and an addict, and argues that the
root cause of many of society's troubles is poverty and that the
entire political spectrum isn't willing to face the enormity of the
problem.

What I particularly like here is that the author feels believable.  He
went through a bunch of difficulties giving him an unusual perspective
on the world, but he also overcame his challenges and was sufficiently
introspective to be able to write a book about it all.

It's also very interesting to me that, during the low point of the
author's life, he was receiving more state help than the median salary
in the country I was growing up in at the time.  Poverty in a
developed country isn't at all what I imagined it to be.

I think I recommend this to basically everyone because it's important
to know what's going on in the lower strata of society so that help
isn't misdirected at the wrong problems.
