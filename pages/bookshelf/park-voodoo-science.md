---
title = "Voodoo Science"
author = "Robert Park"
link = "https://global.oup.com/academic/product/voodoo-science-9780195147100"
cover_image = "/r/park-voodoo-science.jpg"
cover_image_height = 482
cover_image_width = 300
read = "2023-06-09"
categories = [ "real-world", "pop-science" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a collection of stories about bad science.  It ranges from
health scares based on poorly-understood physics to impossible
devices like infinite energy machines.

It's not a bad read, but each of the many stories is treated only
superficially.  This makes it very approachable, but the only thing to
learn from the shallow discussion is that there's lots of people who
peddle fake science and that there's no shortage of PhDs who are
willing to back fantastical claims.

I think I would only recommend this to a somebody who's on the verge
of trying homeopathy or is thinking of investing in a car that runs on
water.
