---
title = "The Scholomance Trilogy"
author = "Naomi Novik"
link = "https://www.naominovik.com/category/scholomance/"
cover_image = "/r/novic-scholomance.jpg"
cover_image_height = 465
cover_image_width = 320
read = "2022-11-09"
categories = [ "fantasy", "novel", "magic-school" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

The snide summary for the Scholomance books is "Mary Sue goes to dark
edgy magic school".  It's ok fantasy and it's fun to read, but there's
nothing memorable and there are no new ideas to take away.
