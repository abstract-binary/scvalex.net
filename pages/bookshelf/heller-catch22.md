---
title = "Catch-22"
author = "Joseph Heller"
link = "https://en.wikipedia.org/wiki/Catch-22"
cover_image = "/r/heller-catch22.jpg"
cover_image_height = 462
cover_image_width = 300
read = "2023-06-30"
categories = [ "novel" ]
opinion = "must-read"
type = "BookshelfBook"
---

This was a very good novel.  I first read it in high-school and I
wasn't sufficiently patient and world-weary to appreciate it back
then.

The prose and story structure are unlike anything I've seen anywhere
else.  At the small scale, the text is very convoluted often changing
location, time frame, and focused character in the middle of a
paragraph.  This comes off as witty and I suspect the goal is to
prevent the reader from guessing the punchlines to jokes.

Zooming out, the story is set up like a jigsaw puzzle.  At the
beginning, we're dropped into the middle of a complicated situation
with many characters.  As the story goes on, we find out what exactly
the deal is with each character and what events led to the oft-times
absurd situations described.

I recommend this to everyone---it's just a very good novel.
