---
title = "Something & Anything"
author = "Dakota Krout"
link = "https://www.mountaindalepress.com/dakota-krout"
cover_image = "/r/krout-full-murderhobo.jpg"
cover_image_height = 232
cover_image_width = 320
read = "2022-05-08"
categories = [ "litrpg", "novel" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

The joke is that the protagonist's class in-universe is literally
["murderhobo"][wiktionary-murderhobo] and that he solves every problem
by being extremely violent.

[wiktionary-murderhobo]: https://en.wiktionary.org/wiki/murderhobo

This is an entertaining read for about half the first book.  However,
there are three books in the series and there's nothing else to carry
the narrative along.
