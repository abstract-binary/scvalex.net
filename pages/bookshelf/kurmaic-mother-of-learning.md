---
title = "Mother of Learning"
author = "Domagoj Kurmaić"
link = "https://www.royalroad.com/fiction/21220/mother-of-learning/"
cover_image = "/r/kurmaic-mother-of-learning.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-03-23"
categories = [ "webnovel", "litrpg", "fantasy" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is another take on the "fantasy character stuck in a time-loop"
trope.  It's well written, even if the middle part feels like a slog,
and the mystery of the setting is good enough.  The big problems with
it are that it doesn't do anything to set itself apart from the crowd
and that it's very long.
