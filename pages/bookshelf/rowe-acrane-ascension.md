---
title = "Arcane Ascension 1–3"
author = "Andrew Rowe"
link = "https://andrewkrowe.wordpress.com/series-info/"
cover_image = "/r/rowe-arcane-ascension.jpg"
cover_image_height = 320
cover_image_width = 320
read = "2021-12-16"
categories = [ "fantasy", "magic-school", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

These are sword-and-sorcery stories with a hint of progression novel.
The many dungeon puzzles are fun to read about, the fight scenes are
well-narrated, and the cast of characters is likeable.

I'd recommend this to anyone into fantasy, especially if they're
looking for a non-Tolkien and somewhat lighthearted setting.
