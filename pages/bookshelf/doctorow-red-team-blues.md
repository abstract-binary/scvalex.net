---
title = "Red Team Blues"
author = "Cory Doctorow"
link = "https://craphound.com/category/redteamblues/"
cover_image = "/r/doctorow-red-team-blues.jpg"
cover_image_height = 447
cover_image_width = 300
read = "2023-05-24"
categories = [ "novel" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

The idea behind this book is great: a forensic accountant untangles
financial crime in Silicon Valley.  The problem is that the main
character doesn't actually do forensic accounting.  Instead, he often
repeats that he's a forensic accountant, but does all the actual
forensic accounting off-page.  So, the book ends up following a 60
year old man as he drives around in a bus, has dinner with friends,
sleeps with every female character, and avoids the mob without putting
much effort into it.

This is basically a Tom Clancy or John Grisham novel, but without the
clever plot and stripped down to two hundred pages.
