---
title = "The Atlas Six"
author = "Olivie Blake"
link = "https://www.olivieblake.com/the-atlas-six"
cover_image = "/r/blake-atlas-six.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2023-03-19"
categories = [ "novel", "fantasy" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This was a pretty good modern character-focused fantasy read.

The basic plot is that six magicians have to live together in the same
house for a year and are forced to interact with one another.  The
characters really shine: each feels like a real person, each has their
own thing going on, and they butt heads in entertaining ways.

The part where the novel is weakest is the skeletal plot: it does
little more than setup interactions between characters and string the
reader along with hints of a mystery.  It all feels flat.  There's no
waves, peaks, or really any texture to it.

However, in a genre dominated by stories like "I Got Hit by a Truck
and Discovered I Am a Wizard with a Cheat-level Skill --- Say
Whaaat?", this book stands a head above most of the competition.
