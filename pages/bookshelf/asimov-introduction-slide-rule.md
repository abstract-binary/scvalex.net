---
title = "An Easy Introduction to the Slide Rule"
author = "Isaac Asimov"
link = "https://archive.org/details/easyintroduction00isaa/"
cover_image = "/r/asimov-introduction-slide-rule.jpg"
cover_image_height = 441
cover_image_width = 250
read = "2022-10-24"
categories = [ "math", "slide-rule" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This book focuses on the theory behind how slide-rules work.  It has
the best introduction to logarithms that I've found and then a very
slow-paced explanation of how a slide-rule uses them.

I think the biggest reason to read this is to be able to out-nerd
almost anybody.  "Oh, you've read all of Asimov's books?  But have you
read the slide-rule one?"
