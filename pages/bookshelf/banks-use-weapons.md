---
title = "Use of Weapons"
author = "Iain M. Banks"
link = "https://www.iain-banks.net/titles/iain-m-banks-3/use-of-weapons/9781857231359/"
cover_image = "/r/banks-use-weapons.jpg"
cover_image_height = 475
cover_image_width = 300
read = "2022-10-24"
categories = [ "sci-fi" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is the third book in the Culture series.  It feels a lot more
like the "Consider Phlebas" than "The Player of Games" in that it has
a fairly weak overarching plot and is mostly characters reacting to
events beyond their control.

It uses a gimmick where half the chapters are in chronological order,
and half are in reverse order, and the two kinds of chapters are
interleaved.  I think the point of this was to flesh out the main
character by providing bits of his backstory.  It succeeds in this,
but it was annoying to read, especially given that some of the
flashbacks are essentially fever dreams.

All said, the book provides more details of how the Culture operates,
so it's worth a read if you're into the setting.
