---
title = "Project Hail Mary"
author = "Andy Weir"
link = "https://www.penguinrandomhouse.com/books/611060/project-hail-mary-by-andy-weir/"
cover_image = "/r/weir-hail-mary.jpg"
cover_image_height = 462
cover_image_width = 300
read = "2021-12-29"
categories = [ "novel", "sci-fi", "hard-sci-fi" ]
opinion = "must-read"
type = "BookshelfBook"
---

This is a hard sci-fi book about people solving a massive, but
fundamentally basic, problem with limited resources.  It's refreshing
to see physics experiments replace techno-babble as the *sci* in
sci-fi.  The plot is also interesting---it is essentially one big
research project---and all the plot twists are eminently plausible in
context.

If you like hard sci-fi, you'll like this.
