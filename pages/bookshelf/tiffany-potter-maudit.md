---
title = "Harry Potter et l'Enfant Maudit"
author = "John Tiffany, Jack Thorne"
link = "https://www.gallimard-jeunesse.fr/9782075074209/harry-potter-et-l-enfant-maudit.html"
cover_image = "/r/tiffany-potter-maudit.jpg"
cover_image_height = 436
cover_image_width = 300
read = "2021-03-02"
categories = [ "fantasy", "stage-play" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

I wasn't much a fun of stage plays and reading this did not change my
mind.  The plot is a pretty dull time-travel one that you've
definitely seen before, the characters each have exactly one trait
that defines them, and the protagonist's development is boring and
reeks of heavy-handed moralism.

I think this would only appeal to someone who's are really into Harry
Potter and wants to see the characters again after a few decades.
Like with most of Rowling's stuff, Pratchett did it better and he
wrote a lot, so there's always more to read.
