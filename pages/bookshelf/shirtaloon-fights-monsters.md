---
title = "He Who Fights With Monsters"
author = "Shirtaloon"
link = "https://www.royalroad.com/fiction/26294/he-who-fights-with-monsters"
cover_image = "/r/shirtaloon-fights-monsters.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-01-28"
categories = [ "webnovel", "litrpg", "fantasy" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

I think this might be the arch-typical progression novel.  It is
insanely long and just follows a single character as he grinds
experience at a snail's pace.  There's probably a plot somewhere in
there, but it's impossible to see from how thin it's stretched.

There are certainly funny scenes, but I found the protagonist to be
extremely annoying.  Every one of his lines tries to be a quip, his
contribution to conversations is usually some non sequitur, and he
just never ever shuts up.  The book acknowledges this and implies he's
doing it to distract and annoy his adversaries.  But **I** have to
read this crap too and it annoys me just as much.

I listened to the first audio-book, but I won't be bothering with the
other eight (and counting).
