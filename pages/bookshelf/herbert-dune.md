---
title = "Dune"
author = "Frank Herbert"
link = "https://dunenovels.com/dune/"
cover_image = "/r/herbert-dune.jpg"
cover_image_height = 537
cover_image_width = 300
read = "2021-09-29"
categories = [ "novel", "sci-fi" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is one of those classic sci-fi novels that makes it onto all the
reading lists.  I'm sure it was amazing when it came out, it still
holds up for the most part and it certainly has a bit of everything
you'd expect from a sci-fi story, but there are now many novels that
have done any of the individual elements much better.

I think I'd recommend it only to people who enjoy getting into the
atmosphere of a book and to anyone who's already gone through all the
other big sci-fi novels.
