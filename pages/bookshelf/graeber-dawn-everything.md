---
title = "Dawn of Everything"
author = "David Graeber, David Wengrow"
link = "https://us.macmillan.com/books/9780374157357/thedawnofeverything"
cover_image = "/r/graeber-dawn-everything.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-10-19"
categories = [ "history", "real-world" ]
opinion = "must-read"
type = "BookshelfBook"
---

This is a book about how humans have historically organized themselves
into societies.  It's structured as an argument against the simple
story that, as numbers of individuals increased and technology
advanced, humans naturally organized into hierarchies.

The book presents many examples of human societies covering the period
from 10,000 years ago to a few centuries ago, and focuses on whether
they were hierarchies and what these were based upon.  The sheer
variety of modes of organization surprised me.  In particular, I
didn't know that there were very many variations on egalitarianism,
that some of them were fairly recent, and that they worked on a scale
of tens of thousands of people.

I recommend this book to anyone who has any interest in organizing
people.
