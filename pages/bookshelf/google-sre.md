---
title = "Site Reliability Engineering"
author = "Google"
link = "https://sre.google/workbook/table-of-contents/"
cover_image = "/r/google-sre.jpg"
cover_image_height = 393
cover_image_width = 300
read = "2021-04-10"
categories = [ "ops", "sre", "programming", "distributed-systems" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I have mixed feelings about this book.  One the one hand, it's the
single best collection of essays on engineering large-scale
distributed systems that I've ever found.  I think it's basically
mandatory reading for anyone working in the field.

On the other hand, I've had a growing feeling for a while that large
distributed systems are either a mistake, or a symptom of a problem.
As such, spending a massive amount of effort to make them rock-solid
might not be the right solution.

For instance, this is the book that teaches you how to engineer a
telemetry ingestion pipeline of many geographically-distributed
ingresses, message queues, map-reduce processors, and gigantic
databases.  Notably, it also describes how to monitor these systems
and what the human support organisation might look like.  What the
book doesn't do is suggest that maybe you shouldn't be generating a 1
KB event for every click a user makes in your app which includes their
GPS coordinates, what other apps they have open, and a slew of IDs to
track them across different networks.

More succinctly, this book describes how to operationalize
Google-scale solutions.  But maybe the right thing to do is to
engineer your systems and organisation to not have Google-scale
problems in the first place.
