---
title = "Black Hat Rust"
author = "Sylvain Kerkour"
link = "https://kerkour.com/black-hat-rust"
cover_image = "/r/kerkour-black-hat-rust_thumb.jpg"
cover_image_height = 399
cover_image_width = 300
read = "2022-03-03"
categories = [ "programming", "rust", "infosec" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This book by [Sylvain Kerkour][kerkour] was a broad overview of a few
security topics, descriptions of some tools to explore said topics,
and then Rust code to implement the tools.  This is the book for you
if you know nothing about infosec and want to follow a tutorial
implementing a web crawler in Rust, among other things.

[kerkour]: https://kerkour.com/
