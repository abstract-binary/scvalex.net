---
title = "Accidence will Happen"
author = "Oliver Kamm"
link = "https://www.simonandschuster.com/books/Accidence-Will-Happen/Oliver-Kamm/9781681774237"
cover_image = "/r/kamm-accidence.jpg"
cover_image_height = 449
cover_image_width = 300
read = "2022-11-25"
categories = [ "writing" ]
opinion = "must-read"
type = "BookshelfBook"
---

This is the first writing style guide I've read that actually had
justifications for the suggestions.  For instance, every other guide
emphasizes the critical importance of using "who" and "whom"
correctly.  This one instead uses the Oxford English Dictionary and
Merriam-Webster to show examples of "misuses" throughout history,
including by famous authors.  It then points out that neither
dictionary actually advises using "whom", and since confusion is
unlikely to occur, writers shouldn't worry about it and instead focus
on more important aspects of the text.

This is my favourite style guide now.
