---
title = "Existentially Challenged"
author = "Yahtzee Croshaw"
link = "https://www.penguinrandomhouse.com/books/717922/existentially-challenged-by-written-by-yahtzee-croshaw/"
cover_image = "/r/yahtzee-existentially-challenged.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2021-12-26"
categories = [ "novel", "fantasy", "witty-writing" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is the sequel to "Differently Morphous".  It's a fun read and
showcases Yahtzee's dry humour, this time mixing occult societies,
British bureaucracy, and net culture.

This is one of the few books I've read in recent years that's carried
not by its plot, setting or characters, but by its very witty writing.
I'd recommend this to anyone who likes Terry Pratchett or Douglas
Adams.
