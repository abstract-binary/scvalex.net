---
title = "The Peripheral"
author = "William Gibson"
link = "https://williamgibsonbooks.com/"
cover_image = "/r/gibson-peripheral.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2021-08-05"
categories = [ "novel", "sci-fi", "cyberpunk" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a solid sci-fi novel with a future society which is actually
plausible, a time-travel McGuffin which operates by hard rules, and
interesting characters who are forced to adapt to unusual
circumstances.

The future world described feels lived-in and the various actors are
all believable.  Basically, it's a cyberpunk future, but the
mega-corps aren't comically evil and everything isn't just shit for no
reason.  Instead, it's an almost idyllic society which only appears
dark and melancholy to our present-day eyes.

The novel also describes a version of the apocalypse that I hadn't
encountered before: the Jackpot. Spoiler: <span
class="spoiler">Instead of one big thing happening, it was a series of
unfortunate events whose combined effect was to decimate the human
population.  However, throughout all this, technology progressed
enough for the survivors to overcome the problems of the new
world and build fairly comfortable lives for themselves.</span>

I'd recommend this to anyone into sci-fi.
