---
title = "Lords of Uncreation"
author = "Adrian Tchaikovsky"
link = "https://www.panmacmillan.com/authors/adrian-tchaikovsky/lords-of-uncreation/9781529051988"
cover_image = "/r/tchaikovsky-lords-uncreation.jpg"
cover_image_height = 462
cover_image_width = 300
read = "2023-05-01"
categories = [ "sci-fi", "space-opera", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I liked the book overall, but it's much less good than the previous
two in the series.

The first two thirds are solid space-opera, but then the book closes
with magical dream logic.  Also, the explanations it gives for some of
the mysteries are kind of meh and they would've been better left
unexplained.

I think the novel would've been much stronger without the last third.
Still, if you enjoyed the first two books in the series, or
Tchaikovsky's other novels, then this is worth a read.
