---
title = "The 3D Printing Handbook"
author = "Ben Redwood, Filemon Schöffer, Brian Garret"
link = "https://www.hubs.com/3d-printing-handbook/"
cover_image = "/r/redwood-3d-printing-handbook.jpg"
cover_image_height = 424
cover_image_width = 300
read = "2022-07-05"
categories = [ "3d-printing" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This textbook is a good overview of the different 3D printing
technologies, how they work, what strengths they each have, and what
difficulties to expect when using them.

I read this because I thought it would help me choose which 3D printer
to buy.  It did not help in that, but at least now I've got a better
idea of how 3D printing could be used in industry.
