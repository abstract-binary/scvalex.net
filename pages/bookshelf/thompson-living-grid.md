---
title = "Living on the Grid"
author = "William L. Thompson"
link = "https://www.iuniverse.com/en/bookstore/bookdetails/720908-Living-on-the-Grid"
cover_image = "/r/thompson-living-grid.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2022-03-17"
categories = [ "electricity", "systematics", "real-world" ]
opinion = "must-read"
type = "BookshelfBook"
---

The book is an overview of how the electric grid of the US functions
from the point of view of an experienced operator.  It focuses on how
hard it is to balance supply and demand of power on a moment-by-moment
basis, how different kinds of electricity generation and loads affect
grid stability, and how hard it is to recover from failures.

I found this to be a very interesting read both because there are
parallels to distributed systems, but also because it unambiguously
answers questions like "Why can't we replace everything with solar
power?".
