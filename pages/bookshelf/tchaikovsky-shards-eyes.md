---
title = "Shards of Earth & Eyes of the Void"
author = "Adrian Tchaikovsky"
link = "https://www.adriantchaikovsky.com/books.html"
cover_image = "/r/tchaikovsky-shards-eyes.jpg"
cover_image_height = 237
cover_image_width = 320
read = "2022-05-04"
categories = [ "sci-fi", "space-opera", "novel" ]
opinion = "must-read"
type = "BookshelfBook"
---

The "Final Architecture" novels are the best space opera I've read in
years.  They have everything from a large and complicated galactic
setting, interesting characters that change during the story, and a
plot that is both small enough for the characters to matter and large
enough to impact the whole setting.

One interesting concept I hadn't encountered before is the idea of a
technological ceiling: there are multiple races in the setting and all
are at around the same level of technology.  The explanation is that
this is basically the highest tech level attainable with their
understanding of the laws of physics.  Then there's the one race
that's more advanced than anybody else and whose technology nobody can
reverse engineer because they just don't understand the principles by
which it functions.  Of course, there's also the obligatory precursor
race whose understanding of the universe was so deep, that all the
artifacts they left behind look like magic to the current races.

I look forward to reading the third book when it comes out in Q2 2023.
