---
title = "Heaven's River"
author = "Dennis E. Taylor"
link = "http://dennisetaylor.org/"
cover_image = "/r/taylor-havens-river.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2021-07-24"
categories = [ "novel", "sci-fi" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I liked this book, but less so than the trilogy that preceded it.
Half the book felt like a solid sci-fi plot where the characters are
exploring a strange alien megastructure, but the other half felt like
the setup for another book.  It makes for good reading, but without
the "Bob" angle of the previous books, it feels a bit generic, almost
like second tier sci-fi from the 1980's.

I suggest waiting until the follow-up book or books come out before
reading this.
