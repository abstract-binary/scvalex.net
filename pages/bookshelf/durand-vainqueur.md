---
title = "Vainqueur the Dragon"
author = "Maxime J. Durand"
link = "https://www.royalroad.com/fiction/26534/vainqueur-the-dragon"
cover_image = "/r/durand-vainqueur.jpg"
cover_image_height = 497
cover_image_width = 300
read = "2022-07-29"
categories = [ "webnovel", "litrpg", "fantasy" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

The joke is that an [ancient red dragon][dndbeyond-dragon] discovers
the class and leveling system.  It then decides to be an adventurer
because completing quests is a more efficient way of increasing its
hoard than attacking merchant caravans.

[dndbeyond-dragon]: https://www.dndbeyond.com/monsters/16782-ancient-red-dragon

This is funny for about two chapters.  However, there are many
chapters in the book, and there are more books in the series.
Everything else is just a bog standard progression novel with the
twist that it's the monsters who are leveling up and gaining skills.
