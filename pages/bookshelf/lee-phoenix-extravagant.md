---
title = "Phoenix Extravagant"
author = "Yoon Ha Lee"
link = "https://www.simonandschuster.com/books/Phoenix-Extravagant/Yoon-Ha-Lee/9781781089194"
cover_image = "/r/lee-phoenix-extravagant.jpg"
cover_image_height = 463
cover_image_width = 300
read = "2021-05-11"
categories = [ "novel", "sci-fi" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This certainly wasn't The Hexarchate Trilogy or even just Ninefox
Gambit.  I'm not sure how to describe it other than kind of meh.

The story follows a non-binary painter who's hired by the occupation
government of his African country to animate golems using the magic of
painting.  That's unfortunately the entire book---the ideas are just
introduced and not explored in any depth.  The plot is thin as well
and it's mostly just "stuff happens".

I recommend not reading this until more books come out in the series
with hopefully more developments.
