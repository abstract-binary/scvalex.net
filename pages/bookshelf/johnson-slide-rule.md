---
title = "The Slide Rule"
author = "Lee H. Johnson"
link = "https://archive.org/details/sliderule00john"
cover_image = "/r/johnson-slide-rule.jpg"
cover_image_height = 471
cover_image_width = 300
read = "2022-10-24"
categories = [ "math", "slide-rule" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is the best user's guide for a slide-rule that I've found.  It
covers all the operations and has many solved exercises for practice.
It contains very little theory, but in any case, figuring out why
things actually work is half the fun.

The Internet Archive lends out the book, but it's also not hard to
find a PDF of it online.
