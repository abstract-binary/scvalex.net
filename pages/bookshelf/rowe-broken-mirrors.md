---
title = "The War of Broken Mirrors"
author = "Andrew Rowe"
link = "https://andrewkrowe.wordpress.com/series-info/"
cover_image = "/r/rowe-broken-mirrors.jpg"
cover_image_height = 450
cover_image_width = 309
read = "2022-05-26"
categories = [ "fantasy", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

The three books in the series are fun fantasy novels.

The setting is a fairly traditional sword-and-sorcery world with some
twists.  While the magic system isn't as developed as in the author's
later works, it's still versatile enough that the characters can do
interesting things with it.  It's refreshing to see magic make some
amount of in-universe sense, and characters that try to make the most
of their limited abilities.
