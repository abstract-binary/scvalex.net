---
title = "500 Lines or Less"
author = "many authors"
link = "https://www.aosabook.org/en/500L/introduction.html"
cover_image = "/r/dibernardo-500-lines.jpg"
cover_image_height = 390
cover_image_width = 300
read = "2022-07-19"
categories = [ "programming" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This is one of the "Architecture of Open Source Applications"
follow-up books and it was pretty bad.  I think it's been mostly
downhill in terms of quality since the first book.

Out of the 22 chapters, I found only 4 to be interesting.  I have a
better hit rate just by reading the front page of
[lobste.rs](https://lobste.rs).
