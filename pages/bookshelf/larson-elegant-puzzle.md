---
title = "An Elegant Puzzle"
author = "Will Larson"
link = "https://press.stripe.com/an-elegant-puzzle"
cover_image = "/r/larson-elegant-puzzle.jpg"
cover_image_height = 444
cover_image_width = 300
read = "2021-06-15"
categories = [ "management" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

To start with the good bits, I think this book has an excellent
binding.  It's the kind of hardcover that you rarely see these days:
not only is it a pleasure to hold open, but the cover also has a nice
texture to it that feels really pleasant to your fingertips.  The
subtle embossing of the tree and the minimalist aesthetic make it a
great addition to any bookshelf.  I give the cover a ten out of ten.

As for the content, it's been two years since I read it and I can't
remember a single thing from it.  Flipping through, it looks like it's
just the usual management platitudes.  The most interesting part is
the last third which is recommendations for books and papers.  The
papers in particular are definitely the popular and interesting ones.

As far as management schlock goes, this book is probably one of the
best.  If you have to read one, maybe because you're interacting with
people who like reading management books, then this is worth a look.
