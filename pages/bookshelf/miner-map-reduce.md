---
title = "MapReduce Design Patterns"
author = "Donald Miner, Adam Shook"
link = "https://www.oreilly.com/library/view/mapreduce-design-patterns/9781449341954/"
cover_image = "/r/miner-map-reduce.jpg"
cover_image_height = 328
cover_image_width = 250
read = "2022-01-01"
categories = [ "programming", "big-data" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This book was an overview of a few ways to organize map-reduce jobs in
terms of splitting work and merging results.  It was an interesting
read---I learned a few things---but the text is padded out by
uninteresting Java code.  On the whole, the book should've been a wiki
or a series of blog posts.
