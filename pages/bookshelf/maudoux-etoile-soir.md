---
title = "Freaks' Squeele—L'Étoile du soir"
author = "Florent Maudoux, Ophélie Bruneau"
link = "https://www.ankama-shop.com/en/label-619/732-freaks-squeele-volume-1-l-etoile-du-soir-novel.html"
cover_image = "/r/maudoux-etoile-soir.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2021-09-23"
categories = [ "novel", "fantasy" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This is a novel set in the same universe as the [Freaks'
Squeele][wiki-freaks-squeele] comics.  It's not bad, but the plot is
just "wacky stuff happens".  This works fine in the comics because the
art does all the heavy lifting, but it makes for a forgettable novel.

[wiki-freaks-squeele]: https://en.wikipedia.org/wiki/Freaks%27_Squeele
