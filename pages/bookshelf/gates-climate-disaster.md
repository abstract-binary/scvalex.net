---
title = "How to Avoid a Climate Disaster"
author = "Bill Gates"
link = "https://www.penguinrandomhouse.com/books/633968/how-to-avoid-a-climate-disaster-by-bill-gates/"
cover_image = "/r/gates-avoid-climate-disaster.jpg"
cover_image_height = 462
cover_image_width = 300
read = "2022-12-29"
categories = [ "real-world", "climate" ]
opinion = "must-read"
type = "BookshelfBook"
---

This is an excellent overview of the problem of climate change.
Having read it, this is the first time I've felt like I grasp the
whole scope of the situation, what areas need work, how much work is
needed in each area, and roughly what the solutions are expected to
look like.

If you feel lost hearing that planting trees is the solution to
climate change, then hearing that bogs are the real solution, then
hearing that we need more nuclear or more wind or more solar, and have
never heard about steel, cement and fertilizer, then this is the book
that organizes all the noise into information.

This book focuses mostly on technological solutions to climate change:
what tech breakthroughs are needed and what R&D is in the pipeline.
For government policy and national economy oriented solutions, see the
book by Dieter Helm.
