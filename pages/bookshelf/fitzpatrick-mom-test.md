---
title = "The Mom Test"
author = "Rob Fitzpatrick"
link = "https://www.momtestbook.com/"
cover_image = "/r/fitzpatrick-mom-test.jpg"
cover_image_height = 441
cover_image_width = 300
read = "2021-12-04"
categories = [ "business", "communication" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

The central premise of this book is that people are naturally bad at
giving feedback, especially if they're afraid they might offend.  The
book presents strategies and questions to extract useful opinions
even in these adverse circumstances.

I didn't find the text particularly interesting because I've grown to
have very low expectations of people when I solicit feedback.  That
said, this is a good book to recommend to people who might not realize
that everybody lies to them about their work.
