---
title = "Freaks' Squeele"
author = "Florent Maudoux"
link = "https://www.ankama-shop.com/en/label-619/2903-florent-maudoux-freaks-squeele-boxed-set-volumes-1-through-4.html"
cover_image = "/r/maudoux-freaks-squeele.jpg"
cover_image_height = 473
cover_image_width = 300
read = "2021-03-15"
categories = [ "comic", "fantasy" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

I found this French comic to be a fun read.  It's better than most in
that it has small-scale stories that tie into a larger saga, the
characters have things going on and change as the story advances, and
there's even social commentary threaded subtly through the narrative.

I read this mostly because I wanted some French practice, but I think
I would've enjoyed it even translated.  I'd recommend it to anyone
who's into Japanese or Korean fantasy comics, but is disappointed by
the generally inept writing.
