---
title = "99 Variations on a Proof"
author = "Philip Ording"
link = "https://press.princeton.edu/books/hardcover/9780691158839/99-variations-on-a-proof"
cover_image = "/r/ording-99-variations-proof.jpg"
cover_image_height = 322
cover_image_width = 300
read = "2022-09-29"
categories = [ "math" ]
opinion = "must-read"
type = "BookshelfBook"
---

The book consists of 100 proofs for "If $$x^3 - 6x^2 - 6 = 2x - 2$$,
then $$x = 1$$ or $$x=4$$".  The proofs include expected and
unexpected approaches: "substitution", "calculus", "analytic",
"origami", "found", "medieval", and "slide rule".  It is a very fun
read.

This is the best demonstration I've seen that different branches of
mathematics are really just tools and that they can be used to solve
problems that are not strictly within their target domains.
