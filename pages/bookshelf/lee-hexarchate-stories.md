---
title = "Hexarchate Stories"
author = "Yoon Ha Lee"
link = "https://rebellionpublishing.com/product/hexarchate_stories/"
cover_image = "/r/lee-hexarchate-stories.jpg"
cover_image_height = 464
cover_image_width = 300
read = "2022-04-13"
categories = [ "sci-fi", "space-opera", "short-stories" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

I was hoping this would further flesh out the Hexarchate setting.
Instead, it was a bunch of mostly uninteresting short stories.  As I
write this eight months later, I can't remember a single one.

I have a suggestion for the author: if you're going to write an
author's note bragging about how quick and easy it is to write these
stories, don't put it after the one that's just a really long sex
scene.  Instead, put it after one of the stories that's actually like
the main books and that has something actually interesting happen.
