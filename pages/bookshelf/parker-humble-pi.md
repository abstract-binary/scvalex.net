---
title = "Humble Pi"
author = "Matt Parker"
link = "https://www.penguinrandomhouse.com/books/610964/humble-pi-by-matt-parker/"
cover_image = "/r/parker-humble-pi.jpg"
cover_image_height = 463
cover_image_width = 300
read = "2021-03-15"
categories = [ "pop-math" ]
opinion = "dont-bother"
type = "BookshelfBook"
---

This is a collection of little stories and anecdotes involving
numbers.  I happened to know every one of them despite never searching
for this kind of facts.

I imagine it's a fun read if you've basically had no interaction with
math since high-school, but for the people likely to read my stuff, I
expect it won't be very interesting.
