---
title = "Cage of Souls"
author = "Adrian Tchaikovsky"
link = "https://www.adriantchaikovsky.com/books.html"
cover_image = "/r/tchaikovsky-cage-of-souls.jpg"
cover_image_height = 460
cover_image_width = 300
read = "2022-09-30"
categories = [ "novel", "sci-fi" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

This is a standalone post-post-post-apocalyptic novel.  It's a fun
story about the scheming happening in the last city on a dying Earth.

I read this right after "The Gulag Archipelago", so when half of this
book took place in a gulag, it felt quite real.  The take was more
light-hearted of course, but not by much.
