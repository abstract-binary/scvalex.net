---
title = "Ancillery Justice & Ancillery Sword"
author = "Ann Leckie"
link = "https://annleckie.com/novel/ancillary-justice/"
cover_image = "/r/leckie-ancillary-justice-sword.jpg"
cover_image_height = 229
cover_image_width = 320
read = "2022-02-03"
categories = [ "sci-fi", "space-opera", "novel" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

The first book in the trilogy is an interesting sci-fi story about a
trans-human making her way through a universe shaped by the
millennia-old actions of other trans-humans.  It is very good.

The second book throws out everything that made the first book
interesting and is instead just a story about rooting out some corrupt
officials on a space station.  Mind you, it isn't a bad book exactly,
but it's just generic sci-fi schlock.

The second book was disappointing enough that I'm not going to bother
reading the third one.
