---
title = "Other Minds"
author = "Peter Godfrey-Smith"
link = "https://us.macmillan.com/books/9780374537197/otherminds"
cover_image = "/r/godfrey-smith-other-minds.jpg"
cover_image_height = 450
cover_image_width = 300
read = "2023-04-19"
categories = [ "pop-science", "animals", "real-world" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

Half the book is stories about the author interacting with octopuses
and the other half is discussions on recent research in how brains
work.  The book's gimmick is that it compares and contrasts the brain
research with what we currently know about octopuses, which the author
describes as the most alien minds on Earth.

It's an ok read and I definitely learned some new things.  It was
nothing earth-shattering, but the book is fairly short at 200 pages,
so it also doesn't drag on.

I recommend this to anyone who likes reading about smart animals and
has an interest in neuro-science research.
