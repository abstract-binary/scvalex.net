---
title = "Net Zero — How We Stop Causing Climate Change"
author = "Dieter Helm"
link = "https://www.harpercollins.com/products/net-zero-how-we-stop-causing-climate-change-dieter-helm"
cover_image = "/r/helm-net-zero.jpg"
cover_image_height = 460
cover_image_width = 300
read = "2023-01-09"
categories = [ "real-world", "climate" ]
opinion = "maybe-read"
type = "BookshelfBook"
---

The book looks at stopping climate change from a government policy and
national economy point of view.  This contrasts nicely with Bill
Gates' book which mostly focuses on the tech angle.

The first third of the book is an excellent analysis on the policies
enacted in the last 30 years, how they failed, and how they will
likely continue failing.  Given that the author writes reports on
climate change for the UK gov, this is clearly his area of expertise.

According to the author, the three principles for a sustainable
economy are the polluter pays, public money for public goods, and net
environmental gain.  The discussion around all three is interesting,
but I think the first one is the most important.  The idea is that,
since our goal is to get to net-zero globally, we have to not only tax
local emitters of carbon, but also importers of carbon in order to
encourage the origin countries to tax their own carbon emitters.  If
we only tax the local producers, then that just moves industry
overseas where polluting is free.  More generally, we need to target
net-zero local *consumption* of carbon, not just net-zero local
*production*.

At the very end of the book, there is also a short description on how
to reform the electricity market to encourage decarbonization.  Sadly,
not a lot of details are given.  I think this was further developed in
the ["Cost of Energy Review"][helm-report], but I have not read that
yet.

[helm-report]: https://www.gov.uk/government/publications/cost-of-energy-independent-review

The main problem with the book is that the latter two thirds are a
disorganized overview of climate change combined with wishy-washy
technobabble.  The Gates book makes for a much better overview.
