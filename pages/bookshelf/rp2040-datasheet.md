---
title = "RP2040 Datasheet"
author = "Raspberry Pi"
link = "https://www.raspberrypi.com/documentation/microcontrollers/rp2040.html"
cover_image = "/r/rp2040-cover.jpg"
cover_image_height = 422
cover_image_width = 300
read = "2023-04-25"
categories = [ "programming", "manual", "embedded" ]
opinion = "must-read"
type = "BookshelfBook"
---

I learnt a lot reading the RP2040 datasheet.  As an experienced
programmer, this is the first time I've felt that I had a grasp of how
hardware actually works.  For instance, I now know what a DMA
controller is and how to use one.  I am also shocked by the sheer
variety of peripherals available at the lowest levels.

I could've tried reading books on the x86-64 architecture, but that's
complicated by the variety of manufacturers, the history of the
platform, and the complexity of the much larger system.  On the other
hand, the RP2040 is a brand new MCU developed by just one company and
it is documented in a single PDF.

As weird as it sounds, I'd recommend this to every developer.  Once
you get through it, a lot of the magic of hardware suddenly feels
tractable.
