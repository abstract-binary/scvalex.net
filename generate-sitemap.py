#!/usr/bin/env python3

from datetime import datetime, timezone
from functools import total_ordering
from pathlib import Path
import os
import subprocess
import xml.etree.ElementTree as ET

@total_ordering
class Page:
    def __init__(self, page):
        self.path = page
        self.url = "https://scvalex.net/" + self.path.removeprefix("dist/").removesuffix("index.html")
        if self.url == "https://scvalex.net/":
            self.url = "https://scvalex.net"
        self.source = self.find_source()
        self.lastmod = self.find_lastmod()

    def __eq__(self, other):
        return (self.url == other.url)

    def __lt__(self, other):
        return (self.url < other.url)

    def find_source(self):
        p = self.path.removeprefix("dist/")
        candidates = [
            "pages/" + p.removesuffix(".html") + ".md",
            "pages/" + p.removesuffix(".html") + ".html",
            "pages/" + p.removesuffix("/index.html") + ".md",
            "pages/" + p.removesuffix("/index.html") + ".html",
            "static/" + p,
        ]
        for c in candidates:
            f = Path(c)
            if f.is_file():
                return str(f)
        print("No source for " + self.path)
        return None

    def find_lastmod(self):
        if self.source is not None:
            try:
                res = subprocess.run(
                    ["git", "--no-pager", "log", "-1", '--pretty=%cI', self.source],
                    capture_output=True
                )
                return datetime.fromisoformat(res.stdout.decode("utf-8").strip())
            except ValueError:
                print("Failed to get commit time of", self.url)
                return datetime.fromtimestamp(os.lstat(self.source).st_mtime)
        else:
            return None

def noindex(page):
    import re
    with open(page) as f:
        text = f.read()
        return re.search('<meta name="robots" content="noindex">', text)

def ignore_page(page):
    return (page.endswith("now/index.html")
            or page.endswith("reading-list/index.html")
            or page.endswith("google66b4f0b4f789f10d.html")
            or page.endswith("utils/counter.html")
            or page.endswith("404/index.html")
            or noindex(page))

def collect_pages():
    all_pages = [str(p) for p in Path("dist").glob("**/*.html")]
    filtered_pages = [page for page in all_pages if not ignore_page(page)]
    pages = map(Page, filtered_pages)
    return list(pages)

def main():
    pages = collect_pages()
    pages.sort()
    with open("dist/sitemap.txt", "w") as sitemap_txt:
        for p in pages:
            print(p.url, file=sitemap_txt)
    with open("dist/sitemap.xml", "wb") as sitemap_xml:
        s = ET.Element("urlset", xmlns="http://www.sitemaps.org/schemas/sitemap/0.9")
        for p in pages:
            url = ET.SubElement(s, "url")
            loc = ET.SubElement(url, "loc")
            loc.text = p.url
            if p.lastmod:
                lastmod = ET.SubElement(url, "lastmod")
                lastmod.text = str(p.lastmod.date())
        sitemap_xml.write(ET.tostring(s, xml_declaration=True, encoding="utf-8"))

if __name__ == "__main__":
    main()
