#!/bin/sh

if (( $# != 1 )); then
    echo "USAGE: $0 PATH"
    exit 1
fi

cat $1 | sed -e '/^```/,/^```$/d' -e '/^<pre /,/<\/pre>$/d' -e '/^---$/,/^---$/d' -e '/^\[.*\]:/d' -e '/<[^>]*>/d' | wc -w
