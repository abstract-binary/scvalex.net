const colors = require('tailwindcss/colors')

module.exports = {
    mode: 'jit',
    content: [
        './static/**/*.html',
        './pages/**/*.{html,js,jsx,ts,tsx,vue,md}',
        './templates/**/*.liquid',
    ],
    safelist: ["top-0", "top-4", "active:top-1", "active:top-5", "bookHidden", ".katex-html"],
    theme: {
        fontFamily: {
            sans: ["Atkinson-Hyperlegible", "Roboto", "ui-sans-serif", "system-ui", "-apple-system", "BlinkMacSystemFont", "Segoe UI", "Helvetica Neue", "Arial", "Noto Sans", "sans-serif", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"],
            serif: ["ui-serif", "Georgia", "Cambria", "Times New Roman", "Times", "serif"],
            mono: ["ui-monospace", "SFMono-Regular", "Menlo", "Monaco", "Consolas", "Liberation Mono", "Courier New", "monospace"],
        },
        extend: {
            colors: {
                'button-hover': colors.sky['200'],
            },
            width: {
                'article': '56rem',
                'cv-full': '96rem',
                'cv-half-big': '46rem',
                'cv-half-small': '32rem',
                '92': '23rem',
            },
            height: {
                'cv-half-card': '30rem',
            },
            minWidth: {
            },
            maxWidth: {
                '96': '24rem',
                '120': '30rem',
            },
            margin: {
                'article': '57rem',
            },
            listStyleType: {
                'square': 'square',
            },
        },
    }
};
