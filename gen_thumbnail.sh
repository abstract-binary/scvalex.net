#!/usr/bin/env bash

set -e -x

OUT="${1%.svg}_thumb.png"

inkscape "$1" --export-width 800 --export-background "#ffffff" -o "${OUT}"
optipng -o7 "${OUT}"
